﻿using AntFarm.Core;
using System;
using System.Data;
using System.Data.SqlClient;

namespace AntFarm.Providers.SQLServer
{
    /// <summary>
    /// Extends the base class to provide support for <see cref="DateTimeOffset"/> and <see cref="TimeSpan"/> types.
    /// </summary>
    public class SqlDataReaderCo : DataReaderCo
    {
        public SqlDataReaderCo(IDataReader dr, IFormatProvider fp)
            : base(dr, fp)
        {
            SqlDataReader = 
                dr as SqlDataReader 
                ?? throw new InvalidOperationException($"Invalid type of data reader. Expected {typeof(SqlDataReader)}, got {dr.GetType()}.");
        }

        public SqlDataReader SqlDataReader { get; }

        public override DateTimeOffset GetDateTimeOffset(int i) => SqlDataReader.GetDateTimeOffset(i);
        public override TimeSpan GetTimeSpan(int i) => SqlDataReader.GetTimeSpan(i);
    }
}
