﻿using AntFarm.ORM;
using AntFarm.ORM.Providers;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;

namespace AntFarm.Providers.SQLite
{
    public class SQLiteTableProvider : TableProvider
    {
        public SQLiteTableProvider(SQLiteDatabaseProvider dbProvider, Type rowType)
           : base(dbProvider, rowType)
        { }

        public override string GetCommandTextForTruncate()
        {
            var sb = new StringBuilder();
            sb
               .Append("DELETE FROM ")
               .Append(GetTableName());
            return sb.ToString();
        }

        public override string GetCommandTextForInsert()
        {
            //var pkcols = this.PrimaryKeyColumns;
            //var pkparms = new List<string>(pkcols.Length);
            var inscols = this.Columns.Where(x => !x.IsReadOnly).ToList();
            //var idcol = this.Columns.SingleOrDefault(x => x.IsAutoNumber);

            var sb = new StringBuilder();

            ////Build the statement(s) for all the sequences in the inserted record. The parameters are of the
            ////form @S<i> where i is the index of the inserted column, to make it easier to match in the VALUES
            ////clause of the insert statement (see below).
            //for (int i = 0; i < inscols.Count; i++)
            //{
            //    var colInfo = inscols[i];
            //    if (colInfo.SequenceName != null)
            //        throw new InvalidOperationException("Sequences are not a feature of SQLite.");
            //}

            //Build the actual INSERT statement. This is a standard statement, of the form:
            //INSERT INTO <table>(<columns>)VALUES(<values>)
            sb
                .Append("INSERT INTO ")
                .Append(GetTableName())
                .Append("(");
            for (int i = 0; i < inscols.Count; i++)
            {
                var colInfo = inscols[i];
                sb
                    .Append(i > 0 ? "," : "")
                    .Append(GetColumnName(colInfo));
            }
            sb.Append(")VALUES(");
            for (int i = 0; i < inscols.Count; i++)
            {
                var inscol = inscols[i];
                string paramName = $"@V{i}";
                sb.Append(i > 0 ? "," : "").Append(paramName);
                //if (inscol.IsPrimaryKey)
                //    pkparms.Add(paramName);
            }
            sb.AppendLine(");");

            ////If an identity column is defined, build the statement to fetch it. Like for the sequences, this statement
            ////is highly dependent on the provider (here, SQL Server syntax).
            //if (idcol != null)
            //{
            //    sb.AppendLine("DECLARE @ID INT;");
            //    sb.AppendLine("SELECT @ID=_ROWID_;");
            //    if (idcol.IsPrimaryKey)
            //        pkparms.Add("@ID");
            //}

            ////Finally, append the statement to return the primary key (if any). It can either be a provided
            ////value (@PK=@V#), the next number in a sequence (@PK=@S#), or the next identity number (@PK=@ID).
            //if (pkparms.Count > 0)
            //{
            //    var sbpk = new StringBuilder();
            //    sbpk.Append("SELECT ");
            //    Debug.Assert(pkcols.Length == pkparms.Count);
            //    for (int i = 0; i < pkparms.Count; i++)
            //    {
            //        if (i > 0)
            //            sbpk.Append(",");
            //        sbpk.Append("@PK").Append(i).Append("=").Append(pkparms[i]);
            //    }
            //    sbpk.AppendLine(";");
            //    sb.Append(sbpk.ToString());
            //}

            return sb.ToString();
        }

        public override IDbDataParameter[] ParameterizeCommandForInsert(IDbCommand cd, object row)
        {
            var inscols = this.Columns.Where(x => !x.IsReadOnly).ToList();
            for (int i = 0; i < inscols.Count; i++)
            {
                var inscol = inscols[i];
                if (inscol.SequenceName == null)
                {
                    object value = inscol.Accessor.GetValue(row);
                    var p = inscol.CreateCommandParameter(cd, $"@V{i}", value);
                    cd.Parameters.Add(p);
                }
            }
            //var pkcols = this.PrimaryKeyColumns;
            //if (pkcols.Length > 0)
            //{
            //    var outps = new IDbDataParameter[pkcols.Length];
            //    for (int i = 0; i < pkcols.Length; i++)
            //    {
            //        //IDbDataParameter
            //        var p = cd.CreateParameter();
            //        p.ParameterName = $"@PK{i}";
            //        p.Direction = ParameterDirection.Output;
            //        p.DbType = pkcols[i].DBType;
            //        p.Size = 4000; //TODO: Better way to determine the max size. Via the Column attribute?
            //        cd.Parameters.Add(p);
            //        outps[i] = p;
            //    }
            //    return outps;
            //}
            //else
                return null;
        }
    }
}