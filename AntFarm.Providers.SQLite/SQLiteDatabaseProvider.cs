﻿using AntFarm.ORM.Providers;
using System;
using System.Data.Common;

namespace AntFarm.Providers.SQLite
{
    public class SQLiteDatabaseProvider : DatabaseProvider
    {
        public override char SQLIdentifierDelimiterBegin => '"';
        public override char SQLIdentifierDelimiterEnd => '"';
        public override char SQLStatementSeparator => ';';
        public override char SQLParameterPrefix => '@';
        public SQLiteDatabaseProvider(AntConfig config)
            : base(config)
        { }
        public override DbConnection CreateConnection() => Microsoft.Data.Sqlite.SqliteFactory.Instance.CreateConnection();

        public override TableProvider CreateTableProvider(Type rowType) => new SQLiteTableProvider(this, rowType);
    }
}