Relational Data Set Model with Enforced Integrity
=================================================

Project Names
=============
Preferred: AntFarm
Alternate: PocoFarm

Project Description
===================
AntFarm is yet another Micro ORM project. It features two main layers. The first layer is the ORM itself, with classic operations such as selection and CRUD on database entities represented by .NET classes. The second layer manages relationships between those entities to integrate edit operations within a "data set" of related objects, helping preserve their referential integrity, and adding a few other goodies.
