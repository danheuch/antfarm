﻿using AntFarm.ORM.Providers;
using System;
using System.Data.Common;

namespace AntFarm.Providers.PostgreSQL
{
    public class PostgresDatabaseProvider : DatabaseProvider
    {
        public override char SQLIdentifierDelimiterBegin => '"';
        public override char SQLIdentifierDelimiterEnd => '"';
        public override char SQLStatementSeparator => ';';
        public override char SQLParameterPrefix => '@';
        public PostgresDatabaseProvider(AntConfig config)
            : base(config)
        { }
        public override DbConnection CreateConnection() => Npgsql.NpgsqlFactory.Instance.CreateConnection();

        public override TableProvider CreateTableProvider(Type rowType) => new PostgresTableProvider(this, rowType);
    }
}