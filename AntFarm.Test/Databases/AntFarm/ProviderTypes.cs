﻿using AntFarm.ORM;
using AntFarm.Providers.SqlServer;
using System;
using System.Data;

namespace AntFarm.Test.Databases.AntFarm
{
    [Table("ProviderTypes", "dbo"), ExplicitColumns(false)]
    public class ProviderTypes
    {
        #region Columns

        public int ID { get; set; }

        /// <summary>
        /// [bigint]
        /// </summary>
        public long? Fld_BigInt { get; set; }

        /// <summary>
        /// [binary](50)
        /// </summary>
        public byte[] Fld_Binary { get; set; }

        /// <summary>
        /// [bit]
        /// </summary>
        public bool? Fld_Bit { get; set; }

        /// <summary>
        /// [char](10)
        /// </summary>
        public string Fld_Char { get; set; }

        /// <summary>
        /// [date]
        /// </summary>
        public DateTime? Fld_Date { get; set; }

        /// <summary>
        /// [datetime]
        /// </summary>
        public DateTime? Fld_DateTime { get; set; }

        /// <summary>
        /// [datetime2](7)
        /// </summary>
        public DateTime? Fld_DateTime2 { get; set; }

        /// <summary>
        /// [datetimeoffset](7)
        /// </summary>
        public DateTimeOffset? Fld_DateTimeOffset { get; set; }

        /// <summary>
        /// [decimal](18, 2)
        /// </summary>
        public decimal? Fld_Decimal { get; set; }

        /// <summary>
        /// [float]
        /// </summary>
        public double? Fld_Float { get; set; }

        /// <summary>
        /// [image]
        /// </summary>
        public byte[] Fld_Image { get; set; }

        /// <summary>
        /// [int]
        /// </summary>
        public int? Fld_Int { get; set; }

        /// <summary>
        /// [money]
        /// </summary>
        public decimal? Fld_Money { get; set; }

        /// <summary>
        /// [nchar](10)
        /// </summary>
        public string Fld_NChar { get; set; }

        /// <summary>
        /// [ntext]
        /// </summary>
        public string Fld_NText { get; set; }

        /// <summary>
        /// [nvarchar](max)
        /// </summary>
        public string Fld_NVarChar { get; set; }

        /// <summary>
        /// [real]
        /// </summary>
        public float? Fld_Real { get; set; }

        /// <summary>
        /// [smalldatetime]
        /// </summary>
        public DateTime? Fld_SmallDateTime { get; set; }

        /// <summary>
        /// [smallint]
        /// </summary>
        public short? Fld_SmallInt { get; set; }

        /// <summary>
        /// [smallmoney]
        /// </summary>
        public decimal? Fld_SmallMoney { get; set; }

        /// <summary>
        /// [text]
        /// </summary>
        public string Fld_Text { get; set; }

        /// <summary>
        /// [time](7)
        /// </summary>
        public TimeSpan Fld_Time { get; set; }

        /// <summary>
        /// [timestamp]
        /// </summary>
        [SqlMapper(SqlDbType.Timestamp)]
        public byte[] Fld_Timestamp { get; set; }

        /// <summary>
        /// [tinyint]
        /// </summary>
        public byte? Fld_TinyInt { get; set; }

        /// <summary>
        /// [uniqueidentifier]
        /// </summary>
        public Guid? Fld_UniqueIdentifier { get; set; }

        /// <summary>
        /// [varbinary](max)
        /// </summary>
        public byte[] Fld_VarBinary { get; set; }

        /// <summary>
        /// [varchar](max)
        /// </summary>
        public string Fld_VarChar { get; set; }

        /// <summary>
        /// [sql_variant]
        /// </summary>
        public object Fld_Variant { get; set; }

        /// <summary>
        /// [xml]
        /// </summary>
        public string Fld_Xml { get; set; }

        #endregion

        public static ProviderTypes Create_Random(int id)
        {
            return new ProviderTypes
            {
                ID = id,
                Fld_BigInt = RandomData.RandomInt64(0, 10000),
                Fld_Binary = RandomData.RandomBytes(50),
                Fld_Bit = RandomData.RandomBoolean(),
                Fld_Char = RandomData.RandomString(10),
                Fld_Date = RandomData.RandomDateTime(false),
                Fld_DateTime = RandomData.RandomDateTime(true),
                Fld_DateTime2 = RandomData.RandomDateTime(true),
                Fld_DateTimeOffset = RandomData.RandomDateTimeOffset(true),
                Fld_Decimal = RandomData.RandomDecimal(),
                Fld_Float = RandomData.RandomDouble(),
                Fld_Image = RandomData.RandomBytes(1000),
                Fld_Int = RandomData.RandomInt32(),
                Fld_Money = RandomData.RandomDecimal(),
                Fld_NChar = RandomData.RandomString(10),
                Fld_NText = RandomData.RandomString(255),
                Fld_NVarChar = RandomData.RandomString(1000),
                Fld_Real = RandomData.RandomSingle(),
                Fld_SmallDateTime = RandomData.RandomDateTime(true),
                Fld_SmallInt = RandomData.RandomInt16(),
                Fld_SmallMoney = RandomData.RandomDecimal(),
                Fld_Text = RandomData.RandomString(1000),
                Fld_Time = RandomData.RandomTimeSpan(1),
                //Fld_Timestamp is a Sql Server timestamp => read-only,
                Fld_TinyInt = 0,
                Fld_UniqueIdentifier = Guid.NewGuid(),
                Fld_VarBinary = RandomData.RandomBytes(1000),
                Fld_VarChar = RandomData.RandomString(1000),
                Fld_Variant = RandomData.RandomObject(),
                Fld_Xml = RandomData.RandomXml()
            };
        }
    }
}