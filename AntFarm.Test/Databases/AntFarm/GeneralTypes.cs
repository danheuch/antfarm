﻿using AntFarm.ORM;
using NUnit.Framework;
using System;

namespace AntFarm.Test.Databases.AntFarm
{
    [Table("GeneralTypes", "dbo"), ExplicitColumns(false)]
    public class GeneralTypes
    {
        public int ID { get; set; }

        #region Non-Nullable Columns

        public bool fldBoolean { get; set; }
        public byte fldByte { get; set; }
        public sbyte fldSByte { get; set; }
        public short fldInt16 { get; set; }
        public ushort fldUInt16 { get; set; }
        public int fldInt32 { get; set; }
        public uint fldUInt32 { get; set; }
        public long fldInt64 { get; set; }
        public ulong fldUInt64 { get; set; }
        public float fldSingle { get; set; }
        public double fldDouble { get; set; }
        public decimal fldDecimal { get; set; }
        public char fldChar { get; set; }
        public string fldString { get; set; }
        public DateTime fldDateTime { get; set; }
        public Guid fldGuid { get; set; }

        #endregion
        
        #region Nullable Columns

        public bool? fldBooleanN { get; set; }
        public byte? fldByteN { get; set; }
        public sbyte? fldSByteN { get; set; }
        public short? fldInt16N { get; set; }
        public ushort? fldUInt16N { get; set; }
        public int? fldInt32N { get; set; }
        public uint? fldUInt32N { get; set; }
        public long? fldInt64N { get; set; }
        public ulong? fldUInt64N { get; set; }
        public float? fldSingleN { get; set; }
        public double? fldDoubleN { get; set; }
        public decimal? fldDecimalN { get; set; }
        public char? fldCharN { get; set; }
        public string fldStringN { get; set; }
        public DateTime? fldDateTimeN { get; set; }
        public Guid? fldGuidN { get; set; }

        #endregion

        public byte[] fldByteArray { get; set; }

        public static GeneralTypes Create_Random(int id)
        {
            return new GeneralTypes
            {
                ID = id,
                fldBoolean = RandomData.RandomBoolean(),
                fldByte = Byte.MaxValue,
                fldSByte = SByte.MinValue,
                fldInt16 = Int16.MaxValue,
                fldUInt16 = UInt16.MaxValue,
                fldInt32 = Int32.MaxValue,
                fldUInt32 = UInt32.MaxValue,
                fldInt64 = Int64.MaxValue,
                fldUInt64 = UInt64.MaxValue,
                fldSingle = RandomData.RandomSingle(),
                fldDouble = RandomData.RandomDouble(),
                fldDecimal = RandomData.RandomDecimal(),
                fldChar = RandomData.RandomChar(),
                fldString = RandomData.RandomString(10),
                fldDateTime = RandomData.RandomDateTime(true),
                fldGuid = Guid.NewGuid(),
            };
        }
    }

    [TestFixture]
    public class UniTest2
    {
        [Test]
        public void MainTest()
        {
            using (var db = new Database("AntFarm"))
            {
                db.Truncate<GeneralTypes>();
                for (int i = 1; i <= 10; i++)
                    db.Insert(GeneralTypes.Create_Random(i));
                var list1 = db.SelectAll<GeneralTypes>();
            }
        }
    }
}