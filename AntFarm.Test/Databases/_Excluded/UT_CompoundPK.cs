﻿using AntFarm.ORM;
using System;
using System.Linq;

namespace AntFarm.Test.Databases.SqlServer.AntFarm
{
    [Table("UT_CompoundPK", "dbo"), ExplicitColumns]
    public class UT_CompoundPK : RowBase
    {
        [Column("Code", IsPrimaryKey = true)]
        public string PK0 { get; set; }
        [Column("Version", IsPrimaryKey = true)]
        public DateTime PK1 { get; set; }
        [Column]
        public string SomeText { get; set; }
        [Column]
        public double? SomeNumber { get; set; }
        [Column]
        public DateTime? SomeDate { get; set; }

        public static UT_CompoundPK Create_Empty()
        {
            return new UT_CompoundPK
            {
                PK0 = "0",
                PK1 = DateTime.Now,
            };
        }
        public static UT_CompoundPK Create_Min()
        {
            return new UT_CompoundPK
            {
                PK0 = "A",
                PK1 = DateTime.Now,
                ////SomeText = null,
                //SomeNumber = double.MinValue,
                //SomeDate = new DateTime(1800, 1, 1),
            };
        }
        public static UT_CompoundPK Create_Max()
        {
            return new UT_CompoundPK
            {
                PK0 = "Z",
                PK1 = DateTime.Now,
                ////SomeText = null,
                //SomeNumber = double.MaxValue,
                //SomeDate = new DateTime(9999, 12, 31),
            };
        }
        private static Random _rnd = new Random();
        public static UT_CompoundPK Create_Random()
        {
            const string chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            var buffer = new byte[_rnd.Next(0, 1000)];
            _rnd.NextBytes(buffer);
            return new UT_CompoundPK
            {
                PK0 = new string(Enumerable.Repeat(chars, 10).Select(s => s[_rnd.Next(s.Length)]).ToArray()),
                PK1 = DateTime.Now,
                SomeText = new string(Enumerable.Repeat(chars, _rnd.Next(0, 50)).Select(s => s[_rnd.Next(s.Length)]).ToArray()),
                SomeNumber = Convert.ToDouble(_rnd.NextDouble()) * float.MaxValue,
                SomeDate = DateTime.Now,
            };
        }
    }
}