﻿using AntFarm.ORM;
using System;
using System.Linq;

namespace AntFarm.Test.Databases.SqlServer.AntFarm
{
    public enum SomeEnumType { None = 0, Yes = 1, No = 2, Maybe = 3 }

    [Table("UT_Basic", "dbo"), ExplicitColumns]
    public class UT_Basic : RowBase
    {
        [Column("ID", IsPrimaryKey = true, IsAutoNumber = true)]
        public int ID { get; set; }

        [Column]
        public bool? SomeBool { get; set; }
        [Column]
        public byte? SomeByte { get; set; }
        [Column]
        public char? SomeChar { get; set; }
        [Column]
        public decimal? SomeDecimal { get; set; }
        [Column]
        public double? SomeDouble { get; set; }
        [Column]
        public short? SomeShort { get; set; }
        [Column]
        public int? SomeInt { get; set; }
        [Column]
        public long? SomeLong { get; set; }
        [Column]
        public sbyte? SomeSByte { get; set; }
        [Column]
        public float? SomeSingle { get; set; }
        [Column]
        public ushort? SomeUShort { get; set; }
        [Column]
        public uint? SomeUInt { get; set; }
        [Column]
        public ulong? SomeULong { get; set; }
        [Column]
        public DateTime? SomeDateTime { get; set; }
        [Column]
        public DateTimeOffset? SomeDateTimeOffset { get; set; }
        [Column]
        public string SomeString { get; set; }
        [Column]
        public byte[] SomeBytes { get; set; }
        [Column]
        public SomeEnumType? SomeEnum { get; set; }

        public static UT_Basic Create_Empty()
        {
            return new UT_Basic();
        }
        public static UT_Basic Create_Min()
        {
            return new UT_Basic
            {
                SomeBool = false,
                SomeByte = byte.MinValue,
                SomeChar = char.MinValue,
                SomeDateTime = new DateTime(1800, 1, 1),
                SomeDateTimeOffset = new DateTimeOffset(new DateTime(1800, 1, 1)),
                SomeDecimal = 0,
                SomeDouble = 0,
                SomeInt = int.MinValue,
                SomeLong = long.MinValue,
                SomeSByte = sbyte.MinValue,
                SomeShort = short.MinValue,
                SomeSingle = 0,
                SomeUInt = uint.MinValue,
                SomeULong = ulong.MinValue,
                SomeUShort = ushort.MinValue,
                //SomeString = null,
                //SomeBytes = null,
            };
        }
        public static UT_Basic Create_Max()
        {
            return new UT_Basic
            {
                SomeBool = true,
                SomeByte = byte.MaxValue,
                SomeChar = char.MaxValue,
                SomeDateTime = new DateTime(9999, 12, 31),
                SomeDateTimeOffset = new DateTimeOffset(new DateTime(9999, 12, 31)),
                SomeDecimal = 1000,
                SomeDouble = 1000,
                SomeInt = int.MaxValue,
                SomeLong = long.MaxValue,
                SomeSByte = sbyte.MaxValue,
                SomeShort = short.MaxValue,
                SomeSingle = 1000,
                SomeUInt = uint.MaxValue,
                SomeULong = ulong.MaxValue,
                SomeUShort = ushort.MaxValue,
                //SomeString = null,
                //SomeBytes = null,
            };
        }
        private static Random _rnd = new Random();
        public static UT_Basic Create_Random()
        {
            const string chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            var buffer = new byte[_rnd.Next(0, 1000)];
            _rnd.NextBytes(buffer);
            var x = new UT_Basic
            {
                SomeBool = _rnd.Next(0, 1) == 1,
                SomeByte = Convert.ToByte(_rnd.Next(byte.MinValue, byte.MaxValue)),
                SomeChar = chars[_rnd.Next(0, chars.Length - 1)],
                SomeDateTime = DateTime.Now,
                SomeDateTimeOffset = DateTimeOffset.Now,
                SomeDecimal = Convert.ToDecimal(_rnd.NextDouble()) * 1000,
                SomeDouble = Convert.ToDouble(_rnd.NextDouble()) * 1000,
                SomeInt = Convert.ToInt32(_rnd.Next(int.MinValue, int.MaxValue)),
                SomeLong = Convert.ToInt64(_rnd.Next(int.MinValue, int.MaxValue)),
                SomeSByte = Convert.ToSByte(_rnd.Next(sbyte.MinValue, sbyte.MaxValue)),
                SomeShort = Convert.ToInt16(_rnd.Next(short.MinValue, short.MaxValue)),
                SomeSingle = Convert.ToSingle(_rnd.NextDouble()) * 1000,
                SomeUInt = Convert.ToUInt32(_rnd.Next(0, int.MaxValue)),
                SomeULong = Convert.ToUInt64(_rnd.Next(0, int.MaxValue)),
                SomeUShort = Convert.ToUInt16(_rnd.Next(0, ushort.MaxValue)),
                SomeString = new string(Enumerable.Repeat(chars, _rnd.Next(0, 50)).Select(s => s[_rnd.Next(s.Length)]).ToArray()),
                SomeBytes = buffer,
                SomeEnum = SomeEnumType.Maybe,
            };
            return x;
        }
    }
}