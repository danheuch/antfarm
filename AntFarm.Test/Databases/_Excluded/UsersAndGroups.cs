﻿using AntFarm.Models;
using AntFarm.ORM;

namespace AntFarm.Test.Databases.SqlServer.AntFarm
{
    [Table("UsersAndGroups", "dbo"), ExplicitColumns]
    public class UserAndGroup : RowBase
    {
        [Column(IsPrimaryKey = true), References(typeof(User))]
        public int User_ID { get; set; }
        [Column(IsPrimaryKey = true), References(typeof(Group))]
        public string Group_CD { get; set; }
    }

    [Table("Users", "dbo"), ExplicitColumns]
    public class User : RowBase
    {
        [Column(IsPrimaryKey = true, IsAutoNumber = true)]
        public int ID { get; set; }
        [Column]
        public string Name { get; set; }
    }

    [Table("Groups", "dbo"), ExplicitColumns]
    public class Group : RowBase
    {
        [Column(IsPrimaryKey = true)]
        public string CD { get; set; }
        [Column]
        public string Name { get; set; }
    }
}
