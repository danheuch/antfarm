﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AxORM.Test.Databases.TIPSQA.APP_DATA
{
    class TABLE1
    {
        [Column(IsPrimaryKey = true, SequenceName = "sequence1")]
        public int SEQ { get; set; }
        [Column(IsAutoNumber = true)]
        public int ID1 { get; set; }
        public string TXT { get; set; }
    }
}
