﻿using AntFarm.Models;
using AntFarm.ORM;
using System;

namespace AntFarm.Test.Databases.SqlServer.Phoenix
{
    [Table("Product_Names", "tips"), ExplicitColumns, Serializable]
    public class Product_Name : RowBase
    {
        [Column, References(typeof(Product))]
        public int Product_ID { get; set; }
        [Column]
        public string Language_Code { get; set; }
        [Column]
        public string ProductName { get; set; }
    }
}