﻿using AntFarm.Models;
using AntFarm.ORM;
using System;

namespace AntFarm.Test.Databases.SqlServer.Phoenix
{
    [Table("Product_Countries", "tips"), ExplicitColumns, Serializable]
    public class Product_Country : RowBase
    {
        [Column, References(typeof(Product))]
        public int Product_ID { get; set; }
        [Column]
        public string Country_Code { get; set; }
    }
}