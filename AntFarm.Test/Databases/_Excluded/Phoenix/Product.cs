﻿using AntFarm.ORM;
using System;

namespace AntFarm.Test.Databases.SqlServer.Phoenix
{
    [Table("Product", "tips"), ExplicitColumns, Serializable]
    public class Product : RowBase
    {
        [Column]
        public string ProductCode { get; set; }
        [Column]
        public string FormulaCode { get; set; }
        [Column]
        public string Brand_Code { get; set; }
        [Column]
        public string DefaultProductName { get; set; }
    }
}
