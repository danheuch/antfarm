﻿using AntFarm.ORM;
using System;

namespace AntFarm.Test.Databases.SqlServer.Phoenix
{
    [Serializable]
    public abstract class RowBase
    {
        [Column("ID", IsPrimaryKey = true, IsAutoNumber = true)]
        public int ID { get; set; }

        [Column(AuditOption = AuditColumnOption.CreatedBy)]
        public string syscreaby { get; set; }

        [Column(AuditOption = AuditColumnOption.CreatedOn)]
        public DateTime? syscreaon { get; set; }

        [Column(AuditOption = AuditColumnOption.UpdatedBy)]
        public string sysupdtby { get; set; }

        [Column(AuditOption = AuditColumnOption.UpdatedOn)]
        public DateTime? sysupdton { get; set; }
    }
}
