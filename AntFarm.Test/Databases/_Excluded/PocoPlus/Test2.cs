﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace AxORM.Test.Databases.AxORM
{
    [Table]
    class Test2

    {
        [Column(IsPrimaryKey = true)]
        public string Code { get; set; }
        [Column(IsPrimaryKey = true)]
        public string SubCode { get; set; }
        [Column(IsPrimaryKey = true, SequenceName = "Sequence1")]
        public int Seq { get; set; }
        public string Description { get; set; }
        public override string ToString() { return $"{Code}-{SubCode}-{Seq}"; }
    }
}
