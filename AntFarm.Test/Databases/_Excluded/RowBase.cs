﻿using AntFarm.ORM;
using System;

namespace AntFarm.Test.Databases.SqlServer.AntFarm
{
    public abstract class RowBase
    {
        [Column(AuditOption = AuditColumnOption.CreatedBy)]
        public string syscreaby { get; set; }

        [Column(AuditOption = AuditColumnOption.CreatedOn)]
        public DateTimeOffset? syscreaon { get; set; }

        [Column(AuditOption = AuditColumnOption.UpdatedBy)]
        public string sysupdtby { get; set; }

        [Column(AuditOption = AuditColumnOption.UpdatedOn)]
        public DateTimeOffset? sysupdton { get; set; }
    }
}
