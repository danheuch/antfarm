﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AxORM.Test.Databases.TIPSQA.APP_DATA
{
    class STATE_JOBTRIGGERS
    {
        [Column(IsPrimaryKey = true)]
        public string TRIGGERNAME { get; set; }
        public DateTime? LASTRUN { get; set; }
        public DateTime? UPDATEDON { get; set; }
    }
}
