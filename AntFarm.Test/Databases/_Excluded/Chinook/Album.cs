﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AxORM.Test.Databases.Chinook
{
    [Table("$Album", "dbo"), ExplicitColumns]
    class Album
    {
        [Column("Album ID", IsPrimaryKey = true, IsAutoNumber = true)]
        public int Id { get; set; }
        [Column]
        public string Title { get; set; }
        [Column]
        public int? ArtistId { get; set; }
    }
}
