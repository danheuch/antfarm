﻿using AntFarm.ORM;
using AntFarm.Providers.Oracle;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Linq;

namespace AntFarm.Test.Databases.TIPS.APP_DATA
{
    //0:GetValue=1,GetFieldType=System.Decimal,GetDataTypeName=Decimal
    //1:GetValue=,GetFieldType=System.String,GetDataTypeName=Char
    //2:GetValue=,GetFieldType=System.String,GetDataTypeName=Varchar2
    //3:GetValue=,GetFieldType=System.String,GetDataTypeName=NChar
    //4:GetValue=,GetFieldType=System.String,GetDataTypeName=NVarchar2
    //5:GetValue=,GetFieldType=System.DateTime,GetDataTypeName=Date
    //6:GetValue=,GetFieldType=System.DateTime,GetDataTypeName=TimeStamp
    //7:GetValue=,GetFieldType=System.Int64,GetDataTypeName=IntervalYM
    //8:GetValue=,GetFieldType=System.TimeSpan,GetDataTypeName=IntervalDS
    //9:GetValue=,GetFieldType=System.Byte[],GetDataTypeName=Blob
    //10:GetValue=,GetFieldType=System.String,GetDataTypeName=Clob
    //11:GetValue=,GetFieldType=System.String,GetDataTypeName=NClob
    //12:GetValue=,GetFieldType=System.Byte[],GetDataTypeName=BFile
    //13:GetValue=,GetFieldType=System.Decimal,GetDataTypeName=Decimal
    //14:GetValue=,GetFieldType=System.Single,GetDataTypeName=BinaryFloat
    //15:GetValue=,GetFieldType=System.Double,GetDataTypeName=BinaryDouble

    [Table("PROVIDER_TYPES", "APP_DATA"), ExplicitColumns(false)]
    public class ProviderTypes
    {
        public int ID { get; set; }

        #region Basic Columns (no issue)

        /// <summary>BLOB</summary>
        public byte[] FLD_BLOB { get; set; }

        /// <summary>CHAR(10 BYTE)</summary>
        [OracleMapper(OracleDbType.Char, Size = 10)]
        public string FLD_CHAR { get; set; }

        /// <summary>CLOB</summary>
        public string FLD_CLOB { get; set; }

        /// <summary>DATE</summary>
        public DateTime? FLD_DATE { get; set; }

        /// <summary>DECIMAL</summary>
        public decimal? FLD_DECIMAL { get; set; }

        /// <summary>LONG</summary>
        public string FLD_LONG { get; set; }

        /// <summary>SMALLINT</summary>
        public short? FLD_INT16 { get; set; }

        /// <summary>INT</summary>
        public int? FLD_INT32 { get; set; }

        /// <summary>NCLOB</summary>
        public string FLD_NCLOB { get; set; }

        /// <summary>NCHAR(10)</summary>
        public string FLD_NCHAR { get; set; }

        /// <summary>NVARCHAR2(50)</summary>
        public string FLD_NVARCHAR2 { get; set; }

        /// <summary>TIMESTAMP(6)</summary>
        public DateTime? FLD_TIMESTAMP { get; set; }

        /// <summary>VARCHAR2(50 BYTE)</summary>
        public string FLD_VARCHAR2 { get; set; }

        /// <summary>BINARY_DOUBLE</summary>
        public double? FLD_BINARYDOUBLE { get; set; }

        /// <summary>BINARY_FLOAT</summary>
        public float? FLD_BINARYFLOAT { get; set; }

        #endregion

        #region Interval types need an Oracle mapper attribute to nudge the correct type.

        /// <summary>INTERVAL DAY(2) TO SECOND(6)</summary>
        [OracleMapper(OracleDbType.IntervalDS)]
        public TimeSpan? FLD_INTERVALDS { get; set; }

        /// <summary>INTERVAL YEAR(2) TO MONTH</summary>
        [OracleMapper(OracleDbType.IntervalYM)]
        public long? FLD_INTERVALYM { get; set; }

        #endregion

        //NOT SUPPORTED FOR NOW
        ///// <summary>BFILE</summary>
        //[OracleMapper(OracleDbType.BFile)]
        //public byte[] FLD_BFILE { get; set; }

        public static ProviderTypes Create_Random(int id)
        {
            return new ProviderTypes
            {
                ID = id,

                FLD_BINARYDOUBLE = RandomData.RandomDouble(),
                FLD_BINARYFLOAT = RandomData.RandomSingle(),
                FLD_BLOB = RandomData.RandomBytes(600),
                FLD_CHAR = RandomData.RandomString(10),
                FLD_CLOB = RandomData.RandomString(500),
                FLD_DATE = RandomData.RandomDateTime(true),
                FLD_DECIMAL = RandomData.RandomDecimal(),
                FLD_INT16 = RandomData.RandomInt16(),
                FLD_INT32 = RandomData.RandomInt32(),
                FLD_LONG = RandomData.RandomString(500),
                FLD_NCHAR = RandomData.RandomString(10),
                FLD_NCLOB = RandomData.RandomString(1000),
                FLD_NVARCHAR2 = RandomData.RandomString(50),
                FLD_TIMESTAMP = RandomData.RandomDateTime(true),
                FLD_VARCHAR2 = RandomData.RandomString(50),

                FLD_INTERVALDS = RandomData.RandomTimeSpan(10),
                FLD_INTERVALYM = RandomData.RandomInt64(0, 10),

                //FLD_BFILE = null,
            };
        }
    }
}