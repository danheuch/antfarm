﻿using AntFarm.ORM;
using System;

namespace AntFarm.Test.Databases.GRS
{
    namespace Axalta.Data.TIPS.DAL.MakeSDS
    {
        [Table("SDSBank", "grs"), ExplicitColumns(false)]
        public class SDSBank
        {
            [Column("ID", IsPrimaryKey = true, IsAutoNumber = true)]
            public int ID { get; set; }
            public DateTimeOffset FirstGeneratedOn { get; set; }
            public DateTimeOffset LastGeneratedOn { get; set; }
            public int GenerationCount { get; set; }
            public string CPDocumentId { get; set; }
            public string ProductCode { get; set; }
            public string CountryCode { get; set; }
            public string LanguageCode { get; set; }
            public string BrandCode { get; set; }
            public int SDSOverlayID { get; set; }
            public string FormulaCode { get; set; }
            public string ProductName { get; set; }
        }
    }
}