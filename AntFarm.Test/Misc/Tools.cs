﻿using NUnit.Framework;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Data;
using System.Diagnostics;

namespace AntFarm.Test.Misc
{
    [TestFixture]
    internal class Tools
    {
        [TestCase("APP_DATA.PROVIDER_TYPES")]
        public void WriteDataTypeNamesToDebugOutput(string tableName)
        {
            string cs = AntFarmer.DefaultConfig.ConnectionStrings["APP_DATA"].ConnectionString;
            using (var cn = new OracleConnection(cs))
            {
                cn.Open();
                using (var cd = cn.CreateCommand())
                {
                    cd.CommandText = $"select * from {tableName} where 1=0";
                    cd.CommandType = CommandType.Text;
                    using (IDataReader dr = cd.ExecuteReader())
                    {
                        for (int i = 0; i < dr.FieldCount; i++)
                        {
                            Debug.WriteLine($"{i}: GetFieldType={dr.GetFieldType(i)}, GetDataTypeName={dr.GetDataTypeName(i)}");
                        }
                    }
                }
            }
        }

        [Test]
        public void DateTimeOffset_Conversion_Between_Time_Zones()
        {
            var localNow = DateTimeOffset.Now;
            foreach (var tz in TimeZoneInfo.GetSystemTimeZones())
            {
                var remoteNow = tz.ConvertDateTimeOffset(localNow);
                Assert.AreEqual(remoteNow, localNow);
                if (tz.BaseUtcOffset != TimeZoneInfo.Local.BaseUtcOffset)
                    Assert.AreNotEqual(remoteNow.Offset, localNow.Offset);
                Assert.AreEqual(remoteNow.Offset, tz.BaseUtcOffset);
                Assert.AreEqual(remoteNow.UtcDateTime, localNow.UtcDateTime);
            }
        }

        #region ILSpy methods to examine
#if false
        public static dynamic ILSpy1()
        {
            dynamic x = new { };
            x.y = 22;
            x.z = "Bonjour";
            return x;
        }

        public static object[] ILSpy2()
        {
            object[] x = new object[2];
            x[0] = 22;
            x[1] = "Bonjour";
            return x;
        }

        public static (int, string) ILSpy3()
        {
            (int y, string z) x;
            x.y = 22;
            x.z = "Bonjour";
            return x;
        }

        public static dynamic ILSpy4()
        {
            dynamic x = new ExpandoObject();
            x.y = 22;
            x.z = "Bonjour";
            return x;
        }

        public static dynamic ILSpy5()
        {
            var x = new ExpandoObject() as IDictionary<string, Object>;
            x.Add("y", 22);
            x.Add("z", "Bonjour");
            return x;
        }

        public class Test
        {
            public DateTime sysupdton1 { get; set; }
            public DateTimeOffset sysupdton2 { get; set; }
            public DateTime? sysupdton3 { get; set; }
            public DateTimeOffset? sysupdton4 { get; set; }
        }

        public static void ILSpy6(Test test, DateTime dt, TimeZoneInfo tz)
        {
            test.sysupdton1 = dt;
            test.sysupdton2 = new DateTimeOffset(dt, tz.BaseUtcOffset);
            test.sysupdton3 = dt;
            test.sysupdton4 = new DateTimeOffset(dt, tz.BaseUtcOffset);
        }

        public static void ILSpy7(Test test, DateTimeOffset dt)
        {
            test.sysupdton1 = dt.DateTime;
            test.sysupdton2 = dt;
            test.sysupdton3 = dt.DateTime;
            test.sysupdton4 = dt;
        }
#endif
        #endregion
    }
}