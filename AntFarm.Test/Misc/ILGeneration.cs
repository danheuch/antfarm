﻿using AntFarm.Core;
using AntFarm.ORM;
using AntFarm.ORM.Providers;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Reflection;

namespace AntFarm.Test.Misc
{
    [TestFixture]
    internal class ILGeneration
    {
        public class ILTestClass
        {
            [Column(IsPrimaryKey = true)]
            public int PK1 { get; set; }
            [Column(IsPrimaryKey = true)]
            public string PK2 { get; set; }

            public int SomeInteger { get; set; }
            public int? SomeNullInteger { get; set; }
            public DateTime SomeDateTime { get; set; }
            public DateTime? SomeNullDateTime { get; set; }
            public DateTimeOffset SomeDateTimeOffset { get; set; }
            public DateTimeOffset? SomeNullDateTimeOffset { get; set; }
            public string SomeString { get; set; }
            public byte[] SomeByteArray { get; set; }

            [Column(AuditOption = AuditColumnOption.CreatedOn)]
            public DateTime crea_time_1 { get; set; }
            [Column(AuditOption = AuditColumnOption.CreatedOn)]
            public DateTime crea_time_2 { get; set; }
            [Column(AuditOption = AuditColumnOption.CreatedOn)]
            public DateTimeOffset crea_time_3 { get; set; }
            [Column(AuditOption = AuditColumnOption.CreatedOn)]
            public DateTimeOffset crea_time_4 { get; set; }
            [Column(AuditOption = AuditColumnOption.CreatedBy)]
            public string crea_user { get; set; }

            [Column(AuditOption = AuditColumnOption.UpdatedOn)]
            public DateTime updt_time_1 { get; set; }
            [Column(AuditOption = AuditColumnOption.UpdatedOn)]
            public DateTime updt_time_2 { get; set; }
            [Column(AuditOption = AuditColumnOption.UpdatedOn)]
            public DateTimeOffset updt_time_3 { get; set; }
            [Column(AuditOption = AuditColumnOption.UpdatedOn)]
            public DateTimeOffset updt_time_4 { get; set; }
            [Column(AuditOption = AuditColumnOption.UpdatedBy)]
            public string updt_user { get; set; }
        }

        [Test]
        public void PrimaryKey()
        {
            var config = AntFarmer.DefaultConfig;
            var dbInfo = new DatabaseProvider(config);
            var tbInfo = dbInfo.CreateTableProvider(typeof(ILTestClass));

            var row = new ILTestClass { PK1 = 1, PK2 = "Hello", SomeString = "BlahBlah" };

            var pk = tbInfo.PrimaryKeyGetter.Invoke(row);
            Assert.AreEqual(pk.Length, 2);
            Assert.AreEqual(pk, new object[] { 1, "Hello" });

            pk[0] = 2;
            pk[1] = "Bonjour";
            tbInfo.PrimaryKeySetter.Invoke(row, pk);
            Assert.AreEqual(row.PK1, 2);
            Assert.AreEqual(row.PK2, "Bonjour");
        }

        [Test]
        public void AuditColumns()
        {
            var config = AntFarmer.DefaultConfig;
            var dbInfo = new DatabaseProvider(config);
            var tbInfo = dbInfo.CreateTableProvider(typeof(ILTestClass));

            var row = new ILTestClass();

            var insertTime = DateTimeOffset.Now;
            tbInfo.InsertPreparer.Invoke(row, insertTime, "inserter");
            //Check insert audit fields...
            Assert.AreEqual(row.crea_user, "inserter");
            Assert.AreEqual(row.crea_time_1, insertTime.DateTime);
            Assert.AreEqual(row.crea_time_2, row.crea_time_1);
            Assert.AreEqual(row.crea_time_3, insertTime);
            Assert.AreEqual(row.crea_time_4, row.crea_time_3);

            var updateTime = insertTime.AddHours(1);
            tbInfo.UpdatePreparer.Invoke(row, updateTime, "updater");
            //Check update audit fields...
            Assert.AreEqual(row.updt_user, "updater");
            Assert.AreEqual(row.updt_time_1, updateTime.DateTime);
            Assert.AreEqual(row.updt_time_2, row.updt_time_1);
            Assert.AreEqual(row.updt_time_3, updateTime);
            Assert.AreEqual(row.updt_time_4, row.updt_time_3);
            //Confirm insert audit fields...
            Assert.AreEqual(row.crea_user, "inserter");
            Assert.AreEqual(row.crea_time_1, insertTime.DateTime);
            Assert.AreEqual(row.crea_time_2, row.crea_time_1);
            Assert.AreEqual(row.crea_time_3, insertTime);
            Assert.AreEqual(row.crea_time_4, row.crea_time_3);
        }

        //public class ILTestClass_DTO
        //{
        //    public int PK1 { get; set; }
        //    public string PK2 { get; set; }
        //}

        //public class ILTestClassEx : ILTestClass
        //{
        //    public string SomeOtherString { get; set; }
        //}


        //[Test(ExpectedResult = true)]
        //public bool Mapper()
        //{
        //    var dto = new ILTestClass_DTO
        //    {
        //        PK1 = 222,
        //        PK2 = "Hello",
        //    };
        //    var method1 = MSILGenerator.CreateFactoryAndMapper(typeof(ILTestClass), typeof(ILTestClass_DTO));
        //    var del1 = method1.CreateDelegate(typeof(Func<ILTestClass_DTO, ILTestClass>)) as Func<ILTestClass_DTO, ILTestClass>;
        //    var poco = del1.Invoke(dto);
        //    var method2 = MSILGenerator.CreateMapper(typeof(ILTestClass), typeof(ILTestClass_DTO));
        //    var del2 = method2.CreateDelegate(typeof(Action<object, object>)) as Action<object, object>;
        //    del2.Invoke(poco, dto);
        //    return (poco.PK1 == dto.PK1 && poco.PK2 == dto.PK2);
        //}

        //[Test(ExpectedResult = true)]
        //public bool Serialization()
        //{
        //    var serialMethod = MSILGenerator.CreateSerializer(typeof(ILTestClass));
        //    var deserialMethod = MSILGenerator.CreateDeserializer(typeof(ILTestClass));
        //    var serialize = serialMethod.CreateDelegate(typeof(Action<BinaryWriterEx, object>)) as Action<BinaryWriterEx, object>;
        //    var deserialze = deserialMethod.CreateDelegate(typeof(Func<BinaryReaderEx, object>)) as Func<BinaryReaderEx, object>;

        //    var poco1 = new ILTestClass
        //    {
        //        PK1 = 222,
        //        PK2 = "Hello",
        //        SomeDateTime = DateTime.Now,
        //        SomeNullDateTime = DateTime.Now,
        //        SomeByteArray = System.Text.Encoding.UTF8.GetBytes("La vie est un long fleuve")
        //    };
        //    byte[] content;
        //    {
        //        using (var s = new MemoryStream())
        //        using (var bw = new BinaryWriterEx(s))
        //        {
        //            serialize(bw, poco1);
        //            content = s.GetBuffer();
        //        }
        //    }
        //    ILTestClass poco2;
        //    {
        //        using (var s = new MemoryStream(content))
        //        using (var br = new BinaryReaderEx(s))
        //        {
        //            poco2 = deserialze(br) as ILTestClass;
        //        }
        //    }

        //    return (poco1.PK1 == poco2.PK1 && System.Text.Encoding.UTF8.GetString(poco2.SomeByteArray) == "La vie est un long fleuve");
        //}

        //[Test(ExpectedResult = true)]
        //public bool Test1()
        //{
        //    var m = MSILGenerator.CreateFactory(typeof(ILTestClassEx));
        //    var d = m.CreateDelegate(typeof(Func<object>)) as Func<object>;
        //    var o = d();
        //    return o != null;
        //}
    }
}