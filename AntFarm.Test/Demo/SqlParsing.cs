﻿using AntFarm.ORM;
using NUnit.Framework;

namespace AntFarm.Test.Demo
{
    [TestFixture]
    internal class SqlParsing
    {
        [TestCase("SqlServer")]
        public void UT_01(string csKey)
        {
            using (var db = new Database(csKey))
            {
                Sql sql;
                string str;
                //-----------------------------------------------------------
                sql = Sql.Builder.Append("select * from Country where 1=0");
                str = sql.ToString();
                Assert.DoesNotThrow(() => db.Execute(sql));
                //-----------------------------------------------------------
                sql = Sql.Builder.Append("select * from Country where [Name] = '' --Line Comment");
                str = sql.ToString();
                Assert.DoesNotThrow(() => db.Execute(sql));
                //-----------------------------------------------------------
                sql = Sql.Builder.Append("select * from Country where \"Name\" = '' --Line Comment\n");
                str = sql.ToString();
                Assert.DoesNotThrow(() => db.Execute(sql));
                //-----------------------------------------------------------
                sql = Sql.Builder.Append("select * from Country where [Name] = ?0--Line Comment\n--Param to ignore... ?1\r\n/*Comment*/", new object[] { null });
                str = sql.ToString();
                Assert.DoesNotThrow(() => db.Execute(sql));
                //-----------------------------------------------------------
                sql = Sql.Builder.Append(@"select * from Country where [Name] = ?0--Line Comment
                    --Param to ignore... ?1
                    /*Comment*/", new object[] { null });
                str = sql.ToString();
                Assert.DoesNotThrow(() => db.Execute(sql));
                //-----------------------------------------------------------
                sql = Sql.Builder
                    .Append("select * from Country where [Name] = /*Hi there ?0*/ ?0", "France")
                    .Append("--or [Name] = ?0", "Luxemburg")
                    .Append(1 == 1 ? "or [Name] = ?0" : "", "Germany")
                    .Append(1 == 0 ? "or [Name] = ?0" : "", "Poland")
                    .Append("/*Multi-line")
                    .Append("")
                    .Append("?0")
                    .Append("Comment*/")
                    .Append("or [Name] = ?0 or [Name] = ?1", "Belgium", "Spain")
                    ;
                str = sql.ToString();
                Assert.DoesNotThrow(() => db.Execute(sql));
            }
        }
    }
}
