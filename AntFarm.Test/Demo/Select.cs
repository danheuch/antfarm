﻿using AntFarm.Data.Test;
using AntFarm.ORM;
using NUnit.Framework;
using System;
using System.Linq;

namespace AntFarm.Test.Demo
{
    [TestFixture]
    internal class Select
    {
        private int __EnsureData(Database db)
        {
            int count = db.ExecuteScalar<int>("select count(*) from Country");
            if (count != 249)
                return Country.RepopulateTable(db);
            return count;
        }

        [TestCase("SqlServer")]
        [TestCase("Oracle")]
        [TestCase("MySql")]
        //[TestCase("Postgres")]
        [TestCase("SQLite")]
        public void UT_01_SelectAll(string csKey)
        {
            using (var db = new Database(csKey))
            {
                int count = __EnsureData(db);
                //--------------------------------------------------------------------------
                Assert.AreEqual(db.SelectAll<Country>().Count, count);
            }
        }

        [TestCase("SqlServer")]
        [TestCase("Oracle")]
        [TestCase("MySql")]
        //[TestCase("Postgres")]
        [TestCase("SQLite")]
        public void UT_02_EnumerateAll(string csKey)
        {
            using (var db = new Database(csKey))
            {
                int count = __EnsureData(db);
                //--------------------------------------------------------------------------
                Assert.AreEqual(db.EnumerateAll<Country>().Count(), count);
            }
        }

        [TestCase("SqlServer")]
        [TestCase("Oracle")]
        [TestCase("MySql")]
        //[TestCase("Postgres")]
        [TestCase("SQLite")]
        public void UT_03_SelectPK(string csKey)
        {
            using (var db = new Database(csKey))
            {
                int count = __EnsureData(db);
                string pk = db.ExecuteScalar<string>("select min(`Code2`) from Country");
                //--------------------------------------------------------------------------
                Assert.AreEqual(db.SelectPK<Country>(pk).Code2, pk);
                Assert.AreEqual(db.SelectPK<Country>(new[] { pk }).Code2, pk);
                //--------------------------------------------------------------------------
                Assert.Throws<InvalidOperationException>(() => db.SelectPK<object[]>(pk));
                Assert.Throws<InvalidOperationException>(() => db.SelectPK<dynamic>(pk));
            }
        }


        [TestCase("SqlServer")]
        [TestCase("Oracle")]
        [TestCase("MySql")]
        //[TestCase("Postgres")]
        [TestCase("SQLite")]
        public void UT_04_SelectOne(string csKey)
        {
            using (var db = new Database(csKey))
            {
                int count = __EnsureData(db);
                Sql sql;
                string pk = db.ExecuteScalar<string>("select min(`Code2`) from Country");
                //--------------------------------------------------------------------------
                sql = Sql.Builder.Append("select * from Country where `Code2` = ?0", pk);
                Assert.AreEqual(db.SelectOne<Country>($"select * from Country where `Code2` = '{pk}'").Code2, pk);
                Assert.AreEqual(db.SelectOne<Country>("select * from Country where `Code2` = ?0", pk).Code2, pk);
                Assert.AreEqual(db.SelectOne<Country>(sql).Code2, pk);
                Assert.AreEqual(db.SelectOne<Country>(x => x.Code2 == pk).Code2, pk);
                //--------------------------------------------------------------------------
                sql = Sql.Builder.Append("select * from Country where `Code2` = ?0", pk);
                Assert.AreEqual(db.SelectOne<object[]>(sql)[0], pk);
                Assert.AreEqual(db.SelectOne<dynamic>(sql).Code2, pk);
            }
        }

        [TestCase("SqlServer")]
        [TestCase("Oracle")]
        [TestCase("MySql")]
        //[TestCase("Postgres")]
        [TestCase("SQLite")]
        public void UT_05_SelectMany(string csKey)
        {
            using (var db = new Database(csKey))
            {
                int count = __EnsureData(db);
                string pk = db.ExecuteScalar<string>("select min(Code2) from Country");
                //--------------------------------------------------------------------------
                Assert.AreEqual(db.SelectMany<Country>("where Code2 = 'xyz'").Count, 0);
                Assert.AreEqual(db.SelectMany<Country>($"where Code2 = '{pk}'").Count, 1);
                Assert.AreEqual(db.SelectMany<Country>($"where Code2 <> '{pk}'").Count, count - 1);
                Assert.AreEqual(db.SelectMany<Country>($"where Code2 = ?0", pk).Count, 1);
                //--------------------------------------------------------------------------
                Assert.AreEqual(db.SelectMany<Country>(x => x.Code2 == "xyz").Count, 0);
                Assert.AreEqual(db.SelectMany<Country>(x => x.Code2 == pk).Count, 1);
                Assert.AreEqual(db.SelectMany<Country>(x => x.Code2 != pk).Count, count - 1);
                //--------------------------------------------------------------------------
                Assert.AreEqual(db.SelectMany<Country>(Sql.Builder.Append("where Code2 = 'xyz'")).Count, 0);
                Assert.AreEqual(db.SelectMany<Country>(Sql.Builder.Append($"where Code2 = '{pk}'")).Count, 1);
                Assert.AreEqual(db.SelectMany<Country>(Sql.Builder.Append($"where Code2 <> '{pk}'")).Count, count - 1);
                Assert.AreEqual(db.SelectMany<Country>(Sql.Builder.Append("where Code2 = ?0", pk)).Count, 1);
                //--------------------------------------------------------------------------
                Assert.AreEqual(db.SelectMany<object[]>(Sql.Builder.Append("select * from Country where Code2 = ?0", pk)).Count, 1);
                Assert.AreEqual(db.SelectMany<dynamic>(Sql.Builder.Append("select * from Country where Code2 = ?0", pk)).Count, 1);
            }
        }

        [TestCase("SqlServer")]
        [TestCase("Oracle")]
        [TestCase("MySql")]
        //[TestCase("Postgres")]
        [TestCase("SQLite")]
        public void UT_06_EnumerateMany(string csKey)
        {
            using (var db = new Database(csKey))
            {
                int count = __EnsureData(db);
                string pk = db.ExecuteScalar<string>("select min(Code2) from Country");
                //--------------------------------------------------------------------------
                Assert.AreEqual(db.EnumerateMany<Country>("where Code2 = 'xyz'").Count(), 0);
                Assert.AreEqual(db.EnumerateMany<Country>($"where Code2 = '{pk}'").Count(), 1);
                Assert.AreEqual(db.EnumerateMany<Country>($"where Code2 <> '{pk}'").Count(), count - 1);
                Assert.AreEqual(db.EnumerateMany<Country>($"where Code2 = ?0", pk).Count(), 1);
                //--------------------------------------------------------------------------
                Assert.AreEqual(db.EnumerateMany<Country>(x => x.Code2 == "xyz").Count(), 0);
                Assert.AreEqual(db.EnumerateMany<Country>(x => x.Code2 == pk).Count(), 1);
                Assert.AreEqual(db.EnumerateMany<Country>(x => x.Code2 != pk).Count(), count - 1);
                //--------------------------------------------------------------------------
                Assert.AreEqual(db.EnumerateMany<Country>(Sql.Builder.Append("where Code2 = 'xyz'")).Count(), 0);
                Assert.AreEqual(db.EnumerateMany<Country>(Sql.Builder.Append($"where Code2 = '{pk}'")).Count(), 1);
                Assert.AreEqual(db.EnumerateMany<Country>(Sql.Builder.Append($"where Code2 <> '{pk}'")).Count(), count - 1);
                Assert.AreEqual(db.EnumerateMany<Country>(Sql.Builder.Append("where Code2 = ?0", pk)).Count(), 1);
                //--------------------------------------------------------------------------
                Assert.AreEqual(db.EnumerateMany<object[]>(Sql.Builder.Append("select * from Country where Code2 = ?0", pk)).Count(), 1);
                Assert.AreEqual(db.EnumerateMany<dynamic>(Sql.Builder.Append("select * from Country where Code2 = ?0", pk)).Count(), 1);
            }
        }

        [TestCase("SqlServer")]
        [TestCase("Oracle")]
        [TestCase("MySql")]
        //[TestCase("Postgres")]
        [TestCase("SQLite")]
        public void UT_07_ExecuteScalar(string csKey)
        {
            using (var db = new Database(csKey))
            {
                int count = __EnsureData(db);
                //--------------------------------------------------------------------------
                Assert.AreEqual(db.ExecuteScalar<int>("select count(*) from Country"), count);
                Assert.AreEqual(db.ExecuteScalar<int?>("select count(*) from Country"), count);
                Assert.AreEqual(db.ExecuteScalar<short>("select count(*) from Country"), count);
                Assert.AreEqual(db.ExecuteScalar<string>("select count(*) from Country"), count.ToString());
                Assert.AreEqual(db.ExecuteScalar<object>("select count(*) from Country"), count);
                //--------------------------------------------------------------------------
                Assert.AreEqual(db.ExecuteScalar<int?>("select null from Country"), null);
                Assert.AreEqual(db.ExecuteScalar<short?>("select null from Country"), null);
                Assert.AreEqual(db.ExecuteScalar<object>("select null from Country"), null);
                //--------------------------------------------------------------------------
                Assert.Throws<InvalidCastException>(() => db.ExecuteScalar<int>("select null from Country"));
                Assert.Throws<InvalidCastException>(() => db.ExecuteScalar<short>("select null from Country"));
                Assert.Throws<FormatException>(() => db.ExecuteScalar<int>("select 'ABC' from Country"));
            }
        }

        [TestCase("SqlServer")]
        [TestCase("Oracle")]
        [TestCase("MySql")]
        //[TestCase("Postgres")]
        [TestCase("SQLite")]
        public void UT_08_AltRowTypes(string csKey)
        {
            using (var db = new Database(csKey))
            {
                int count = __EnsureData(db);
                Sql sql;
                string pk = db.ExecuteScalar<string>("select min(Code2) from Country");
                //--------------------------------------------------------------------------
                sql = Sql.Builder.Append("select Code2, Code3, \"Numeric\", \"Name\" from Country where Code2 = ?0", pk);
                var anon1 = db.SelectOne(sql, (dr) => new { Code2 = dr.GetString(0), Code3 = dr.GetString(1) });
                Assert.AreEqual(anon1.Code2, pk);
                //--------------------------------------------------------------------------
                sql = Sql.Builder.Append("select Code2, Code3, \"Numeric\", \"Name\" from Country where Code2 <> ?0", pk);
                var anonList1 = db.SelectMany(sql, (dr) => new { Code2 = dr.GetString(0), Code3 = dr.GetString(1) });
                Assert.AreEqual(anonList1.Count, count - 1);
                //--------------------------------------------------------------------------
                sql = Sql.Builder.Append("select Code2, Code3, \"Numeric\", \"Name\" from Country where Code2 = ?0", pk);
                var tuple1 = db.SelectOne<(string A, string B, string C, string D)>(sql);
                Assert.AreEqual(tuple1.A, pk);
                var tuple2 = db.SelectOne<(string A, string B, string C)>(sql);
                Assert.AreEqual(tuple2.A, pk);
                var tuple3 = db.SelectOne<(string A, string B, string C, string D, bool E, int F)>(sql);
                Assert.AreEqual(tuple2.A, pk);
            }
        }
    }
}
