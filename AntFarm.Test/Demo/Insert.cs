﻿using AntFarm.Data.Test;
using AntFarm.ORM;
using NUnit.Framework;
using System;
using System.Linq;

namespace AntFarm.Test.Demo
{
    [TestFixture]
    internal class BasicTests
    {
        [TestCase("SqlServer")]
        [TestCase("Oracle")]
        [TestCase("MySql")]
        [TestCase("Postgres")]
        [TestCase("SQLite")]
        public void UT_01_SimplePK(string csKey)
        {
            using (var db = new Database(csKey))
            {
                T01_SimplePK row;
                //--------------------------------------------------------------------------
                db.Truncate<T01_SimplePK>();
                //--------------------------------------------------------------------------
                row = new T01_SimplePK { Code = "A", Name = "Name of A" };
                Assert.DoesNotThrow(() => db.Insert(row));
                Assert.Catch(() => db.Insert(row));
            }
        }

        [TestCase("SqlServer")]
        [TestCase("Oracle")] 
        [TestCase("MySql")]
        //[TestCase("Postgres")]
        //[TestCase("SQLite")]
        public void UT_02_AutoNumberPK(string csKey)
        {
            using (var db = new Database(csKey))
            {
                T02_AutoNumberPK row;
                //--------------------------------------------------------------------------
                db.Truncate<T02_AutoNumberPK>();
                //--------------------------------------------------------------------------
                row = new T02_AutoNumberPK { ID = 0, Name = "X" };
                db.Insert(row);
                Assert.IsTrue(row.ID > 0);
                row.Name = $"Value of ID is {row.ID}.";
                db.Update(row);
            }
        }
    }
}
