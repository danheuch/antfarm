﻿using AntFarm.Providers.MySQL;
using AntFarm.Providers.Oracle;
using AntFarm.Providers.PostgreSQL;
using AntFarm.Providers.SQLite;
using AntFarm.Providers.SQLServer;
using NUnit.Framework;
using System;
using System.Globalization;

namespace AntFarm.Test
{
    [SetUpFixture]
    public partial class Startup
    {
        [OneTimeSetUp]
        public void RunBeforeAnyTests()
        {
            AntFarmer.Configure(config =>
            {
                config
                    .SetLoggerFactory(new NLog.Extensions.Logging.NLogLoggerFactory())
                    .EnableSqlLogging()
                    .AddConnectionString<SqlDatabaseProvider>("SqlServer", CS_Demo_SQLServer)
                    .AddConnectionString<OracleDatabaseProvider>("Oracle", CS_Demo_Oracle)
                    //.AddConnectionString<MySqlDatabaseProvider>("MySql", CS_Demo_MySQL)
                    //.AddConnectionString<PostgresDatabaseProvider>("Postgres", CS_Demo_PostgreSQL)
                    .AddConnectionString<SQLiteDatabaseProvider>("SQLite", CS_Demo_SQLite)
                    .SetDefaultUserName("UT")
                    .SetDefaultDataCulture(CultureInfo.GetCultureInfo("en-US"))
                    .SetDefaultDataTimeZone(TimeZoneInfo.Local)
                    .SetAsDefault();
            });
        }

        [OneTimeTearDown]
        public void RunAfterAnyTests()
        {
            //Nothing to do at the moment...
        }
    }
}
