﻿using AntFarm.ORM;
using AntFarm.Test.Databases.GRS.Axalta.Data.TIPS.DAL.MakeSDS;
using Microsoft.Extensions.Logging;
using NUnit.Framework;
using System;

namespace AntFarm.Test.Tests
{
    [TestFixture]
    internal class GRS
    {
        private readonly ILogger _Logger = AntFarmer.DefaultConfig.LoggerFactory.CreateLogger<GRS>();

        public void Try(Action action)
        {
            try
            {
                action();
            }
            catch (Exception ex)
            {
                _Logger.LogError(ex.Message, ex);
                _Logger.LogError(ex.ToString());
                throw;
            }
        }

        [Test]
        public void SpeedTest_01_ReadAllFromSDSBank()
        {
            Try(() =>
            {
                using (var db = new Database("GRS"))
                {
                    var list1 = db.SelectAll<SDSBank>();
                }
            });
        }

        [Test]
        public void SpeedTest_01_EnumAllFromSDSBank()
        {
            Try(() =>
            {
                using (var db = new Database("GRS"))
                    foreach (var item in db.SelectAll<SDSBank>())
                    {
                        var x = item;
                    }
            });
        }
    }
}