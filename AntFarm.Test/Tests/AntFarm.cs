﻿using AntFarm.ORM;
using AntFarm.Test.Databases.AntFarm;
using Microsoft.Extensions.Logging;
using NUnit.Framework;
using System;

namespace AntFarm.Test.Tests
{
    [TestFixture]
    internal class AntFarm
    {
        private readonly ILogger _Logger = AntFarmer.DefaultConfig.LoggerFactory.CreateLogger<AntFarm>();

        public void Try(Action action)
        {
            try
            {
                action();
            }
            catch (Exception ex)
            {
                _Logger.LogError(ex.Message, ex);
                _Logger.LogError(ex.ToString());
                throw;
            }
        }

        [Test]
        public void UT_01_GeneralTypes_SqlServer()
        {
            Try(() =>
            {
                using (var db = new Database("AntFarm"))
                {
                    var list1 = db.SelectAll<GeneralTypes>();
                }
            });
        }

        [Test]
        public void UT_02_ProviderTypes_SqlServer()
        {
            Try(() =>
            {
                using (var db = new Database("AntFarm"))
                {
                    db.Truncate<Databases.AntFarm.ProviderTypes>();
                    for (int i = 1; i <= 100; i++)
                        db.Insert(Databases.AntFarm.ProviderTypes.Create_Random(i));
                    var list1 = db.SelectAll<Databases.AntFarm.ProviderTypes>();
                }
            });
        }

        [Test]
        public void UT_02_ProviderTypes_Oracle()
        {
            Try(() =>
            {
                using (var db = new Database("APP_DATA"))
                {
                    db.Truncate<Databases.TIPS.APP_DATA.ProviderTypes>();
                    for (int i = 1; i <= 10; i++)
                        db.Insert(Databases.TIPS.APP_DATA.ProviderTypes.Create_Random(i));
                    var list1 = db.SelectAll<Databases.TIPS.APP_DATA.ProviderTypes>();
                }
            });
        }

#if false
        [Test]
        public void BasicTest()
        {
            Try(() =>
            {
                const int count = 100;
                using (var db = new Database(_Config, "Default"))
                {
                    db.Execute("truncate table dbo.[UT_Basic]");
                    db.Execute("truncate table dbo.[UT_CompoundPK]");
                    db.Insert(UT_Basic.Create_Empty());
                    db.Insert(UT_Basic.Create_Min());
                    db.Insert(UT_Basic.Create_Max());
                    for (int i = 0; i < count; i++)
                        db.Insert(UT_Basic.Create_Random());
                    db.Insert(UT_CompoundPK.Create_Empty());
                    db.Insert(UT_CompoundPK.Create_Min());
                    db.Insert(UT_CompoundPK.Create_Max());
                    for (int i = 0; i < count; i++)
                        db.Insert(UT_CompoundPK.Create_Random());
                    var list1 = db.SelectAll<UT_Basic>();
                    var list2 = db.SelectAll<UT_CompoundPK>();
                }
            });
        }

        [Test]
        public void UsersAndGroups_ORM()
        {
            Try(() =>
            {
                using (var db = new Database(_Config, "Default"))
                {
                    db.Execute("delete from dbo.[UsersAndGroups]");
                    db.Execute("delete from dbo.[Users]");
                    db.Execute("delete from dbo.[Groups]");
                    db.Insert(new User { Name = "Christophe Danheux", });
                    db.Insert(new User { Name = "Dan Foster", });
                    db.Insert(new User { Name = "Brian O'Sullivan", });
                    db.Insert(new User { Name = "Brian Devlin", });
                    db.Insert(new Group { CD = "TIPS", Name = "TIPS Admins" });
                    db.Insert(new Group { CD = "GRS", Name = "GRS Admins" });
                    var chmd = db.SelectOne<User>(x => x.Name == "Christophe Danheux");
                    db.Insert(new UserAndGroup { Group_CD = "TIPS", User_ID = chmd.ID });
                    db.Insert(new UserAndGroup { Group_CD = "GRS", User_ID = chmd.ID });
                    db.Insert(new UserAndGroup { Group_CD = "TIPS", User_ID = db.SelectOne<User>(x => x.Name == "Dan Foster").ID });
                    db.Insert(new UserAndGroup { Group_CD = "TIPS", User_ID = db.SelectOne<User>(x => x.Name == "Brian O'Sullivan").ID });
                    db.Insert(new UserAndGroup { Group_CD = "TIPS", User_ID = db.SelectOne<User>(x => x.Name == "Brian Devlin").ID });
                    var list1 = db.SelectAll<User>();
                    var list2 = db.SelectAll<Group>();
                    var list3 = db.SelectAll<UserAndGroup>();
                }
            });
        }

        [Test]
        public void UsersAndGroups_Model()
        {
            Try(() =>
            {
                //Set the test up by clearing the data and populating the test tables with basic records.
                using (var db = new Database(_Config, "Default"))
                {
                    db.Execute("delete from dbo.[UsersAndGroups]");
                    db.Execute("delete from dbo.[Users]");
                    db.Execute("delete from dbo.[Groups]");
                    db.Insert(new Group { CD = "TIPS", Name = "TIPS Admins" });
                    db.Insert(new Group { CD = "GRS", Name = "GRS Admins" });
                    db.Insert(new Group { CD = "Sphera", Name = "Sphera Admins" });
                }

                //Actual test starts here, by creating the model.
                var model = new ModelBase(_Config, "Default");
                var users = model.Add<User>();
                var memberships = model.Add<UserAndGroup>();
                users.MergeWith(new { Name = "Christophe Danheux" });
                Debug.Assert(users.Count() == 1 && users.CNM.Count() == 1);
                var groups1 = new[] { "TIPS", "GRS" };
                memberships.MergeWith(groups1.Select(x => new { Group_CD = x, User_ID = users.CNM.Single().ID }));
                Debug.Assert(memberships.Count() == 2 && memberships.CNM.Count() == 2);
                var groups2 = new[] { "TIPS", "GRS" };
                memberships.MergeWith(groups2.Select(x => new { Group_CD = x, User_ID = users.CNM.Single().ID }));
                Debug.Assert(memberships.Count() == 2 && memberships.CNM.Count() == 2);
                var groups3 = new[] { "Sphera", "GRS" };
                var options3 = new MergeOptions { AllowDelete = false };
                memberships.MergeWith(groups3.Select(x => new { Group_CD = x, User_ID = users.CNM.Single().ID }), options3);
                Debug.Assert(memberships.Count() == 3 && memberships.CNM.Count() == 3);

                model.Save();

                users.MergeWith(new { Name = "Christophe Danheux" });
                Debug.Assert(users.Count() == 2 && users.CNM.Count() == 1);
                Debug.Assert(memberships.Count() == 3 && memberships.CNM.Count() == 0);
                memberships.MergeWith(groups1.Select(x => new { Group_CD = x, User_ID = users.CNM.Single().ID }));
                Debug.Assert(memberships.Count() == 5 && memberships.CNM.Count() == 2);
                users.MergeWith(new { users.CNM.Single().ID, Name = "Christophe Henri Danheux" });
                Debug.Assert(memberships.Count() == 5 && memberships.CNM.Count() == 2);

                model.Save();
            });
        }
#endif
    }
}