﻿using AntFarm.Data.Demo;
using AntFarm.ORM;
using NUnit.Framework;
using System;
using System.Linq;

namespace AntFarm.Test.Tests.Demo
{
    [TestFixture]
    internal class UnitTests1
    {
        [TestCase("Demo.MSSQL")]
        public void UT_01_Database_SelectAll(string csKey)
        {
            using (var db = new Database(csKey))
            {
                Assert.AreEqual(db.SelectAll<Country>().Count, db.ExecuteScalar<int>("select count(*) from country"));
                Assert.AreEqual(db.SelectAll<object[]>().Count, db.ExecuteScalar<int>("select count(*) from country"));
                Assert.AreEqual(db.SelectAll<dynamic>().Count, db.ExecuteScalar<int>("select count(*) from country"));
            }
        }

        [TestCase("Demo.MSSQL")]
        public void UT_02_Database_SelectPK(string csKey)
        {
            using (var db = new Database(csKey))
            {
                string pk = db.ExecuteScalar<string>("select min([Alpha-2]) from country");
                Assert.Throws<InvalidOperationException>(() => db.SelectPK<object[]>(pk));
                Assert.Throws<InvalidOperationException>(() => db.SelectPK<dynamic>(pk));
                Assert.AreEqual(db.SelectPK<Country>(pk).Alpha_2, pk);
            }
        }


        [TestCase("Demo.MSSQL")]
        public void UT_03_Database_SelectOne(string csKey)
        {
            using (var db = new Database(csKey))
            {
                string pk = db.ExecuteScalar<string>("select min([Alpha-2]) from country");
                Assert.AreEqual(db.SelectOne<Country>($"select * from country where [Alpha-2] = '{pk}'").Alpha_2, pk);
                Assert.AreEqual(db.SelectOne<Country>($"select * from country where [Alpha-2] = ?0", pk).Alpha_2, pk);
                Assert.AreEqual(db.SelectOne<Country>(Sql.Builder.Append("where [Alpha-2] = ?0", pk)).Alpha_2, pk);
                Assert.AreEqual(db.SelectOne<Country>(x => x.Alpha_2 == pk).Alpha_2, pk);
                // API4
                Assert.AreEqual(db.SelectOne<object[]>(Sql.Builder.Append("select * from country where [Alpha-2] = ?0", pk))[0], pk);
                Assert.AreEqual(db.SelectOne<dynamic>(Sql.Builder.Append("select * from country where [Alpha-2] = ?0", pk)).Alpha_2, pk);
            }
        }

        [TestCase("Demo.MSSQL")]
        public void UT_04_Database_SelectMany(string csKey)
        {
            using (var db = new Database(csKey))
            {
                //
                int count = db.ExecuteScalar<int>("select count(*) from country");
                string pk = db.ExecuteScalar<string>("select min([Alpha-2]) from country");
                // API1
                Assert.AreEqual(db.SelectMany<Country>("where [Alpha-2] = 'xyz'").Count, 0);
                Assert.AreEqual(db.SelectMany<Country>($"where [Alpha-2] = '{pk}'").Count, 1);
                Assert.AreEqual(db.SelectMany<Country>($"where [Alpha-2] <> '{pk}'").Count, count - 1);
                Assert.AreEqual(db.SelectMany<Country>($"where [Alpha-2] = ?0", pk).Count, 1);
                // API2
                Assert.AreEqual(db.SelectMany<Country>(x => x.Alpha_2 == "xyz").Count, 0);
                Assert.AreEqual(db.SelectMany<Country>(x => x.Alpha_2 == pk).Count, 1);
                Assert.AreEqual(db.SelectMany<Country>(x => x.Alpha_2 != pk).Count, count - 1);
                // API3
                Assert.AreEqual(db.SelectMany<Country>(Sql.Builder.Append("where [Alpha-2] = 'xyz'")).Count, 0);
                Assert.AreEqual(db.SelectMany<Country>(Sql.Builder.Append($"where [Alpha-2] = '{pk}'")).Count, 1);
                Assert.AreEqual(db.SelectMany<Country>(Sql.Builder.Append($"where [Alpha-2] <> '{pk}'")).Count, count - 1);
                Assert.AreEqual(db.SelectMany<Country>(Sql.Builder.Append("where [Alpha-2] = ?0", pk)).Count, 1);
                // API4
                db.SelectMany<object[]>(Sql.Builder.Append("select * from country where [Alpha-2] = ?0", pk));
                db.SelectMany<dynamic>(Sql.Builder.Append("select * from country where [Alpha-2] = ?0", pk));
                // API5
                Assert.AreEqual(db.SelectMany<object[]>(Sql.Builder.Append("select * from country where [Alpha-2] = ?0", pk)).Count, 1);
                Assert.AreEqual(db.SelectMany<dynamic>(Sql.Builder.Append("select * from country where [Alpha-2] = ?0", pk)).Count, 1);
            }
        }

        [TestCase("Demo.MSSQL")]
        public void UT_05_Database_ExecuteScalar(string csKey)
        {
            using (var db = new Database(csKey))
            {
                var list1 = db.SelectAll<Country>();
                object res;
                res = db.ExecuteScalar<int>("select count(*) from country");
                res = db.ExecuteScalar<int?>("select count(*) from country");
                res = db.ExecuteScalar<short>("select count(*) from country");
                res = db.ExecuteScalar<short?>("select count(*) from country");
                res = Assert.Throws<InvalidCastException>(() => db.ExecuteScalar<int>("select null from country"));
                res = db.ExecuteScalar<int?>("select null from country");
                res = Assert.Throws<InvalidCastException>(() => db.ExecuteScalar<short>("select null from country"));
                res = db.ExecuteScalar<short?>("select null from country");
                res = Assert.Throws<FormatException>(() => db.ExecuteScalar<int>("select 'ABC' from country"));
                res = db.ExecuteScalar<object>("select count(*) from country");
                res = db.ExecuteScalar<object>("select null from country");
            }
        }
    }
}
