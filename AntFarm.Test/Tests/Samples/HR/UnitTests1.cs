﻿using AntFarm.Data.Samples.HR;
using AntFarm.ORM;
using NUnit.Framework;
using System;
using System.Linq;

namespace AntFarm.Test.Tests.Samples.HR
{
    [TestFixture]
    internal class UnitTests1
    {
        /// <summary>
        /// In the HR database (Samples):
        /// SelectAll test.
        /// </summary>
        [TestCase("Sample.HR.MSSQL")]
        [TestCase("Sample.HR.MYSQL")]
        public void UT_01_Database_SelectAll(string csKey)
        {
            using (var db = new Database(csKey))
            {
                var list1 = db.SelectAll<Job>();
                Assert.AreEqual(list1.Count, db.ExecuteScalar<int>("select count(*) from jobs"));
                var list2 = db.SelectAll<Region>();
                Assert.AreEqual(list2.Count, db.ExecuteScalar<int>("select count(*) from regions"));
                var list3 = db.SelectAll<Country>();
                Assert.AreEqual(list3.Count, db.ExecuteScalar<int>("select count(*) from countries"));
                var list4 = db.SelectAll<Location>();
                Assert.AreEqual(list4.Count, db.ExecuteScalar<int>("select count(*) from locations"));
                var list5 = db.SelectAll<Department>();
                Assert.AreEqual(list5.Count, db.ExecuteScalar<int>("select count(*) from departments"));
                var list6 = db.SelectAll<Employee>();
                Assert.AreEqual(list6.Count, db.ExecuteScalar<int>("select count(*) from employees"));
                var list7 = db.SelectAll<Dependent>();
                Assert.AreEqual(list7.Count, db.ExecuteScalar<int>("select count(*) from dependents"));
            }
        }

        /// <summary>
        /// In the HR database (Samples):
        /// SelectPK test.
        /// </summary>
        [TestCase("Sample.HR.MSSQL")]
        [TestCase("Sample.HR.MYSQL")]
        public void UT_02_Database_SelectPK(string csKey)
        {
            using (var db = new Database(csKey))
            {
                int id = db.ExecuteScalar<int>("select min(job_id) from jobs");
                var item1 = db.SelectPK<Job>(id);
                Assert.AreEqual(item1.job_id, id);
            }
        }


        /// <summary>
        /// In the HR database (Samples):
        /// SelectOne test.
        /// </summary>
        [TestCase("Sample.HR.MSSQL")]
        [TestCase("Sample.HR.MYSQL")]
        public void UT_03_Database_SelectOne(string csKey)
        {
            using (var db = new Database(csKey))
            {
                int id = db.ExecuteScalar<int>("select min(job_id) from jobs");
                Assert.AreEqual(db.SelectOne<Job>($"select * from jobs where job_id = {id}").job_id, id);
                Assert.AreEqual(db.SelectOne<Job>($"select * from jobs where job_id = ?0", id).job_id, id);
                Assert.AreEqual(db.SelectOne<Job>(Sql.Builder.Append("where job_id = ?0", id)).job_id, id);
                Assert.AreEqual(db.SelectOne<Job>(x => x.job_id == id).job_id, id);
            }
        }

        /// <summary>
        /// In the HR database (Samples):
        /// SelectMany test.
        /// </summary>
        [TestCase("Sample.HR.MSSQL")]
        [TestCase("Sample.HR.MYSQL")]
        public void UT_04_Database_SelectMany(string csKey)
        {
            using (var db = new Database(csKey))
            {
                int count = db.ExecuteScalar<int>("select count(*) from countries");
                string first_id = db.ExecuteScalar<string>("select min(country_id) from countries");
                // API1
                Assert.AreEqual(db.SelectMany<Country>("where country_id = 'xyz'").Count, 0);
                Assert.AreEqual(db.SelectMany<Country>($"where country_id = '{first_id}'").Count, 1);
                Assert.AreEqual(db.SelectMany<Country>($"where country_id <> '{first_id}'").Count, count - 1);
                Assert.AreEqual(db.SelectMany<Country>($"where country_id = ?0", first_id).Count, count - 1);
                // API2
                Assert.AreEqual(db.SelectMany<Country>(x => x.country_id == "xyz").Count, 0);
                Assert.AreEqual(db.SelectMany<Country>(x => x.country_id == first_id).Count, 1);
                Assert.AreEqual(db.SelectMany<Country>(x => x.country_id != first_id).Count, count - 1);
                // API3
                Assert.AreEqual(db.SelectMany<Country>(Sql.Builder.Append("where country_id = ?0", first_id)).Count, 1);
            }
        }

        /// <summary>
        /// In the HR database (Samples):
        /// ExecuteScalar test.
        /// </summary>
        [TestCase("Sample.HR.MSSQL")]
        [TestCase("Sample.HR.MYSQL")]
        public void UT_05_Database_ExecuteScalar(string csKey)
        {
            using (var db = new Database(csKey))
            {
                var list1 = db.SelectAll<Job>();
                object res;
                res = db.ExecuteScalar<int>("select count(*) from jobs");
                res = db.ExecuteScalar<int?>("select count(*) from jobs");
                res = db.ExecuteScalar<short>("select count(*) from jobs");
                res = db.ExecuteScalar<short?>("select count(*) from jobs");
                res = Assert.Throws<InvalidCastException>(() => db.ExecuteScalar<int>("select null from jobs"));
                res = db.ExecuteScalar<int?>("select null from jobs");
                res = Assert.Throws<InvalidCastException>(() => db.ExecuteScalar<short>("select null from jobs"));
                res = db.ExecuteScalar<short?>("select null from jobs");
                res = Assert.Throws<FormatException>(() => db.ExecuteScalar<int>("select 'ABC' from jobs"));
                res = db.ExecuteScalar<object>("select count(*) from jobs");
                res = db.ExecuteScalar<object>("select null from jobs");
            }
        }

        /// <summary>
        /// In the HR database (Samples):
        /// Selects all the rows from every table, delete them and re-insert them.
        /// </summary>
        [Test]
        public void UT_09_Database_SelectAll_DeleteAll_And_ReInsertAll()
        {
            using (var db = new Database("Sample.HR"))
            {
                var jobs = db.SelectAll<Job>();
                var regions = db.SelectAll<Region>();
                var countries = db.SelectAll<Country>();
                var locations = db.SelectAll<Location>();
                var departments = db.SelectAll<Department>();
                var employees = db.SelectAll<Employee>();
                var dependents = db.SelectAll<Dependent>();
                db.BeginTransaction();
                try
                {
                    db.DeleteAll<Dependent>();
                    db.DeleteAll<Employee>();
                    db.DeleteAll<Department>();
                    db.DeleteAll<Location>();
                    db.DeleteAll<Country>();
                    db.DeleteAll<Region>();
                    db.DeleteAll<Job>();
                    foreach (var job in jobs)
                    {
                        int oldId = job.job_id;
                        db.Insert(job);
                        foreach (var employee in employees.Where(x => x.job_id == oldId))
                            employee.job_id = job.job_id;
                    }
                    foreach (var region in regions)
                    {
                        int oldId = region.region_id;
                        db.Insert(region);
                        foreach (var country in countries.Where(x => x.region_id == oldId))
                            country.region_id = region.region_id;
                    }
                    foreach (var country in countries)
                    {
                        db.Insert(country);
                    }
                    foreach (var location in locations)
                    {
                        int oldId = location.location_id;
                        db.Insert(location);
                        foreach (var department in departments.Where(x => x.location_id == oldId))
                            department.location_id = location.location_id;
                    }
                    foreach (var department in departments)
                    {
                        int oldId = department.department_id;
                        db.Insert(department);
                        foreach (var employee in employees.Where(x => x.department_id == oldId))
                            employee.department_id = department.department_id;
                    }
                    foreach (var employee in employees)
                    {
                        int oldId = employee.employee_id;
                        db.Insert(employee);
                        foreach (var sub_employee in employees.Where(x => x.manager_id == oldId))
                            sub_employee.manager_id = employee.employee_id;
                        foreach (var dependent in dependents.Where(x => x.employee_id == oldId))
                            dependent.employee_id = employee.employee_id;
                    }
                    foreach (var dependent in dependents)
                    {
                        db.Insert(dependent);
                    }
                    db.CommitTransaction();
                }
                catch
                {
                    db.RollbackTransaction();
                    throw;
                }
            }
        }


        /// <summary>
        /// In the HR database (Samples):
        /// SelectAll test.
        /// </summary>
        [TestCase("Sample.HR.MSSQL")]
        public void UT_08_Database_SelectAll_v2(string csKey)
        {
            using (var db = new Database(csKey))
            {
                var rows = db.EnumerateAll<Job>();
                foreach (var row in rows)
                {
                    int id = row.job_id;
                }
            }
        }
    }
}
