﻿using AxORM.Providers.Oracle;
using AxORM.Test.Databases.TIPSQA.APP_DATA;
using NUnit.Framework;
using System;

namespace AxORM.Test.Tests
{
    [TestFixture]
    class Oracle
    {
        private readonly GeneralConfig _Config = AxORMManager.Configure(c =>
        {
            const string cs = "Data Source=GRS_QA;User ID=APP_DATA;Password=j9kezsdb;Pooling=True;Connection Timeout=30;Connection Lifetime=0;Validate Connection=False;";
            c.SetLoggerFactory(new NLog.Extensions.Logging.NLogLoggerFactory());
            c.AddConnectionInfo<OracleDatabaseProvider>("Default", cs);
        });

        [Test]
        public void Select_All()
        {
            using (var repo = new Database(_Config, "Default"))
            {
                var records = repo.SelectAll<STATE_JOBTRIGGERS>();
            }
        }

        [Test]
        [TestCase("FolderCleanup")]
        public void Select_ByPK(string name)
        {
            using (var repo = new Database(_Config, "Default"))
            {
                var record = repo.SelectOne<STATE_JOBTRIGGERS>(new[] { name });
                record.UPDATEDON = DateTime.Now;
                repo.Update(record);
            }
        }

        [Test]
        [TestCase("FolderCleanup")]
        public void Select_ByLambda(string name)
        {
            using (var repo = new Database(_Config, "Default"))
            {
                var records = repo.SelectMany<STATE_JOBTRIGGERS>(x => x.TRIGGERNAME == name);
                foreach (var record in records)
                {
                    record.UPDATEDON = DateTime.Now;
                    repo.Update(record);
                }
            }
        }

        [Test]
        public void Insert()
        {
            using (var repo = new Database(_Config, "Default"))
            {
                var record = new TABLE1
                {
                    TXT = "This is a test.",
                };
                repo.Insert(record);
                repo.Delete(record);
            }
        }
    }
}
