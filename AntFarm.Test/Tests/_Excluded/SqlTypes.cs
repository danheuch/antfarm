﻿using NUnit.Framework;
using Cascade.ORM;
using Cascade.Test.Databases;
using Cascade.Providers.SqlServer;
using System;
using System.Collections.Generic;
using System.Text;

namespace Cascade.Test.Tests
{
    [TestFixture]
    class SqlTypes
    {
        private readonly GeneralConfig _Config = xxx.Configure(c =>
        {
            const string cs = "Integrated Security=SSPI;Persist Security Info=False;Initial Catalog=xxx;Data Source=localhost";
            c.SetLoggerFactory(new NLog.Extensions.Logging.NLogLoggerFactory());
            c.AddConnectionInfo<SqlDatabaseProvider>("Default", cs);
        });

        [Test]
        public void Select_All()
        {
            using (var repo = new Database(_Config, "Default"))
            {
                var records = repo.SelectAll<Test1>();
            }
        }

        [Test]
        public void Insert_EmptySmallAndBig()
        {
            using (var repo = new Database(_Config, "Default"))
            {
                var minSQLDateTime = new DateTime(1753, 1, 1);
                var minSQLSmallDateTime = new DateTime(1900, 1, 1);
                var maxSQLSmallDateTime = new DateTime(2079, 6, 6); //some time through the day - not sure exactly what time

                Test1 record;
                record = new Test1
                {
                    #region Empty values
                    //Date/Time
                    date_field = DateTime.Now,
                    datetime2_field = DateTime.Now,
                    datetimeoffset_field = new DateTimeOffset(),
                    datetime_field = DateTime.Now,
                    smalldatetime_field = DateTime.Now,
                    time_field = DateTime.Now,
                    //Numeric
                    bigint_field = 0,
                    bit_field = false,
                    decimal_field = 0,
                    float_field = 0,
                    int_field = 0,
                    money_field = 0,
                    numeric_field = 0,
                    real_field = 0,
                    smallint_field = 0,
                    smallmoney_field = 0,
                    tinyint_field = 0,
                    //String
                    char_field = "",
                    nchar_field = "",
                    ntext_field = "",
                    nvarchar_50_field = "",
                    nvarchar_max_field = "",
                    text_field = "",
                    varchar_50_field = "",
                    varchar_max_field = "",
                    #endregion
                };
                repo.Insert(record);

                record = new Test1
                {
                    #region Small values
                    //Date/Time
                    date_field = minSQLDateTime,
                    datetime2_field = minSQLDateTime, //Doc says min value is 0001-01-01 00:00:00, but does not appear to be true. See http://sqlhints.com/tag/datetime-vs-datetime2/
                    datetimeoffset_field = DateTimeOffset.MinValue,
                    datetime_field = minSQLDateTime,
                    smalldatetime_field = minSQLSmallDateTime,
                    time_field = minSQLDateTime,
                    //Numeric
                    bigint_field = long.MinValue,
                    bit_field = false,
                    decimal_field = -900.1m, //max/min depends on definition
                    float_field = double.MinValue,
                    int_field = int.MinValue,
                    money_field = -922337203685477.5808m,
                    numeric_field = -222.222m, //max/min depends on definition
                    real_field = float.MinValue,
                    smallint_field = short.MinValue,
                    smallmoney_field = -214748.3648m,
                    tinyint_field = byte.MinValue,
                    //String
                    char_field = "abc",
                    nchar_field = "abc",
                    ntext_field = "abc",
                    nvarchar_50_field = "abc",
                    nvarchar_max_field = "abc",
                    text_field = "abc",
                    varchar_50_field = "abc",
                    varchar_max_field = "abc",
                    #endregion
                };
                repo.Insert(record);

                record = new Test1
                {
                    #region Big values
                    //Date/Time
                    date_field = DateTime.MaxValue,
                    datetime2_field = DateTime.MaxValue,
                    datetimeoffset_field = DateTimeOffset.MaxValue,
                    datetime_field = DateTime.MaxValue,
                    smalldatetime_field = maxSQLSmallDateTime,
                    time_field = DateTime.MaxValue,
                    //Numeric
                    bigint_field = long.MaxValue,
                    bit_field = false,
                    decimal_field = 900.1m, //max/min depends on definition
                    float_field = double.MaxValue,
                    int_field = int.MaxValue,
                    money_field = 922337203685477.5807m,
                    numeric_field = 222.222m, //max/min depends on definition
                    real_field = float.MaxValue,
                    smallint_field = short.MaxValue,
                    smallmoney_field = 214748.3647m,
                    tinyint_field = byte.MaxValue,
                    //String
                    char_field = "生命是一条长河。", //works, but writes gibberish
                    nchar_field = "生命是一条长河。",
                    ntext_field = "生命是一条长河。",
                    nvarchar_50_field = "生命是一条长河。",
                    nvarchar_max_field = "生命是一条长河。",
                    text_field = "生命是一条长河。", //works, but writes gibberish
                    varchar_50_field = "生命是一条长河。", //works, but writes gibberish
                    varchar_max_field = "生命是一条长河。", //works, but writes gibberish
                    #endregion
                };
                repo.Insert(record);
            }
        }
    }
}