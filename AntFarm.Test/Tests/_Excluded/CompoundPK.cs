﻿using NUnit.Framework;
using AxORM.Test.Databases.AxORM;
using AxORM.Providers.SqlServer;
using System;
using System.Collections.Generic;
using System.Text;
using AxORM.Linq;
using System.Linq;
using System.Diagnostics;

namespace AxORM.Test.Tests
{
    [TestFixture]
    class CompoundPK
    {
        private readonly GeneralConfig _Config = AxORMManager.Configure(c =>
        {
            const string cs = "Integrated Security=SSPI;Persist Security Info=False;Initial Catalog=AxORM;Data Source=localhost";
            c.SetLoggerFactory(new NLog.Extensions.Logging.NLogLoggerFactory());
            c.AddConnectionInfo<SqlDatabaseProvider>("Default", cs);
        });

        [Test]
        [TestCase()]
        public void Select_All()
        {
            using (var repo = new Database(_Config, "Default"))
            {
                var records = repo.SelectAll<Test2>();
            }
        }

        [Test]
        [TestCase()]
        public void Insert_CompoundPrimaryKey()
        {
            using (var repo = new Database(_Config, "Default"))
            {
                Test2 record;
                record = new Test2
                {
                    Code = "ABC",
                    SubCode = "XDE",
                    Description = "This is a test"
                };
                repo.Insert(record);
            }
        }

        [Test]
        public void Query1()
        {
            using (var repo = new Database(_Config, "Default"))
            {
                var records = repo.SelectMany<Test2>(x => x.Code == "ABC");
            }
        }

        [Test]
        [TestCase("ABC")]
        public void Query2(string code)
        {
            using (var repo = new Database(_Config, "Default"))
            {
                var records = repo.SelectMany<Test2>(x => x.Code == code);
            }
        }

        [Test]
        [TestCase("ABC")]
        public void Query3(string code)
        {
            var tuple = new Tuple<string, string>(code, "");
            using (var repo = new Database(_Config, "Default"))
            {
                var records = repo.SelectMany<Test2>(x => x.Code == tuple.Item1);
            }
        }

        [Test]
        [TestCase("ABC")]
        public void Query4(string code)
        {
            var list = new string[] { code, "DEF" };
            using (var repo = new Database(_Config, "Default"))
            {
                var records = repo.SelectMany<Test2>(x => x.Code == list[0]); //TODO: does not work
            }
        }

        [Test]
        [TestCase("ABC")]
        public void Query5(string code)
        {
            var list = new List<string> { code, "DEF" };
            using (var repo = new Database(_Config, "Default"))
            {
                var records = repo.SelectMany<Test2>(x => x.Code == list[0]); //TODO: does not work
            }
        }
    }
}