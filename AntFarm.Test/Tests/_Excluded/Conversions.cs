﻿using NUnit.Framework;
using AxORM;
using AxORM.Test.Databases.AxORM;
using AxORM.Providers.SqlServer;
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

namespace AxORM.Test.Tests
{
    [Table("Test1")]
    class Test1WithDifferentTypes
    {
        [Column(IsPrimaryKey = true, IsAutoNumber = true)]
        public int ID { get; set; }
        public long? bigint_field { get; set; }
        public string int_field { get; set; }
        public DateTime? time_field { get; set; }
#if false
        public byte[] binary_field { get; set; }
        public bool? bit_field { get; set; }
        public string char_field { get; set; }
        [Column(DbType.Date)]
        public DateTime? date_field { get; set; }
        [Column(DbType.DateTime)]
        public DateTime? datetime_field { get; set; }
        [Column(DbType.DateTime2)]
        public DateTime? datetime2_field { get; set; }
        public DateTimeOffset? datetimeoffset_field { get; set; }
        public decimal? decimal_field { get; set; }
        public double? float_field { get; set; }
        //public object geography_field { get; set; }
        //public object geometry_field { get; set; }
        //public object hierarchyid_field { get; set; }
        public byte[] image_field { get; set; }
        public int? int_field { get; set; }
        public decimal? money_field { get; set; }
        public string nchar_field { get; set; }
        public string ntext_field { get; set; }
        public decimal? numeric_field { get; set; }
        public string nvarchar_50_field { get; set; }
        public string nvarchar_max_field { get; set; }
        public float? real_field { get; set; }
        public DateTime? smalldatetime_field { get; set; }
        public short? smallint_field { get; set; }
        public decimal? smallmoney_field { get; set; }
        public object sql_variant_field { get; set; }
        public string text_field { get; set; }
        public DateTime? time_field { get; set; }
        [Column(IsReadOnly = true)]
        public byte[] timestamp_field { get; set; }
        public byte? tinyint_field { get; set; }
        public Guid? uniqueidentifier_field { get; set; }
        public byte[] varbinary_50_field { get; set; }
        public byte[] varbinary_max_field { get; set; }
        public string varchar_50_field { get; set; }
        public string varchar_max_field { get; set; }
#endif
    }

    [TestFixture]
    class Conversions
    {
        private readonly GeneralConfig _Config = AxORMManager.Configure(c =>
        {
            const string cs = "Integrated Security=SSPI;Persist Security Info=False;Initial Catalog=AxORM;Data Source=localhost";
            c.SetLoggerFactory(new NLog.Extensions.Logging.NLogLoggerFactory());
            c.AddConnectionInfo<SqlDatabaseProvider>("Default", cs);
        });

        [Test]
        public void Select_All()
        {
            using (var repo = new Database(_Config, "Default"))
            {
                var records = repo.SelectAll<Test1WithDifferentTypes>();
            }
        }
    }
}