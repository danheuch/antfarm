﻿using Cascade.Models;
using Cascade.ORM;
using Cascade.Providers.SqlServer;
using Cascade.Test.Databases.SqlServer.Phoenix;
using Microsoft.Extensions.Logging;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Cascade.Test.Tests.SqlServer
{
    [TestFixture]
    internal class Phoenix
    {
        private readonly GeneralConfig _Config;
        private readonly ILogger _Logger;

        public Phoenix()
        {
            _Config = CascadeManager.Configure(c =>
            {
                const string cs = "Server=use-dev-sprdb1.axaltacsdev.dev;Database=Axalta.TIPS.RegWS;UID=tips;PWD=cAq$tH7%;";
                c
                    .SetDefaultUserName("Unit Test")
                    .SetLoggerFactory(new NLog.Extensions.Logging.NLogLoggerFactory())
                    .AddConnectionInfo<SqlDatabaseProvider>("Default", cs);
            });
            _Logger = _Config.LoggerFactory.CreateLogger<Phoenix>();
        }

        public void Try(Action action)
        {
            try
            {
                action();
            }
            catch (Exception ex)
            {
                _Logger.LogError(ex.Message, ex);
                throw;
            }
        }

        public class EditProductDTO
        {
            public class CountryDTO
            {
                public string CountryCode { get; set; }
            }
            public class NameDTO
            {
                public string LanguageCode { get; set; }
                public string ProductName { get; set; }
            }
            public int ID { get; set; }
            public string ProductCode { get; set; }
            public string FormulaCode { get; set; }
            public string BrandCode { get; set; }
            public string DefaultProductName { get; set; }
            public List<CountryDTO> Countries { get; set; }
            public List<NameDTO> Names { get; set; }
        }

        internal class ProductModel : ModelBase
        {
            private readonly RecordList<Product> _Root;
            private readonly RecordList<Product_Country> _Countries;
            private readonly RecordList<Product_Name> _Names;

            public ProductModel(GeneralConfig config)
                : base(config, "Default")
            {
                _Root = this.Add<Product>();
                _Countries = this.Add<Product_Country>();
                _Names = this.Add<Product_Name>();
            }

            public void Load(int? productID)
            {
                if (productID.HasValue)
                {
                    using (var db = this.GetDatabase())
                    {
                        _Root.PopulateWith(db.SelectOne<Product>(productID.Value));
                        _Countries.PopulateWith(db.SelectMany<Product_Country>(x => x.Product_ID == productID.Value));
                        _Names.PopulateWith(db.SelectMany<Product_Name>(x => x.Product_ID == productID.Value));
                    }
                }
                else
                {
                    this.Clear();
                }
            }

            public EditProductDTO GetDTO()
            {
                var root = _Root.CNM.SingleOrDefault();
                var dto = new EditProductDTO
                {
                    ID = root?.ID ?? 0,
                    ProductCode = root?.ProductCode,
                    FormulaCode = root?.FormulaCode,
                    BrandCode = root?.Brand_Code,
                    DefaultProductName = root?.DefaultProductName,
                    Countries = _Countries.CNM.Select(x => new EditProductDTO.CountryDTO
                    {
                        CountryCode = x.Country_Code,
                    }).ToList(),
                    Names = _Names.CNM.Select(x => new EditProductDTO.NameDTO
                    {
                        LanguageCode = x.Language_Code,
                        ProductName = x.ProductName,
                    }).ToList(),
                };
                return dto;
            }

            public void PostDTO(EditProductDTO dto)
            {
                _Root.MergeWith(dto,
                    (x) => x.ID, (x) => x.ID,
                    (x) => new Product(),
                    (x, y) =>
                    {
                        x.ProductCode = y.ProductCode;
                        x.FormulaCode = y.FormulaCode;
                        x.Brand_Code = y.BrandCode;
                        x.DefaultProductName = y.DefaultProductName;
                    });
                var root = _Root.CNM.Single();
                _Countries.MergeWith(dto.Countries,
                    (x) => x.CountryCode, (x) => x.Country_Code,
                    (x) => new Product_Country { Product_ID = root.ID, Country_Code = x.CountryCode },
                    null);
                _Names.MergeWith(dto.Names,
                    (x) => x.LanguageCode, (x) => x.Language_Code,
                    (x) => new Product_Name { Product_ID = root.ID, Language_Code = x.LanguageCode },
                    (x, y) => { x.ProductName = y.ProductName; });
            }
        }

        [Test]
        public void Select_All()
        {
            Try(() =>
            {
                using (var db = new Database(_Config, "Default"))
                {
                    var records = db.SelectAll<Product>();
                }
            });
        }

        [Test]
        [TestCase(null)]
        [TestCase(108)]
        public void EditModel(int? productID)
        {
            Try(() =>
            {
                //Controller GET
                var model = new ProductModel(_Config);
                model.Load(productID);
                var dto = model.GetDTO();

                //UI changes
                dto.ProductCode = "My test 3";
                dto.FormulaCode = "WXYZ-12345";
                dto.BrandCode = "(empty)";
                dto.DefaultProductName = "My Test 2 Name";
                dto.Countries = new List<EditProductDTO.CountryDTO>
                {
                    new EditProductDTO.CountryDTO { CountryCode = "US" },
                    new EditProductDTO.CountryDTO { CountryCode = "CA" },
                    new EditProductDTO.CountryDTO { CountryCode = "MX" },
                };
                dto.Names = new List<EditProductDTO.NameDTO>();

                //Controller POST
                model.PostDTO(dto);
                model.Save();
            });
        }
    }
}