﻿using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;

namespace AntFarm.Core
{
    public class BinaryWriterEx : BinaryWriter
    {
        public BinaryWriterEx(Stream output)
            : base(output)
        { }

        public BinaryWriterEx(Stream output, Encoding encoding)
            : base(output, encoding)
        { }

        #region Nullable base types

        public virtual void WriteNullable(bool? value)
        {
            if (value.HasValue)
            {
                base.Write(true);
                base.Write(value.Value);
            }
            else
                base.Write(false);
        }
        public virtual void WriteNullable(byte? value)
        {
            if (value.HasValue)
            {
                base.Write(true);
                base.Write(value.Value);
            }
            else
                base.Write(false);
        }
        public virtual void WriteNullable(char? value)
        {
            if (value.HasValue)
            {
                base.Write(true);
                base.Write(value.Value);
            }
            else
                base.Write(false);
        }
        public virtual void WriteNullable(decimal? value)
        {
            if (value.HasValue)
            {
                base.Write(true);
                base.Write(value.Value);
            }
            else
                base.Write(false);
        }
        public virtual void WriteNullable(double? value)
        {
            if (value.HasValue)
            {
                base.Write(true);
                base.Write(value.Value);
            }
            else
                base.Write(false);
        }
        public virtual void WriteNullable(short? value)
        {
            if (value.HasValue)
            {
                base.Write(true);
                base.Write(value.Value);
            }
            else
                base.Write(false);
        }
        public virtual void WriteNullable(int? value)
        {
            if (value.HasValue)
            {
                base.Write(true);
                base.Write(value.Value);
            }
            else
                base.Write(false);
        }
        public virtual void WriteNullable(long? value)
        {
            if (value.HasValue)
            {
                base.Write(true);
                base.Write(value.Value);
            }
            else
                base.Write(false);
        }
        public virtual void WriteNullable(sbyte? value)
        {
            if (value.HasValue)
            {
                base.Write(true);
                base.Write(value.Value);
            }
            else
                base.Write(false);
        }
        public virtual void WriteNullable(float? value)
        {
            if (value.HasValue)
            {
                base.Write(true);
                base.Write(value.Value);
            }
            else
                base.Write(false);
        }
        public virtual void WriteNullable(ushort? value)
        {
            if (value.HasValue)
            {
                base.Write(true);
                base.Write(value.Value);
            }
            else
                base.Write(false);
        }
        public virtual void WriteNullable(uint? value)
        {
            if (value.HasValue)
            {
                base.Write(true);
                base.Write(value.Value);
            }
            else
                base.Write(false);
        }
        public virtual void WriteNullable(ulong? value)
        {
            if (value.HasValue)
            {
                base.Write(true);
                base.Write(value.Value);
            }
            else
                base.Write(false);
        }

        #endregion

        #region Date/Time

        public virtual void Write(DateTime value)
        {
            base.Write(value.ToBinary());
        }
        public virtual void WriteNullable(DateTime? value)
        {
            if (value.HasValue)
            {
                base.Write(true);
                this.Write(value.Value);
            }
            else
                base.Write(false);
        }
        public virtual void Write(TimeSpan value)
        {
            base.Write(value.Ticks);
        }
        public virtual void WriteNullable(TimeSpan? value)
        {
            if (value.HasValue)
            {
                base.Write(true);
                this.Write(value.Value);
            }
            else
                base.Write(false);
        }
        public virtual void Write(DateTimeOffset value)
        {
            base.Write(value.DateTime.ToBinary());
            base.Write(value.Offset.TotalMinutes);
        }
        public virtual void WriteNullable(DateTimeOffset? value)
        {
            if (value.HasValue)
            {
                base.Write(true);
                this.Write(value.Value);
            }
            else
                base.Write(false);
        }

        #endregion

        #region Guid

        public virtual void Write(Guid value)
        {
            base.Write(value.ToString());
        }
        public virtual void WriteNullable(Guid? value)
        {
            if (value.HasValue)
            {
                base.Write(true);
                this.Write(value.Value);
            }
            else
                base.Write(false);
        }

        #endregion

        public override void Write(string value)
        {
            if (value != null)
            {
                base.Write(true);
                base.Write(value);
            }
            else
                base.Write(false);
        }

        public override void Write(byte[] buffer)
        {
            if (buffer != null)
            {
                base.Write(true);
                base.Write(buffer.Length);
                base.Write(buffer, 0, buffer.Length);
            }
            else
                base.Write(false);
        }

        public void Write(object value)
        {
            if (value != null)
            {
                base.Write(true);
                using (var s = new MemoryStream())
                {
                    var bf = new BinaryFormatter();
                    bf.Serialize(s, value);
                    var buffer = s.GetBuffer();
                    base.Write(buffer.Length);
                    base.Write(buffer, 0, buffer.Length);
                }
            }
            else
                base.Write(false);
        }
    }
}