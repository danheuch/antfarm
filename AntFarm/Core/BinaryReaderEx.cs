﻿using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;

namespace AntFarm.Core
{
    public class BinaryReaderEx : BinaryReader
    {
        public BinaryReaderEx(Stream input)
            : base(input)
        { }

        public BinaryReaderEx(Stream input, Encoding encoding)
            : base(input, encoding)
        { }

        #region Nullable base types

        public virtual bool? ReadNullableBoolean()
        {
            if (base.ReadBoolean())
                return base.ReadBoolean();
            else
                return null;
        }
        public virtual byte? ReadNullableByte()
        {
            if (base.ReadBoolean())
                return base.ReadByte();
            else
                return null;
        }
        public virtual char? ReadNullableChar()
        {
            if (base.ReadBoolean())
                return base.ReadChar();
            else
                return null;
        }
        public virtual decimal? ReadNullableDecimal()
        {
            if (base.ReadBoolean())
                return base.ReadDecimal();
            else
                return null;
        }
        public virtual double? ReadNullableDouble()
        {
            if (base.ReadBoolean())
                return base.ReadDouble();
            else
                return null;
        }
        public virtual short? ReadNullableInt16()
        {
            if (base.ReadBoolean())
                return base.ReadInt16();
            else
                return null;
        }
        public virtual int? ReadNullableInt32()
        {
            if (base.ReadBoolean())
                return base.ReadInt32();
            else
                return null;
        }
        public virtual long? ReadNullableInt64()
        {
            if (base.ReadBoolean())
                return base.ReadInt64();
            else
                return null;
        }
        public virtual sbyte? ReadNullableSByte()
        {
            if (base.ReadBoolean())
                return base.ReadSByte();
            else
                return null;
        }
        public virtual float? ReadNullableSingle()
        {
            if (base.ReadBoolean())
                return base.ReadSingle();
            else
                return null;
        }
        public virtual ushort? ReadNullableUInt16()
        {
            if (base.ReadBoolean())
                return base.ReadUInt16();
            else
                return null;
        }
        public virtual uint? ReadNullableUInt32()
        {
            if (base.ReadBoolean())
                return base.ReadUInt32();
            else
                return null;
        }
        public virtual ulong? ReadNullableUInt64()
        {
            if (base.ReadBoolean())
                return base.ReadUInt64();
            else
                return null;
        }

        #endregion

        #region Date/Time

        public virtual DateTime ReadDateTime()
        {
            return DateTime.FromBinary(base.ReadInt64());
        }
        public virtual DateTime? ReadNullableDateTime()
        {
            if (base.ReadBoolean())
                return this.ReadDateTime();
            else
                return null;
        }
        public virtual TimeSpan ReadTimeSpan()
        {
            return TimeSpan.FromTicks(base.ReadInt64());
        }
        public virtual TimeSpan? ReadNullableTimeSpan()
        {
            if (base.ReadBoolean())
                return this.ReadTimeSpan();
            else
                return null;
        }
        public virtual DateTimeOffset ReadDateTimeOffset()
        {
            return new DateTimeOffset(DateTime.FromBinary(base.ReadInt64()), TimeSpan.FromMinutes(base.ReadDouble()));
        }
        public virtual DateTimeOffset? ReadNullableDateTimeOffset()
        {
            if (base.ReadBoolean())
                return this.ReadDateTimeOffset();
            else
                return null;
        }

        #endregion

        #region Guid

        public virtual Guid ReadGuid()
        {
            return new Guid(base.ReadString());
        }
        public virtual Guid? ReadNullableGuid()
        {
            if (base.ReadBoolean())
                return this.ReadGuid();
            else
                return null;
        }
        
        #endregion

        public override string ReadString()
        {
            if (base.ReadBoolean())
                return base.ReadString();
            else
                return null;
        }

        public virtual byte[] ReadBytes()
        {
            if (base.ReadBoolean())
                return base.ReadBytes(base.ReadInt32());
            else
                return null;
        }

        public virtual object ReadObject()
        {
            if (base.ReadBoolean())
            {
                var buffer = base.ReadBytes(base.ReadInt32());
                using (var s = new MemoryStream(buffer))
                {
                    var bf = new BinaryFormatter();
                    return bf.Deserialize(s);
                }
            }
            else
                return null;
        }
    }
}