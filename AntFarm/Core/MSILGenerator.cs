﻿using AntFarm.ORM;
using AntFarm.ORM.Providers;
using System;
using System.Data;
using System.Diagnostics;
using System.Reflection;
using System.Reflection.Emit;

namespace AntFarm.Core
{
    /// <summary>
    /// Provides a set of static methods that generate dynamic methods in support of various functions in the library.
    /// </summary>
    /// <remarks>
    /// <list type="bullet">
    /// <listheader>Examples of dynamic methods include:</listheader>
    /// <item>Fast IL reader to populate a row class from a data reader</item>
    /// <item>Strongly-types factory method</item>
    /// <item>Primary key getter and setter</item>
    /// <item>etc.</item>
    /// </list>
    /// </remarks>
    public static class MSILGenerator
    {
        /// <summary>
        /// Creates a dynamic method that gets multiple properties from an object and return them via an array.
        /// </summary>
        /// <param name="accessors">The array of properties to get.</param>
        /// <remarks>A null reference in the list of accessors will cause the IL generator to force a null value at that index in the array returned by the dynamic method.</remarks>
        public static DynamicMethod CreateMultiPropertyGetter(PropertyInfo[] accessors)
        {
            //The IL method takes in an object (from which the properties are read), and returns an array of objects (the property values).
            var method = new DynamicMethod("", typeof(object[]), new[] { typeof(object) });
            var il = method.GetILGenerator();
            //object[] arr = new object[length];
            il.DeclareLocal(typeof(object[])); //declare var 0 (for "arr")
            il.Emit(OpCodes.Ldc_I4, accessors.Length); //Push "length" onto the stack (+1)
            il.Emit(OpCodes.Newarr, typeof(object)); //create "arr" (-1+1)
            il.Emit(OpCodes.Stloc_0); //Store "arr" in var 0 (-1)
            for (int i = 0; i < accessors.Length; i++)
            {
                if (accessors[i] == null)
                {
                    //arr[i] = null;
                    il.Emit(OpCodes.Ldloc_0); //Push "arr" (var 0) onto the stack (+1)
                    switch (i)
                    {
                        case 0: il.Emit(OpCodes.Ldc_I4_0); break; //push the value of 0 (+1)
                        case 1: il.Emit(OpCodes.Ldc_I4_1); break; //push the value of 1 (+1)
                        case 2: il.Emit(OpCodes.Ldc_I4_2); break; //push the value of 2 (+1)
                        case 3: il.Emit(OpCodes.Ldc_I4_3); break; //push the value of 3 (+1)
                        case 4: il.Emit(OpCodes.Ldc_I4_4); break; //push the value of 4 (+1)
                        case 5: il.Emit(OpCodes.Ldc_I4_5); break; //push the value of 5 (+1)
                        case 6: il.Emit(OpCodes.Ldc_I4_6); break; //push the value of 6 (+1)
                        case 7: il.Emit(OpCodes.Ldc_I4_7); break; //push the value of 7 (+1)
                        case 8: il.Emit(OpCodes.Ldc_I4_8); break; //push the value of 8 (+1)
                        default: il.Emit(OpCodes.Ldc_I4, i); break; //push the value of "i" (+1)
                    }
                    il.Emit(OpCodes.Ldnull); //Push "null" (+1)
                    il.Emit(OpCodes.Stelem_Ref); //Store null" into "arr[i]" (-3)
                }
                else
                {
                    //arr[i] = obj.<xxx>;
                    il.Emit(OpCodes.Ldloc_0); //Push "arr" (var 0) onto the stack (+1)
                    switch (i)
                    {
                        case 0: il.Emit(OpCodes.Ldc_I4_0); break; //push the value of 0 (+1)
                        case 1: il.Emit(OpCodes.Ldc_I4_1); break; //push the value of 1 (+1)
                        case 2: il.Emit(OpCodes.Ldc_I4_2); break; //push the value of 2 (+1)
                        case 3: il.Emit(OpCodes.Ldc_I4_3); break; //push the value of 3 (+1)
                        case 4: il.Emit(OpCodes.Ldc_I4_4); break; //push the value of 4 (+1)
                        case 5: il.Emit(OpCodes.Ldc_I4_5); break; //push the value of 5 (+1)
                        case 6: il.Emit(OpCodes.Ldc_I4_6); break; //push the value of 6 (+1)
                        case 7: il.Emit(OpCodes.Ldc_I4_7); break; //push the value of 7 (+1)
                        case 8: il.Emit(OpCodes.Ldc_I4_8); break; //push the value of 8 (+1)
                        default: il.Emit(OpCodes.Ldc_I4, i); break; //push the value of "i" (+1)
                    }
                    il.Emit(OpCodes.Ldarg_0); //Push "obj" (arg 0) onto the stack (+1)
                    il.Emit(OpCodes.Callvirt, accessors[i].GetMethod); //Call the property getter (-1+1)
                    if (accessors[i].PropertyType.IsValueType)
                        il.Emit(OpCodes.Box, accessors[i].PropertyType); //Box the value type to object (-1+1)
                    il.Emit(OpCodes.Stelem_Ref); //Store "obj.<xxx>" into "arr[i]" (-3)
                }
            }
            //return result;
            il.Emit(OpCodes.Ldloc_0); //Push "arr" (var 0) onto the stack (+1)
            il.Emit(OpCodes.Ret); //Return
            return method;
        }

        /// <summary>
        /// Creates a dynamic method that sets multiple properties of an object from an array.
        /// </summary>
        /// <param name="accessors">The array of properties to set.</param>
        public static DynamicMethod CreateMultiPropertySetter(PropertyInfo[] accessors)
        {
            //The IL method takes in an object (whose properties are set) and a list of objects (the data source), and returns void.
            var method = new DynamicMethod("", typeof(void), new[] { typeof(object), typeof(object[]) });
            var il = method.GetILGenerator();
            for (int i = 0; i < accessors.Length; i++)
            {
                //obj.<xxx> = arr[i];
                il.Emit(OpCodes.Ldarg_0); //Push "obj" (arg 0) onto the stack (+1)
                il.Emit(OpCodes.Ldarg_1); //Push "arr" (arg 1) onto the stack (+1)
                switch (i)
                {
                    case 0: il.Emit(OpCodes.Ldc_I4_0); break; //push the value of 0 (+1)
                    case 1: il.Emit(OpCodes.Ldc_I4_1); break; //push the value of 1 (+1)
                    case 2: il.Emit(OpCodes.Ldc_I4_2); break; //push the value of 2 (+1)
                    case 3: il.Emit(OpCodes.Ldc_I4_3); break; //push the value of 3 (+1)
                    case 4: il.Emit(OpCodes.Ldc_I4_4); break; //push the value of 4 (+1)
                    case 5: il.Emit(OpCodes.Ldc_I4_5); break; //push the value of 5 (+1)
                    case 6: il.Emit(OpCodes.Ldc_I4_6); break; //push the value of 6 (+1)
                    case 7: il.Emit(OpCodes.Ldc_I4_7); break; //push the value of 7 (+1)
                    case 8: il.Emit(OpCodes.Ldc_I4_8); break; //push the value of 8 (+1)
                    default: il.Emit(OpCodes.Ldc_I4, i); break; //push the value of "i" (+1)
                }
                il.Emit(OpCodes.Ldelem_Ref); //Pop "arr" and "i" and push "arr[i]" (as object) (-2+1)
                if (accessors[i].PropertyType.IsValueType)
                    il.Emit(OpCodes.Unbox_Any, accessors[i].PropertyType); //Unbox the value type from object (-1+1)
                il.Emit(OpCodes.Callvirt, accessors[i].SetMethod); //Call the property setter (-2)
            }
            //return;
            il.Emit(OpCodes.Ret);
            return method;
        }

        public static DynamicMethod CreateFactory(Type createdType)
        {
            var method = new DynamicMethod("", createdType, null);
            var il = method.GetILGenerator();
            var ctor = createdType.GetConstructor(Type.EmptyTypes);
            if (ctor == null)
                throw new InvalidOperationException($"Type {createdType} does not have an empty constructor.");
            il.Emit(OpCodes.Newobj, ctor);
            il.Emit(OpCodes.Ret);
            return method;
        }

        public static DynamicMethod CreateMapper(Type mappedToType, Type mappedFromType)
        {
            var method = new DynamicMethod("", typeof(void), new[] { typeof(object), typeof(object) });
            var il = method.GetILGenerator();
            foreach (var fromProp in mappedFromType.GetProperties())
                foreach (var toProp in mappedToType.GetProperties())
                    if (toProp.Name == fromProp.Name)
                        if (toProp.PropertyType == fromProp.PropertyType)
                        {
                            //Copy the next property from the given DTO to the new object:
                            //e.g., toObj.PropX = fromObj.PropX
                            il.Emit(OpCodes.Ldarg_0); //push argument 0 (the TO object) (+1)
                            il.Emit(OpCodes.Ldarg_1); //push argument 1 (the FROM object) (+1)
                            il.Emit(OpCodes.Callvirt, fromProp.GetGetMethod()); //get/push the property value (0)
                            il.Emit(OpCodes.Callvirt, toProp.GetSetMethod()); //set/pop the property value (-2)
                        }
            il.Emit(OpCodes.Ret);
            return method;
        }

        public static DynamicMethod CreateFactoryAndMapper(Type createdAndMappedToType, Type mappedFromType)
        {
            var method = new DynamicMethod("", createdAndMappedToType, new[] { typeof(object) });
            var il = method.GetILGenerator();
            //Declare a local variable for the created/returned object:
            il.DeclareLocal(createdAndMappedToType);
            //Create the new object (+1):
            var ctor = createdAndMappedToType.GetConstructor(Type.EmptyTypes);
            if (ctor == null)
                throw new InvalidOperationException($"Type {createdAndMappedToType} does not have an empty constructor.");
            il.Emit(OpCodes.Newobj, ctor);
            //Store it at 0 (-1):
            il.Emit(OpCodes.Stloc_0);
            //Map the properties of the DTO/mapped object to the new object:
            var mapper = CreateMapper(createdAndMappedToType, mappedFromType);
            il.Emit(OpCodes.Ldloc_0); //push local 0 (the new object) (+1)
            il.Emit(OpCodes.Ldarg_0); //push argument 0 (the dto) (+1)
            il.Emit(OpCodes.Call, mapper); //call the mapping method (-2)
            //return result;
            il.Emit(OpCodes.Ldloc_0); //push local 0 (the new object)
            il.Emit(OpCodes.Ret); //return
            return method;
        }

        public static DynamicMethod CreateRowSerializer(Type rowType)
        {
            var method = new DynamicMethod("", typeof(void), new[] { typeof(BinaryWriterEx), typeof(object) });
            var il = method.GetILGenerator();
            foreach (var prop in rowType.GetProperties())
            {
                var getter = prop.GetGetMethod();
                if (getter != null)
                {
                    var setter = prop.GetSetMethod();
                    if (setter != null)
                    {
                        var writer = Type2BinaryWriter(prop.PropertyType);
                        if (writer != null)
                        {
                            il.Emit(OpCodes.Ldarg_0);
                            il.Emit(OpCodes.Ldarg_1);
                            il.Emit(OpCodes.Callvirt, getter);
                            il.Emit(OpCodes.Callvirt, writer);
                        }
                        else
                            throw new NotSupportedException($"For property \"{prop.Name}\" of type {rowType} - Serialization of the property type is not supported.");
                    }
                }
            }
            il.Emit(OpCodes.Ret);
            return method;
        }

        public static DynamicMethod CreateRowDeserializer(Type rowType)
        {
            var method = new DynamicMethod("", rowType, new[] { typeof(BinaryReaderEx) });
            var il = method.GetILGenerator();
            il.DeclareLocal(rowType);
            var ctor = rowType.GetConstructor(Type.EmptyTypes);
            if (ctor == null)
                throw new InvalidOperationException($"Type {rowType} does not have an empty constructor.");
            il.Emit(OpCodes.Newobj, ctor);
            il.Emit(OpCodes.Stloc_0);
            foreach (var prop in rowType.GetProperties())
            {
                var getter = prop.GetGetMethod();
                if (getter != null)
                {
                    var setter = prop.GetSetMethod();
                    if (setter != null)
                    {
                        var reader = Type2BinaryReader(prop.PropertyType);
                        if (reader != null)
                        {
                            il.Emit(OpCodes.Ldloc_0);
                            il.Emit(OpCodes.Ldarg_0);
                            il.Emit(OpCodes.Callvirt, reader);
                            il.Emit(OpCodes.Callvirt, setter);
                        }
                        else
                            throw new NotSupportedException($"For property \"{prop.Name}\" of type {rowType} - Deserialization of the property type is not supported.");
                    }
                }
            }
            il.Emit(OpCodes.Ldloc_0);
            il.Emit(OpCodes.Ret);
            return method;
        }

        /// <summary>
        /// Creates a dynamic method allowing to rapidly populate a row from a data source.
        /// The generated method accepts the row to populate as input. It does not instantiate it.
        /// </summary>
        /// <param name="tbProvider">The mapping between destination row and data source.</param>
        /// <param name="coReader">The data source.</param>
        /// <returns>An IL-generated method.</returns>
        public static DynamicMethod CreateRowReader(TableProvider tbProvider, DataReaderCo coReader)
        {
            var coReaderType = coReader.GetType();
            var dr = coReader.DataReader;
            var method = new DynamicMethod("", typeof(void), new[] { tbProvider.RowType, typeof(object) });
            var il = method.GetILGenerator();
            for (int i = 0; i < dr.FieldCount; i++)
            {
                (Type fieldType, string fieldName, string fieldDataTypeName) = (dr.GetFieldType(i), dr.GetName(i), dr.GetDataTypeName(i));
                foreach (var colInfo in tbProvider.Columns)
                {
                    if (colInfo.ColumnName.Equals(fieldName, StringComparison.OrdinalIgnoreCase))
                    {
                        //Generate code for (for example): row.SomeStringProperty = adr.GetString(i)
                        var adrMethod = DiscoverDataReaderMethod(colInfo.ColumnType, fieldType, coReaderType);
                        if (adrMethod == null)
                            throw new NotSupportedException($"An accessor for column \"{colInfo.ColumnName}\" (type {colInfo.ColumnType}, source type {fieldType}, source data type name \"{fieldDataTypeName}\"), could not be found. The data cannot be read from the source.");
                        il.Emit(OpCodes.Ldarg_0); //push argument 0 (row) (+1)
                        il.Emit(OpCodes.Ldarg_1); //push argument 1 (adr) (+1)
                        switch (i)
                        {
                            case 0: il.Emit(OpCodes.Ldc_I4_0); break; //push the value of 0 (+1)
                            case 1: il.Emit(OpCodes.Ldc_I4_1); break; //push the value of 1 (+1)
                            case 2: il.Emit(OpCodes.Ldc_I4_2); break; //push the value of 2 (+1)
                            case 3: il.Emit(OpCodes.Ldc_I4_3); break; //push the value of 3 (+1)
                            case 4: il.Emit(OpCodes.Ldc_I4_4); break; //push the value of 4 (+1)
                            case 5: il.Emit(OpCodes.Ldc_I4_5); break; //push the value of 5 (+1)
                            case 6: il.Emit(OpCodes.Ldc_I4_6); break; //push the value of 6 (+1)
                            case 7: il.Emit(OpCodes.Ldc_I4_7); break; //push the value of 7 (+1)
                            case 8: il.Emit(OpCodes.Ldc_I4_8); break; //push the value of 8 (+1)
                            default: il.Emit(OpCodes.Ldc_I4, i); break; //push the value of "i" (+1)
                        }
                        il.Emit(OpCodes.Callvirt, adrMethod); //call adr.GetString (-2+1=-1)
                        il.Emit(OpCodes.Callvirt, colInfo.Accessor.GetSetMethod()); //set row.SomeStringProperty (-2)
                        break;
                    }
                }
            }
            il.Emit(OpCodes.Ret);
            return method;
        }

        /// <summary>
        /// Creates a dynamic method allowing to rapidly populate a row from a data source.
        /// The generated method instantiates the row, which is a class.
        /// </summary>
        /// <param name="tbProvider">The mapping between destination row and data source.</param>
        /// <param name="coReader">The data source.</param>
        /// <returns>An IL-generated method.</returns>
        public static DynamicMethod CreateRowFactoryAndReaderForObject(TableProvider tbProvider, DataReaderCo coReader)
        {
            var coReaderType = coReader.GetType();
            var dr = coReader.DataReader;
            var rowType = tbProvider.RowType;
            var method = new DynamicMethod("", rowType, new[] { typeof(object) });
            var il = method.GetILGenerator();
            il.DeclareLocal(rowType);
            var ctor = rowType.GetConstructor(Type.EmptyTypes);
            if (ctor == null)
                throw new InvalidOperationException($"Type {rowType} does not have an empty constructor.");
            il.Emit(OpCodes.Newobj, ctor);
            il.Emit(OpCodes.Stloc_0);
            for (int i = 0; i < dr.FieldCount; i++)
            {
                (Type fieldType, string fieldName, string fieldDataTypeName) = (dr.GetFieldType(i), dr.GetName(i), dr.GetDataTypeName(i));
                foreach (var colInfo in tbProvider.Columns)
                {
                    if (colInfo.ColumnName.Equals(fieldName, StringComparison.OrdinalIgnoreCase))
                    {
                        //Generate code for (for example): row.SomeStringProperty = adr.GetString(i)
                        var adrMethod = DiscoverDataReaderMethod(colInfo.ColumnType, fieldType, coReaderType);
                        if (adrMethod == null)
                            throw new NotSupportedException($"An accessor for column \"{colInfo.ColumnName}\" (type {colInfo.ColumnType}, source type {fieldType}, source data type name \"{fieldDataTypeName}\"), could not be found. The data cannot be read from the source.");
                        il.Emit(OpCodes.Ldloc_0); //push local 0 (row) (+1)
                        il.Emit(OpCodes.Ldarg_0); //push argument 0 (adr) (+1)
                        switch (i)
                        {
                            case 0: il.Emit(OpCodes.Ldc_I4_0); break; //push the value of 0 (+1)
                            case 1: il.Emit(OpCodes.Ldc_I4_1); break; //push the value of 1 (+1)
                            case 2: il.Emit(OpCodes.Ldc_I4_2); break; //push the value of 2 (+1)
                            case 3: il.Emit(OpCodes.Ldc_I4_3); break; //push the value of 3 (+1)
                            case 4: il.Emit(OpCodes.Ldc_I4_4); break; //push the value of 4 (+1)
                            case 5: il.Emit(OpCodes.Ldc_I4_5); break; //push the value of 5 (+1)
                            case 6: il.Emit(OpCodes.Ldc_I4_6); break; //push the value of 6 (+1)
                            case 7: il.Emit(OpCodes.Ldc_I4_7); break; //push the value of 7 (+1)
                            case 8: il.Emit(OpCodes.Ldc_I4_8); break; //push the value of 8 (+1)
                            default: il.Emit(OpCodes.Ldc_I4, i); break; //push the value of "i" (+1)
                        }
                        il.Emit(OpCodes.Callvirt, adrMethod); //call adr.GetString (-2+1=-1)
                        il.Emit(OpCodes.Callvirt, colInfo.Accessor.GetSetMethod()); //set row.SomeStringProperty (-2)
                        break;
                    }
                }
            }
            il.Emit(OpCodes.Ldloc_0);
            il.Emit(OpCodes.Ret);
            return method;
        }

        /// <summary>
        /// Creates a dynamic method allowing to rapidly populate a row from a data source.
        /// The generated method instantiates the row, which is a <see cref="ValueTuple"/>.
        /// </summary>
        /// <param name="tbProvider">The mapping between destination row and data source.</param>
        /// <param name="coReader">The data source.</param>
        /// <returns>An IL-generated method.</returns>
        public static DynamicMethod CreateRowFactoryAndReaderForTuple(TableProvider tbProvider, DataReaderCo coReader)
        {
            var coReaderType = coReader.GetType();
            var dr = coReader.DataReader;
            var rowType = tbProvider.RowType;
            var rowFields = rowType.GetFields();
            var method = new DynamicMethod("", rowType, new[] { typeof(object) });
            var il = method.GetILGenerator();
            il.DeclareLocal(rowType);
            for (int i = 0; i < Math.Min(dr.FieldCount, rowFields.Length); i++)
            {
                (Type fieldType, string fieldName, string fieldDataTypeName) = (dr.GetFieldType(i), dr.GetName(i), dr.GetDataTypeName(i));
                var rowField = rowFields[i];
                //Generate code for (for example): tuple.Item1 = adr.GetString(i)
                var adrMethod = DiscoverDataReaderMethod(rowField.FieldType, fieldType, coReaderType);
                if (adrMethod == null)
                    throw new NotSupportedException($"An accessor for field \"{rowField.Name}\" (type {rowField.FieldType}, source type {fieldType}, source data type name \"{fieldDataTypeName}\"), could not be found. The data cannot be read from the source.");
                il.Emit(OpCodes.Ldloca_S, 0); //push ADDRESS of local 0 (row) (+1)
                il.Emit(OpCodes.Ldarg_0); //push argument 0 (adr) (+1)
                switch (i)
                {
                    case 0: il.Emit(OpCodes.Ldc_I4_0); break; //push the value of 0 (+1)
                    case 1: il.Emit(OpCodes.Ldc_I4_1); break; //push the value of 1 (+1)
                    case 2: il.Emit(OpCodes.Ldc_I4_2); break; //push the value of 2 (+1)
                    case 3: il.Emit(OpCodes.Ldc_I4_3); break; //push the value of 3 (+1)
                    case 4: il.Emit(OpCodes.Ldc_I4_4); break; //push the value of 4 (+1)
                    case 5: il.Emit(OpCodes.Ldc_I4_5); break; //push the value of 5 (+1)
                    case 6: il.Emit(OpCodes.Ldc_I4_6); break; //push the value of 6 (+1)
                    case 7: il.Emit(OpCodes.Ldc_I4_7); break; //push the value of 7 (+1)
                    case 8: il.Emit(OpCodes.Ldc_I4_8); break; //push the value of 8 (+1)
                    default: il.Emit(OpCodes.Ldc_I4, i); break; //push the value of "i" (+1)
                }
                il.Emit(OpCodes.Callvirt, adrMethod); //call adr.GetString (-2+1=-1)
                il.Emit(OpCodes.Stfld, rowField); //set the value tuple field i
            }
            il.Emit(OpCodes.Ldloc_0);
            il.Emit(OpCodes.Ret);
            return method;
        }

        /// <summary>
        /// Creates a dynamic method that prepares a row for insert by setting its audit columns (if any).
        /// See <see cref="AuditColumnOption"/> for more info.
        /// </summary>
        /// <param name="tbProvider">Information about the row type, including the audit fields.</param>
        public static DynamicMethod CreateRowInsertPreparer(TableProvider tbProvider)
        {
            var method = new DynamicMethod("", typeof(void), new[] { typeof(object)/*the row to update*/, typeof(DateTimeOffset)/*the current date/time*/, typeof(string)/*the user name*/ });
            var il = method.GetILGenerator();
            foreach (var colInfo in tbProvider.Columns)
            {
                switch (colInfo.AuditOption)
                {
                    case AuditColumnOption.CreatedOn:
                    case AuditColumnOption.UpdatedOn:
                        EmitCodeForAuditDate(tbProvider.RowType, colInfo.Accessor, il);
                        break;
                    case AuditColumnOption.CreatedBy:
                    case AuditColumnOption.UpdatedBy:
                        EmitCodeForAuditUser(tbProvider.RowType, colInfo.Accessor, il);
                        break;
                }
            }
            il.Emit(OpCodes.Ret);
            return method;
        }

        /// <summary>
        /// Creates a dynamic method that prepares a row for update by setting its audit columns (if any).
        /// See <see cref="AuditColumnOption"/> for more info.
        /// </summary>
        /// <param name="tbProvider">Information about the row type, including the audit fields.</param>
        public static DynamicMethod CreateRowUpdatePreparer(TableProvider tbProvider)
        {
            var method = new DynamicMethod("", typeof(void), new[] { typeof(object)/*the row to update*/, typeof(DateTimeOffset)/*the current date/time*/, typeof(string)/*the user name*/ });
            var il = method.GetILGenerator();
            foreach (var colInfo in tbProvider.Columns)
            {
                switch (colInfo.AuditOption)
                {
                    case AuditColumnOption.UpdatedOn:
                        EmitCodeForAuditDate(tbProvider.RowType, colInfo.Accessor, il);
                        break;
                    case AuditColumnOption.UpdatedBy:
                        EmitCodeForAuditUser(tbProvider.RowType, colInfo.Accessor, il);
                        break;
                }
            }
            il.Emit(OpCodes.Ret);
            return method;
        }

        private static void EmitCodeForAuditDate(Type rowType, PropertyInfo accessor, ILGenerator il)
        {
#if false
            //For example: row.CreatedOn = dtNow
            il.Emit(OpCodes.Ldarg_0); //push arg 0 (row)
            il.Emit(OpCodes.Ldarg_1); //push arg 1 (the date/time)
            if (accessor.PropertyType == typeof(DateTime))
            {
                //Set the property value.
                il.Emit(OpCodes.Callvirt, accessor.SetMethod);
            }
            else if (accessor.PropertyType == typeof(DateTime?))
            {
                //Same as previous case, but the nullable DateTime must be constructed first.
                il.Emit(OpCodes.Newobj, typeof(DateTime?).GetConstructor(new[] { typeof(DateTime) }));
                il.Emit(OpCodes.Callvirt, accessor.SetMethod);
            }
            else if (accessor.PropertyType == typeof(DateTimeOffset))
            {
                //The DateTime must be converted into a DateTimeOffset, which requires information about
                //the time zone. For example, if the DateTime is "dt", and the TimeZoneInfo is "tz", the
                //conversion is "new DateTimeOffset(dt, tz.BaseUtcOffset);
                il.Emit(OpCodes.Ldarg_2); //push arg 2 (the time zone)
                il.Emit(OpCodes.Callvirt, typeof(TimeZoneInfo).GetProperty("BaseUtcOffset").GetGetMethod());
                il.Emit(OpCodes.Newobj, typeof(DateTimeOffset).GetConstructor(new[] { typeof(DateTime), typeof(TimeSpan) }));
                il.Emit(OpCodes.Callvirt, accessor.SetMethod);
            }
            else if (accessor.PropertyType == typeof(DateTimeOffset?))
            {
                //Same as previous case, but the nullable DateTimeOffset must be constructed first.
                il.Emit(OpCodes.Ldarg_2); //push arg 2 (the time zone)
                il.Emit(OpCodes.Call, typeof(TimeZoneInfo).GetProperty("BaseUtcOffset").GetGetMethod());
                il.Emit(OpCodes.Newobj, typeof(DateTimeOffset).GetConstructor(new[] { typeof(DateTime), typeof(TimeSpan) }));
                il.Emit(OpCodes.Newobj, typeof(DateTimeOffset?).GetConstructor(new[] { typeof(DateTimeOffset) }));
                il.Emit(OpCodes.Callvirt, accessor.SetMethod);
            }
            else
            {
                throw new NotSupportedException($"Row type {rowType} - Audit date/time stamp must be of underlying type {typeof(DateTime)} or {typeof(DateTimeOffset)}. Property {accessor.Name} is of type {accessor.PropertyType}.");
            }
#else
            //NOTE: The method signature has the row as argument 0 and the date/time stamp as argument 1.
            //Push arg 0 (the row).
            il.Emit(OpCodes.Ldarg_0);
            //Push the date/time stamp. Argument 1 is a DateTimeOffset, and it may have to be converted to whatever the property is expecting.
            if (accessor.PropertyType == typeof(DateTime))
            {
                //Push address of arg 1 (the DateTimeOffset) and get the DateTime portion.
                il.Emit(OpCodes.Ldarga_S, (byte)1);
                il.Emit(OpCodes.Call, typeof(DateTimeOffset).GetProperty("DateTime").GetGetMethod());
            }
            else if (accessor.PropertyType == typeof(DateTime?))
            {
                //Push address of arg 1 (the DateTimeOffset) and get the DateTime portion.
                il.Emit(OpCodes.Ldarga_S, (byte)1);
                il.Emit(OpCodes.Call, typeof(DateTimeOffset).GetProperty("DateTime").GetGetMethod());
                //Convert it to nullable.
                il.Emit(OpCodes.Newobj, typeof(DateTime?).GetConstructor(new[] { typeof(DateTime) }));
            }
            else if (accessor.PropertyType == typeof(DateTimeOffset))
            {
                //Most straightforward case... Simply push arg 1 (the DateTimeOffset). 
                il.Emit(OpCodes.Ldarg_1);
            }
            else if (accessor.PropertyType == typeof(DateTimeOffset?))
            {
                //Also straightforward... Push arg 1 (the DateTimeOffset).
                il.Emit(OpCodes.Ldarg_1);
                //Convert it to nullable.
                il.Emit(OpCodes.Newobj, typeof(DateTimeOffset?).GetConstructor(new[] { typeof(DateTimeOffset) }));
            }
            else
            {
                throw new NotSupportedException($"Row type {rowType} - Audit date/time stamp must be of underlying type {typeof(DateTime)} or {typeof(DateTimeOffset)}. Property {accessor.Name} is of type {accessor.PropertyType}.");
            }
            //Set the row's property.
            il.Emit(OpCodes.Callvirt, accessor.SetMethod);
#endif
        }

        private static void EmitCodeForAuditUser(Type rowType, PropertyInfo accessor, ILGenerator il)
        {
            //NOTE: The method signature has the row as argument 0 and the user name as argument 2.
            il.Emit(OpCodes.Ldarg_0);
            if (accessor.PropertyType == typeof(string))
            {
                il.Emit(OpCodes.Ldarg_2);
            }
            else
            {
                throw new NotSupportedException($"Row type {rowType} - Audit User field must be of type {typeof(string)}. Property {accessor.Name} is of type {accessor.PropertyType}.");
            }
            il.Emit(OpCodes.Callvirt, accessor.SetMethod);
        }

        private static MethodInfo Type2BinaryWriter(Type type)
        {
            Type nullableType = Nullable.GetUnderlyingType(type);
            bool isNullable = nullableType != null;
            Type underlyingType = nullableType ?? type;
            Type bwType = typeof(BinaryWriterEx);
            switch (underlyingType)
            {
                //TODO: Reorder cases
                case Type t when t == typeof(bool):
                    return bwType.GetMethod(isNullable ? "WriteNullable" : "Write", new[] { isNullable ? typeof(bool?) : typeof(bool) });
                case Type t when t == typeof(byte):
                    return bwType.GetMethod(isNullable ? "WriteNullable" : "Write", new[] { isNullable ? typeof(byte?) : typeof(byte) });
                case Type t when t == typeof(char):
                    return bwType.GetMethod(isNullable ? "WriteNullable" : "Write", new[] { isNullable ? typeof(char?) : typeof(char) });
                case Type t when t == typeof(decimal):
                    return bwType.GetMethod(isNullable ? "WriteNullable" : "Write", new[] { isNullable ? typeof(decimal?) : typeof(decimal) });
                case Type t when t == typeof(double):
                    return bwType.GetMethod(isNullable ? "WriteNullable" : "Write", new[] { isNullable ? typeof(double?) : typeof(double) });
                case Type t when t == typeof(short):
                    return bwType.GetMethod(isNullable ? "WriteNullable" : "Write", new[] { isNullable ? typeof(short?) : typeof(short) });
                case Type t when t == typeof(int):
                    return bwType.GetMethod(isNullable ? "WriteNullable" : "Write", new[] { isNullable ? typeof(int?) : typeof(int) });
                case Type t when t == typeof(long):
                    return bwType.GetMethod(isNullable ? "WriteNullable" : "Write", new[] { isNullable ? typeof(long?) : typeof(long) });
                case Type t when t == typeof(sbyte):
                    return bwType.GetMethod(isNullable ? "WriteNullable" : "Write", new[] { isNullable ? typeof(sbyte?) : typeof(sbyte) });
                case Type t when t == typeof(float):
                    return bwType.GetMethod(isNullable ? "WriteNullable" : "Write", new[] { isNullable ? typeof(float?) : typeof(float) });
                case Type t when t == typeof(ushort):
                    return bwType.GetMethod(isNullable ? "WriteNullable" : "Write", new[] { isNullable ? typeof(ushort?) : typeof(ushort) });
                case Type t when t == typeof(uint):
                    return bwType.GetMethod(isNullable ? "WriteNullable" : "Write", new[] { isNullable ? typeof(uint?) : typeof(uint) });
                case Type t when t == typeof(ulong):
                    return bwType.GetMethod(isNullable ? "WriteNullable" : "Write", new[] { isNullable ? typeof(ulong?) : typeof(ulong) });
                case Type t when t == typeof(DateTime):
                    return bwType.GetMethod(isNullable ? "WriteNullable" : "Write", new[] { isNullable ? typeof(DateTime?) : typeof(DateTime) });
                case Type t when t == typeof(TimeSpan):
                    return bwType.GetMethod(isNullable ? "WriteNullable" : "Write", new[] { isNullable ? typeof(TimeSpan?) : typeof(TimeSpan) });
                case Type t when t == typeof(DateTimeOffset):
                    return bwType.GetMethod(isNullable ? "WriteNullable" : "Write", new[] { isNullable ? typeof(DateTimeOffset?) : typeof(DateTimeOffset) });
                case Type t when t == typeof(Guid):
                    return bwType.GetMethod(isNullable ? "WriteNullable" : "Write", new[] { isNullable ? typeof(Guid?) : typeof(Guid) });
                case Type t when t == typeof(string):
                    return bwType.GetMethod("Write", new[] { typeof(string) });
                case Type t when t == typeof(byte[]):
                    return bwType.GetMethod("Write", new[] { typeof(byte[]) });
                case Type t when t == typeof(object):
                    return bwType.GetMethod("Write", new[] { typeof(object[]) });
                default:
                    if (underlyingType.IsEnum)
                        return bwType.GetMethod("Write", new[] { isNullable ? typeof(int?) : typeof(int) });
                    else
                        throw new NotSupportedException($"Binary writer for property type {type} (underlying type {underlyingType}) is not supported.");
            }
        }

        private static MethodInfo Type2BinaryReader(Type type)
        {
            Type nullableType = Nullable.GetUnderlyingType(type);
            bool isNullable = nullableType != null;
            Type underlyingType = nullableType ?? type;
            Type bwType = typeof(BinaryReaderEx);
            switch (underlyingType)
            {
                //TODO: Reorder cases
                case Type t when t == typeof(bool):
                    return bwType.GetMethod(isNullable ? "ReadNullableBoolean" : "ReadBoolean", Type.EmptyTypes);
                case Type t when t == typeof(byte):
                    return bwType.GetMethod(isNullable ? "ReadNullableByte" : "ReadByte", Type.EmptyTypes);
                case Type t when t == typeof(char):
                    return bwType.GetMethod(isNullable ? "ReadNullableChar" : "ReadChar", Type.EmptyTypes);
                case Type t when t == typeof(decimal):
                    return bwType.GetMethod(isNullable ? "ReadNullableDecimal" : "ReadDecimal", Type.EmptyTypes);
                case Type t when t == typeof(double):
                    return bwType.GetMethod(isNullable ? "ReadNullableDouble" : "ReadDouble", Type.EmptyTypes);
                case Type t when t == typeof(short):
                    return bwType.GetMethod(isNullable ? "ReadNullableInt16" : "ReadInt16", Type.EmptyTypes);
                case Type t when t == typeof(int):
                    return bwType.GetMethod(isNullable ? "ReadNullableInt32" : "ReadInt32", Type.EmptyTypes);
                case Type t when t == typeof(long):
                    return bwType.GetMethod(isNullable ? "ReadNullableInt64" : "ReadInt64", Type.EmptyTypes);
                case Type t when t == typeof(sbyte):
                    return bwType.GetMethod(isNullable ? "ReadNullableSByte" : "ReadSByte", Type.EmptyTypes);
                case Type t when t == typeof(float):
                    return bwType.GetMethod(isNullable ? "ReadNullableSingle" : "ReadSingle", Type.EmptyTypes);
                case Type t when t == typeof(ushort):
                    return bwType.GetMethod(isNullable ? "ReadNullableUInt16" : "ReadUInt16", Type.EmptyTypes);
                case Type t when t == typeof(uint):
                    return bwType.GetMethod(isNullable ? "ReadNullableUInt32" : "ReadUInt32", Type.EmptyTypes);
                case Type t when t == typeof(ulong):
                    return bwType.GetMethod(isNullable ? "ReadNullableUInt64" : "ReadUInt64", Type.EmptyTypes);
                case Type t when t == typeof(DateTime):
                    return bwType.GetMethod(isNullable ? "ReadNullableDateTime" : "ReadDateTime", Type.EmptyTypes);
                case Type t when t == typeof(TimeSpan):
                    return bwType.GetMethod(isNullable ? "ReadNullableTimeSpan" : "ReadTimeSpan", Type.EmptyTypes);
                case Type t when t == typeof(DateTimeOffset):
                    return bwType.GetMethod(isNullable ? "ReadNullableDateTimeOffset" : "ReadDateTimeOffset", Type.EmptyTypes);
                case Type t when t == typeof(Guid):
                    return bwType.GetMethod(isNullable ? "ReadNullableGuid" : "ReadGuid", Type.EmptyTypes);
                case Type t when t == typeof(string):
                    return bwType.GetMethod("ReadString", Type.EmptyTypes);
                case Type t when t == typeof(byte[]):
                    return bwType.GetMethod("ReadBytes", Type.EmptyTypes);
                case Type t when t == typeof(object):
                    return bwType.GetMethod("ReadObject", Type.EmptyTypes);
                default:
                    if (underlyingType.IsEnum)
                        return bwType.GetMethod(isNullable ? "ReadNullableInt32" : "ReadInt32", Type.EmptyTypes);
                    else
                        throw new NotSupportedException($"Binary reader for property type {type} (underlying type {underlyingType}) is not supported.");
            }
        }

        /// <summary>
        /// Finds the best suitable accessor to a <see cref="IDataReader"/> field, given the data field type and destination type.
        /// The field is accessed indirectly through a <see cref="DataReaderWrapper"/>, whose type is also given.
        /// </summary>
        /// <param name="dstType">The destination property type.</param>
        /// <param name="srcType">The source field type in the <see cref="IDataReader"/>).</param>
        /// <param name="drwType">The type of <see cref="DataReaderWrapper"/> that wraps calls to the reader.</param>
        /// <returns></returns>
        private static MethodInfo DiscoverDataReaderMethod(Type dstType, Type srcType, Type drwType)
        {
            Type nullableType = Nullable.GetUnderlyingType(dstType);
            bool isNullable = nullableType != null;
            Type underlyingType = nullableType ?? dstType;
            if (underlyingType == srcType)
            {
                foreach (var m in drwType.GetMethods(BindingFlags.Instance | BindingFlags.Public))
                {
                    if (m.ReturnType == dstType)
                    {
                        var getAttr = m.GetCustomAttribute(typeof(GetAttribute));
                        if (getAttr != null)
                        {
                            Debug.Assert(m.Name.StartsWith("Get"));
                            var ps = m.GetParameters();
                            if (ps.Length == 1 && ps[0].ParameterType == typeof(int))
                                return m;
                        }
                    }
                }
            }
            else
            {
                foreach (var m in drwType.GetMethods(BindingFlags.Instance | BindingFlags.Public))
                {
                    if (m.ReturnType == dstType)
                    {
                        var cnvAttr = m.GetCustomAttribute(typeof(ConvertAttribute));
                        if (cnvAttr != null)
                        {
                            Debug.Assert(m.Name.StartsWith("Convert"));
                            var ps = m.GetParameters();
                            if (ps.Length == 1 && ps[0].ParameterType == typeof(int))
                            {
                                return m;
                            }
                        }
                    }
                }
            }
            return null;
        }
    }
}