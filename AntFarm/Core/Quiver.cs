﻿using System;
using System.Collections.Generic;

namespace AntFarm.Core
{
    /// <summary>
    /// Cache of objects of type <see cref="T"/>.
    /// Objects are referenced by an external key (i.e., not an attribute of the object itself.).
    /// </summary>
    /// <typeparam name="T">The type of cached object.</typeparam>
    public class Quiver<T>
    {
        private readonly Dictionary<string, T> __Cache = new Dictionary<string, T>();

        /// <summary>
        /// Gets an object from the cache, given its key. If the object is not found, creates it and adds it to the cache.
        /// </summary>
        /// <param name="key">The object identifier in the cache.</param>
        /// <param name="createNew">A function to create the object if not found.</param>
        /// <returns></returns>
        public T GetArrow(string key, Func<T> createNew)
        {
            if (!__Cache.TryGetValue(key, out T item))
                lock (__Cache)
                    if (!__Cache.TryGetValue(key, out item))
                    {
                        item = (createNew ?? throw new ArgumentNullException(nameof(createNew))).Invoke();
                        __Cache.Add(key, item);
                    }
            return item;
        }

    }
}