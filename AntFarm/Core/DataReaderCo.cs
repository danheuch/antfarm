﻿using AntFarm.ORM;
using AntFarm.ORM.Providers;
using System;
using System.Data;
using System.IO;

namespace AntFarm.Core
{
    public class DataReaderCo
    {
        public DataReaderCo(IDataReader dr, IFormatProvider fp)
        {
            DataReader = dr ?? throw new ArgumentNullException(nameof(dr));
            FormatProvider = fp;
        }

        public IDataReader DataReader { get; }
        public IFormatProvider FormatProvider { get; }

        /// <summary>
        /// Returns a Base-64 string uniquely representing the reader's data content in relation to the type of object
        /// it populates.
        /// </summary>
        public virtual string GetSignature(TableProvider tbProvider)
        {
#if true
            using (var s = new MemoryStream(4 + 4 + 4 + (DataReader.FieldCount * 3 * 4)))
            {
                using (var w = new BinaryWriter(s))
                {
                    //Mark the populated object's type, which is one side of the mapping.
                    //NOTE: This assumes the table info is not manipulated between calls.
                    //TODO: Is it safe to assume that? Consider improvements.
                    w.Write(tbProvider.GUID.GetHashCode());
                    //Mark this class' type, in case it's overriden.
                    w.Write(this.GetType().GUID.GetHashCode());
                    //Mark the data reader's type.
                    w.Write(DataReader.GetType().GUID.GetHashCode());
                    //Mark the data reader's field types.
                    for (int i = 0; i < DataReader.FieldCount; i++)
                    {
                        //Names of the fields must be present because they are used to map to the populated object's properties.
                        w.Write(DataReader.GetName(i).GetHashCode());
                        //Data's .NET type
                        //NOTE: GetFieldType does not always return a value, e.g., with special SQL types like "geography" and others.
                        w.Write((DataReader.GetFieldType(i)?.GUID ?? new Guid()).GetHashCode());
                        //Data's SQL type
                        w.Write(DataReader.GetDataTypeName(i).GetHashCode());
                    }
                    return Convert.ToBase64String(s.GetBuffer());
                }
            }
#else
            //TODO: Improve this to make it a more compact key. It must be unique.
            var sb = new StringBuilder();
            //Mark the populated object's type, which is one side of the mapping.
            sb.Append(typeof(T).GUID.GetHashCode());
            sb.Append(tinfo.GUID.GetHashCode());
            //Mark this class' type, in case it's overriden.
            sb.Append(this.GetType().GUID.GetHashCode());
            //Mark the data reader's type.
            sb.Append(DataReader.GetType().GUID.GetHashCode());
            //Mark the data reader's field types.
            for (int i = 0; i < DataReader.FieldCount; i++)
                sb
                    //Names of the fields must be present because they are used to map to the populated object's properties.
                    .Append(DataReader.GetName(i).GetHashCode().GetHashCode())
                    //Data's .NET type
                    .Append(DataReader.GetFieldType(i).GUID.GetHashCode())
                    //Data's SQL type
                    .Append(DataReader.GetDataTypeName(i).GetHashCode());
            return Convert.ToBase64String(Encoding.UTF8.GetBytes(sb.ToString()));
#endif
        }

        /// <summary>
        /// Throws an <see cref="InvalidOperationException"/> if more rows can be read.
        /// </summary>
        public void ThrowIfContinueReading()
        {
            if (DataReader.Read())
                throw new InvalidOperationException("More rows are available to read than expected.");
        }

        /* Base Types (no conversion) */

        [Get]public bool GetBoolean(int i) => DataReader.GetBoolean(i);
        [Get] public byte GetByte(int i) => DataReader.GetByte(i);
        [Get] public virtual sbyte GetSByte(int i) => Convert.ToSByte(DataReader.GetValue(i));
        [Get] public short GetInt16(int i) => DataReader.GetInt16(i);
        [Get] public virtual ushort GetUInt16(int i) => Convert.ToUInt16(DataReader.GetValue(i));
        [Get] public int GetInt32(int i) => DataReader.GetInt32(i);
        [Get] public virtual uint GetUInt32(int i) => Convert.ToUInt32(DataReader.GetValue(i));
        [Get] public long GetInt64(int i) => DataReader.GetInt64(i);
        [Get] public virtual ulong GetUInt64(int i) => Convert.ToUInt64(DataReader.GetValue(i));
        [Get] public float GetSingle(int i) => DataReader.GetFloat(i);
        [Get] public double GetDouble(int i) => DataReader.GetDouble(i);
        [Get] public decimal GetDecimal(int i) => DataReader.GetDecimal(i);
        [Get] public char GetChar(int i) => DataReader.GetChar(i);
        [Get] public string GetString(int i) => DataReader.IsDBNull(i) ? null : DataReader.GetString(i);
        [Get] public DateTime GetDateTime(int i) => DataReader.GetDateTime(i);
        [Get] public virtual DateTimeOffset GetDateTimeOffset(int i) => throw new NotSupportedException($"Type {typeof(DateTimeOffset)} is not supported.");
        [Get] public virtual TimeSpan GetTimeSpan(int i) => throw new NotSupportedException($"Type {typeof(TimeSpan)} is not supported.");
        [Get] public Guid GetGuid(int i) => DataReader.GetGuid(i);
        [Get] public object GetObject(int i) => DataReader.IsDBNull(i) ? null : DataReader.GetValue(i);

        /* Base Nullable Types (no conversion) */

        [Get] public bool? GetNullableBoolean(int i) => DataReader.IsDBNull(i) ? (bool?)null : DataReader.GetBoolean(i);
        [Get] public byte? GetNullableByte(int i) => DataReader.IsDBNull(i) ? (byte?)null : DataReader.GetByte(i);
        [Get] public sbyte? GetNullableSByte(int i) => DataReader.IsDBNull(i) ? (sbyte?)null : this.GetSByte(i);
        [Get] public short? GetNullableInt16(int i) => DataReader.IsDBNull(i) ? (short?)null : DataReader.GetInt16(i);
        [Get] public ushort? GetNullableUInt16(int i) => DataReader.IsDBNull(i) ? (ushort?)null : this.GetUInt16(i);
        [Get] public int? GetNullableInt32(int i) => DataReader.IsDBNull(i) ? (int?)null : DataReader.GetInt32(i);
        [Get] public uint? GetNullableUInt32(int i) => DataReader.IsDBNull(i) ? (uint?)null : this.GetUInt32(i);
        [Get] public long? GetNullableInt64(int i) => DataReader.IsDBNull(i) ? (long?)null : DataReader.GetInt64(i);
        [Get] public ulong? GetNullableUInt64(int i) => DataReader.IsDBNull(i) ? (ulong?)null : this.GetUInt64(i);
        [Get] public float? GetNullableSingle(int i) => DataReader.IsDBNull(i) ? (float?)null : DataReader.GetFloat(i);
        [Get] public double? GetNullableDouble(int i) => DataReader.IsDBNull(i) ? (double?)null : DataReader.GetDouble(i);
        [Get] public decimal? GetNullableDecimal(int i) => DataReader.IsDBNull(i) ? (decimal?)null : DataReader.GetDecimal(i);
        [Get] public char? GetNullableChar(int i) => DataReader.IsDBNull(i) ? (char?)null : DataReader.GetChar(i);
        [Get] public DateTime? GetNullableDateTime(int i) => DataReader.IsDBNull(i) ? (DateTime?)null : DataReader.GetDateTime(i);
        [Get] public DateTimeOffset? GetNullableDateTimeOffset(int i) => DataReader.IsDBNull(i) ? (DateTimeOffset?)null : this.GetDateTimeOffset(i);
        [Get] public TimeSpan? GetNullableTimeSpan(int i) => DataReader.IsDBNull(i) ? (TimeSpan?)null : this.GetTimeSpan(i);
        [Get] public Guid? GetNullableGuid(int i) => DataReader.IsDBNull(i) ? (Guid?)null : DataReader.GetGuid(i);

        /* Conversion to Base Types */

        [Convert]public bool ConvertBoolean(int i) => Convert.ToBoolean(DataReader.GetValue(i), FormatProvider);
        [Convert]public byte ConvertByte(int i) => Convert.ToByte(DataReader.GetValue(i), FormatProvider);
        [Convert]public sbyte ConvertSByte(int i) => Convert.ToSByte(DataReader.GetValue(i), FormatProvider);
        [Convert]public short ConvertInt16(int i) => Convert.ToInt16(DataReader.GetValue(i), FormatProvider);
        [Convert]public ushort ConvertUInt16(int i) => Convert.ToUInt16(DataReader.GetValue(i), FormatProvider);
        [Convert]public int ConvertInt32(int i) => Convert.ToInt32(DataReader.GetValue(i), FormatProvider);
        [Convert]public uint ConvertUInt32(int i) => Convert.ToUInt32(DataReader.GetValue(i), FormatProvider);
        [Convert]public long ConvertInt64(int i) => Convert.ToInt64(DataReader.GetValue(i), FormatProvider);
        [Convert]public ulong ConvertUInt64(int i) => Convert.ToUInt64(DataReader.GetValue(i), FormatProvider);
        [Convert]public float ConvertSingle(int i) => Convert.ToSingle(DataReader.GetValue(i), FormatProvider);
        [Convert]public double ConvertDouble(int i) => Convert.ToDouble(DataReader.GetValue(i), FormatProvider);
        [Convert]public decimal ConvertDecimal(int i) => Convert.ToDecimal(DataReader.GetValue(i), FormatProvider);
        [Convert]public char ConvertChar(int i) => Convert.ToChar(DataReader.GetValue(i), FormatProvider);
        [Convert]public string ConvertString(int i) => Convert.ToString(DataReader.GetValue(i), FormatProvider);
        [Convert]public DateTime ConvertDateTime(int i) => Convert.ToDateTime(DataReader.GetValue(i), FormatProvider);

        /* Conversion to Nullable Base Types */

        [Convert]public bool? ConvertNullableBoolean(int i) => DataReader.IsDBNull(i) ? (bool?)null : this.ConvertBoolean(i);
        [Convert]public byte? ConvertNullableByte(int i) => DataReader.IsDBNull(i) ? (byte?)null : this.ConvertByte(i);
        [Convert]public sbyte? ConvertNullableSByte(int i) => DataReader.IsDBNull(i) ? (sbyte?)null : this.ConvertSByte(i);
        [Convert]public short? ConvertNullableInt16(int i) => DataReader.IsDBNull(i) ? (short?)null : this.ConvertInt16(i);
        [Convert]public ushort? ConvertNullableUInt16(int i) => DataReader.IsDBNull(i) ? (ushort?)null : this.ConvertUInt16(i);
        [Convert]public int? ConvertNullableInt32(int i) => DataReader.IsDBNull(i) ? (int?)null : this.ConvertInt32(i);
        [Convert]public uint? ConvertNullableUInt32(int i) => DataReader.IsDBNull(i) ? (uint?)null : this.ConvertUInt32(i);
        [Convert]public long? ConvertNullableInt64(int i) => DataReader.IsDBNull(i) ? (long?)null : this.ConvertInt64(i);
        [Convert]public ulong? ConvertNullableUInt64(int i) => DataReader.IsDBNull(i) ? (ulong?)null : this.ConvertUInt64(i);
        [Convert]public float? ConvertNullableSingle(int i) => DataReader.IsDBNull(i) ? (float?)null : this.ConvertSingle(i);
        [Convert]public double? ConvertNullableDouble(int i) => DataReader.IsDBNull(i) ? (double?)null : this.ConvertDouble(i);
        [Convert]public decimal? ConvertNullableDecimal(int i) => DataReader.IsDBNull(i) ? (decimal?)null : this.ConvertDecimal(i);
        [Convert]public char? ConvertNullableChar(int i) => DataReader.IsDBNull(i) ? (char?)null : this.ConvertChar(i);
        [Convert]public DateTime? ConvertNullableDateTime(int i) => DataReader.IsDBNull(i) ? (DateTime?)null : this.ConvertDateTime(i);


        /* BLOB / CLOB */

#if false
        [Get]public virtual byte[] GetBytes(int i) => DataReader.IsDBNull(i) ? null : (byte[])DataReader.GetValue(i);
#else
        [Get]public virtual byte[] GetBytes(int i)
        {
            if (DataReader.IsDBNull(i))
                return null;
            int length = (int)DataReader.GetBytes(i, 0, null, 0, 0);
            var buffer = new byte[length];
            if (length > 0)
                DataReader.GetBytes(i, 0, buffer, 0, length);
            return buffer;
        }
#endif

#if false
        [Get]public virtual char[] GetChars(int i) => DataReader.IsDBNull(i) ? null : (char[])DataReader.GetValue(i);
#else
        [Get]public virtual char[] GetChars(int i)
        {
            if (DataReader.IsDBNull(i))
                return null;
            int length = (int)DataReader.GetChars(i, 0, null, 0, 0);
            var buffer = new char[length];
            if (length > 0)
                DataReader.GetChars(i, 0, buffer, 0, length);
            return buffer;
        }
#endif
    }
}