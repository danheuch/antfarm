﻿using System;
using System.Diagnostics;
using System.Reflection;

namespace AntFarm.Core
{
    internal static class Utils
    {
        /// <summary>
        /// Set any property with any compatible value.
        /// Note that this method useful for prototyping features, but is not substitute for much faster dynamic methods specially dedicated to the
        /// task at hand.
        /// </summary>
        public static void SetPropertyValue(PropertyInfo accessor, object instance, object srcValue, Type srcType = null)
        {
            if (srcValue == null || Convert.IsDBNull(srcValue))
            {
                if (!accessor.PropertyType.IsAssignableFromNull())
                    throw new Exception($"For property \"{accessor.Name}\" of type {accessor.DeclaringType} - Unable to assign from null.");
                accessor.SetValue(instance, null);
            }
            else
            {
                Type dstType = accessor.PropertyType;
                dstType = Nullable.GetUnderlyingType(dstType) ?? dstType;
                if (srcType == null)
                    srcType = srcValue.GetType();
                else
                    Debug.Assert(srcType == srcValue.GetType());
                if (!dstType.IsAssignableFrom(srcType))
                {
                    if (srcType == typeof(TimeSpan) && dstType == typeof(DateTime))
                    {
                        //For now, system requires TimeSpan to be represented by DateTime property, because that
                        //is the way SQL Server expects it in a parameterized query, and there is no good way to
                        //convert from TimeSpan to DateTime. Here, we simply add the TimeSpan value to the smallest
                        //date (1/1/1).
                        srcValue = DateTime.MinValue.AddTicks(((TimeSpan)srcValue).Ticks);
                        srcType = dstType;
                    }
                    else if (srcType == typeof(DateTimeOffset) && dstType == typeof(DateTime))
                    {
                        //Using System.Convert to convert a DateTimeOffset to DateTime does not work. Must be done explicitely. 
                        srcValue = ((DateTimeOffset)srcValue).DateTime.ToUniversalTime();
                        srcType = typeof(DateTime);
                    }
                    else
                    {
                        try
                        {
                            if (dstType.IsEnum)
                            {
                                srcValue = Convert.ChangeType(srcValue, typeof(int), null);
                                srcValue = Enum.ToObject(dstType, srcValue);
                            }
                            else
                                srcValue = Convert.ChangeType(srcValue, dstType, null);
                        }
                        catch (Exception ex)
                        {
                            throw new Exception($"For property \"{accessor.Name}\" of type {accessor.DeclaringType} - Unable to convert \"{srcValue}\" to {dstType}.", ex);
                        }
                        srcType = dstType;
                    }
                    Debug.Assert(dstType.IsAssignableFrom(srcType));
                }
                accessor.SetValue(instance, srcValue);
            }
        }
    }
}