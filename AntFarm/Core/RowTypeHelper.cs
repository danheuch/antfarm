﻿using System;

namespace AntFarm.Core
{
    /// <summary>
    /// Helper functions for the management of row types.
    /// </summary>
    internal static class RowTypeHelper
    {
        /// <summary>
        /// Determines that the given row type is declared as dynamic.
        /// </summary>
        public static bool IsPlainObject(Type rowType)
        {
            //TODO: I don't know if there is a good way to know that a generic type is declared as "dynamic".
            //AFAICT it's just passed as a System.Object. Seems to work, but may have to be improved.
            return rowType == typeof(object);
        }
        /// <summary>
        /// Determines that the given row type is declared as an object array.
        /// </summary>
        public static bool IsObjectArray(Type rowType)
        {
            //This is straightforward...
            return rowType == typeof(object[]);
        }
        /// <summary>
        /// Determines that the given row type is a value tuple.
        /// </summary>
        public static bool IsGenericValueTuple(Type rowType)
        {
            //TODO: I googled ways to determine that, and it's ugly. The following code seems good enough for now.
            //May have to be improved.
            return rowType.IsGenericType && rowType.Name.Contains("ValueTuple");
        }

        /// <summary>
        /// Determines whether the row type describes a table, in which case it can be used to get the table name, column
        /// names, other column details, possibly the primary key, etc. If false, such as when the row type is simply
        /// described as "dynamic", an object array, or a value tuple, then none of that info can be gleaned from the type.
        /// </summary>
        /// <remarks>
        /// This is a fuzzy concept in my mind, and this method should be considered more like a placeholder for a more
        /// meaningful assessment of the type. The key point is the fact that some row types don't describe a table at
        /// all, such as when the type is declared as "dynamic" to the selection method (in which case an ExpandoObject is
        /// returned).
        /// TODO: Improve the method or modify the approach to this issue.
        /// </remarks>
        public static bool PossiblyDescribesATable(Type rowType)
        {
            return !IsPlainObject(rowType) && !IsObjectArray(rowType) && !IsGenericValueTuple(rowType);
        }
    }
}
