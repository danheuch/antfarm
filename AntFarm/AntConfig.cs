﻿using AntFarm.ORM;
using AntFarm.ORM.Providers;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;

namespace AntFarm
{
    /// <summary>
    /// Configuration information.
    /// </summary>
    public class AntConfig
    {
        public bool DefaultChecksReferentialIntegrity { get; internal set; }
        /// <summary>
        /// The default user name used, which is used to tag records through audit fields.
        /// </summary>
        public string DefaultUserName { get; internal set; }
        /// <summary>
        /// The default <see cref="CultureInfo"/> used to format or interpret data during conversions.
        /// </summary>
        public CultureInfo DefaultDataCulture { get; internal set; } = CultureInfo.CurrentCulture;
        /// <summary>
        /// The time zone of any date/time expressed in the data, which by default is the local time zone.
        /// </summary>
        public TimeZoneInfo DefaultDataTimeZone { get; internal set; } = TimeZoneInfo.Local;
        /// <summary>
        /// The <see cref="ILogger"/> factory.
        /// </summary>
        public ILoggerFactory LoggerFactory { get; internal set; }
        /// <summary>
        /// Whether SQL statements are logged. If true, SQL activity is logged at the Debug level.
        /// </summary>
        public bool SqlLoggingEnabled { get; internal set; }
        /// <summary>
        /// The list of registered connection strings.
        /// </summary>
        public ReadOnlyDictionary<string, ConnectionStringInfo> ConnectionStrings { get; internal set; }
        /// <summary>
        /// Creates a new database.
        /// </summary>
        /// <param name="cskey">The connection string key.</param>
        /// <returns>A <see cref="Database"/> instance.</returns>
        public Database GetDatabase(string cskey) => new Database(this, cskey);
        /// <summary>
        /// Creates a new database.
        /// </summary>
        /// <param name="cskey">The connection string key.</param>
        /// <param name="userName">A value to override DefaultUserName.</param>
        /// <returns>A <see cref="Database"/> instance.</returns>
        public Database GetDatabase(string cskey, string userName) => new Database(this, cskey) { UserName = userName };
        /// <summary>
        /// Creates a new database.
        /// </summary>
        /// <param name="cskey">The connection string key.</param>
        /// <param name="userName">A value to override DefaultUserName.</param>
        /// <param name="culture">A value to override DefaultDataCulture.</param>
        /// <returns>A <see cref="Database"/> instance.</returns>
        public Database GetDatabase(string cskey, string userName, CultureInfo culture) => new Database(this, cskey) { UserName = userName, DataCulture = culture };
    }

    /// <summary>
    /// Information about a database connection.
    /// </summary>
    public class ConnectionStringInfo
    {
        internal ConnectionStringInfo(DatabaseProvider provider, string connectionString)
        {
            Provider = provider;
            ConnectionString = connectionString;
        }
        /// <summary>
        /// The <see cref="IDatabaseProvider"/>, which implements provider-specific features.
        /// </summary>
        public DatabaseProvider Provider { get; }
        /// <summary>
        /// The connection string used to connect to the database.
        /// </summary>
        public string ConnectionString { get; }
    }

    /// <summary>
    /// Helps build an AntFarm configuration.
    /// This class cannot be directly instantiated.
    /// Use <see cref="AntFarmer"/> static Configure method to start the building process.
    /// </summary>
    public class ConfigBuilder
    {
        private readonly AntConfig _Config = new AntConfig();
        private readonly Dictionary<string, (Type t, string cs)> _ConnectionStrings =
            new Dictionary<string, (Type t, string cs)>();

        private bool _IsDefault;

        internal ConfigBuilder() { }

        public ConfigBuilder SetDefaultCheckIntegrity(bool checkIntegrity)
        {
            _Config.DefaultChecksReferentialIntegrity = checkIntegrity;
            return this;
        }

        public ConfigBuilder SetDefaultUserName(string userName)
        {
            _Config.DefaultUserName = userName;
            return this;
        }

        public ConfigBuilder SetDefaultDataCulture(CultureInfo culture)
        {
            _Config.DefaultDataCulture = culture ?? CultureInfo.CurrentCulture;
            return this;
        }

        public ConfigBuilder SetDefaultDataTimeZone(TimeZoneInfo timeZone)
        {
            _Config.DefaultDataTimeZone = timeZone ?? TimeZoneInfo.Local;
            return this;
        }

        public ConfigBuilder SetLoggerFactory(ILoggerFactory factory)
        {
            _Config.LoggerFactory = factory;
            return this;
        }

        public ConfigBuilder EnableSqlLogging(bool value = true)
        {
            _Config.SqlLoggingEnabled = value;
            return this;
        }

        public ConfigBuilder AddConnectionString<T>(string key, string connectionString)
            where T : DatabaseProvider
        {
            _ConnectionStrings.Add(key, (typeof(T), connectionString));
            return this;
        }

        public ConfigBuilder SetAsDefault(bool value = true)
        {
            _IsDefault = value;
            return this;
        }

        /// <summary>
        /// Finalizes the building of the configuration.
        /// </summary>
        /// <returns>The resulting <see cref="AntConfig"/> instance.</returns>
        internal AntConfig Wrapup()
        {
            var dic = new Dictionary<string, ConnectionStringInfo>();
            foreach (var item in _ConnectionStrings)
            {
                var provider = (DatabaseProvider)Activator.CreateInstance(item.Value.t, new[] { _Config });
                dic.Add(item.Key, new ConnectionStringInfo(provider, item.Value.cs));
            }
            _Config.ConnectionStrings = new ReadOnlyDictionary<string, ConnectionStringInfo>(dic);
            if (_IsDefault)
                AntFarmer.DefaultConfig = _Config;
            return _Config;
        }
    }
}
