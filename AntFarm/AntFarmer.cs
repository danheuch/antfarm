﻿using System;

namespace AntFarm
{
    /// <summary>
    /// Provides methods to configure the library.
    /// </summary>
    public static class AntFarmer
    {
        /// <summary>
        /// The default configuration to use when working with <see cref="Database"/> instances.
        /// Use <see cref="ConfigBuilder"/>'s SetAsDefault methods to set this property.
        /// </summary>
        public static AntConfig DefaultConfig { get; internal set; }

        /// <summary>
        /// Builds a configuration setup to use when working with <see cref="Database"/> instances. 
        /// </summary>
        /// <param name="configurator">A callback where the actual configuration is to take place.</param>
        /// <returns>A finalized <see cref="AntConfig"/> instance.</returns>
        public static AntConfig Configure(Action<ConfigBuilder> configurator)
        {
            var builder = new ConfigBuilder();
            configurator?.Invoke(builder);
            return builder.Wrapup();
        }
    }
}