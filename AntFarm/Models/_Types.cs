﻿using System;

namespace AntFarm.Models
{
    /// <summary>
    /// Status of a <see cref="IRecord"/> row value relative to the corresponding database row.
    /// </summary>
    public enum RowStatus
    {
        /// <summary>
        /// Indicates that the record is up-to-date and does not need to be saved to database. Note that it is
        /// possible for the status to be Current and the value (see <see cref="IRecord"/>.Value property) to be null, which is
        /// the case when the database record does not exist and the in-memory version is just a placeholder.
        /// </summary>
        Current,
        /// <summary>
        /// Indicates that the record does not exist in the database, and needs to be inserted.
        /// </summary>
        New,
        /// <summary>
        /// Indicates that the record exists in the database, but needs to be updated.
        /// </summary>
        Modified,
        /// <summary>
        /// Indicates that the record exists in the database, but needs to be deleted.
        /// </summary>
        Deleted
    }
}