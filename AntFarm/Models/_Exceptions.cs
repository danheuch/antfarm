﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AntFarm.Models
{
    public class DeleteOrModifyOverDeleteException : Exception
    {
        public DeleteOrModifyOverDeleteException(Type t)
            : base($"Trying to delete or modify a " +
            $"deleted {t}. The deletion may have been caused by a cascading operation. " +
            $"Consider setting the model's EnforceStrictMergingRules to false.")
        { }
    }

    public class InvalidInternalStateException : Exception
    {
        public InvalidInternalStateException(Type t, RowStatus s, object value)
            : base($"Invalid internal state. The type is {t}, status is {s} and value is {value}.")
        { }
        public InvalidInternalStateException(IRecord mp)
            : base($"Invalid internal state. The type is {mp.RecordManager.RowType}, Status is {mp.Status} and HasValue is {mp.HasValue}.")
        { }
    }
}