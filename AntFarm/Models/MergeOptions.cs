﻿namespace AntFarm.Models
{
    /// <summary>
    /// Options controlling how data is merged into the <see cref="RecordList{TRow}"/>.
    /// </summary>
    public class MergeOptions
    {
        /// <summary>
        /// Whether strict merging rules are enforced. See EnforceStrictMergingRules of <see cref="ModelBase"/> for more info.
        /// </summary>
        public bool StrictMergingRules { get; set; } = true;
        /// <summary>
        /// Insert new records.
        /// </summary>
        public bool AllowInsert { get; set; } = true;
        /// <summary>
        /// Update existing records.
        /// </summary>
        public bool AllowUpdate { get; set; } = true;
        /// <summary>
        /// Delete existing records.
        /// </summary>
        public bool AllowDelete { get; set; } = true;
    }
}