﻿using AntFarm.Core;
using AntFarm.ORM;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization.Formatters.Binary;

namespace AntFarm.Models
{
    /// <summary>
    /// Base class for data models.
    /// </summary>
    public class ModelBase
    {
        private readonly List<IRecordSource> _ManagedPocoSources = new List<IRecordSource>();
        private readonly Dictionary<string, RecordManager> _PocoManagers = new Dictionary<string, RecordManager>();

        private readonly AntConfig _Config;
        private readonly string _DBKey;
        private readonly IDatabaseProvider _Provider;
        private readonly ILogger _Logger;

        private List<IRecord> __CachedManagedPocoSet;

        /// <summary>
        /// Initializes a new <see cref="ModelBase"./>
        /// </summary>
        public ModelBase(AntConfig config, string dbkey)
        {
            _Config = config ?? throw new ArgumentNullException(nameof(config));
            _DBKey = dbkey ?? throw new ArgumentNullException(nameof(dbkey));
            _Provider = config.ConnectionStrings[dbkey].Provider;
            _Logger = config.LoggerFactory?.CreateLogger<ModelBase>();
            ChecksReferentialIntegrity = config.DefaultChecksReferentialIntegrity;
            EnforcesStrictMergingRules = true; //TODO: config
        }

        /// <summary>
        /// Whether the data model checks for referential integrity errors.
        /// </summary>
        public bool ChecksReferentialIntegrity { get; set; }

        /// <summary>
        /// Whether strict merging rules are enforced when merging data into the model. The rules apply
        /// to the status of records being deleted or modified. If true (the default), things like updating a deleted
        /// record are not allowed. See remarks for more info.
        /// </summary>
        /// <remarks>
        /// While this kind of enforcement is usually a good thing, it can be a challenge when dealing
        /// with cascaded-down changes like deletions, wherein the model providing the data to merge must also be
        /// able to anticipate and reflect those spreading changes in the data being merged. For example, a view model
        /// representing invoices and their line items would have to make sure that the line items of deleted invoices
        /// are not merged into the data model. In that situation, it may be simpler to let the model cascade the delete
        /// from invoice to line items, and ignore the deleted line items when they are merged.
        /// </remarks>
        public bool EnforcesStrictMergingRules { get; set; }

        public RecordList<T> Add<T>() { return this.Add((IEnumerable<T>)null); }

        public RecordList<T> Add<T>(T poco) { return this.Add(new T[] { poco } as IEnumerable<T>); }

        public RecordList<T> Add<T>(IEnumerable<T> pocos) { return new RecordList<T>(this, pocos); }

        /// <summary>
        /// Clears the model of all data.
        /// </summary>
        public void Clear()
        {
            foreach (var source in _ManagedPocoSources)
                source.Clear();
        }

        /// <summary>
        /// Saves pending modifications.
        /// </summary>
        public void Save()
        {
            if (ChecksReferentialIntegrity)
                ThrowIfNoReferentialIntegrity();
            using (var db = this.GetDatabase())
                this.Save(db);
        }

        protected Database GetDatabase()
        {
            return _Config.GetDatabase(_DBKey);
        }

        internal List<IRecord> ManagedSet
        {
            get
            {
                if (__CachedManagedPocoSet == null)
                {
                    __CachedManagedPocoSet = new List<IRecord>();
                    foreach (var source in _ManagedPocoSources)
                        foreach (var record in source)
                            __CachedManagedPocoSet.Add(record);
                }
                return __CachedManagedPocoSet;
            }
        }

        #region Registration

        internal RecordManager RegisterPocoType(Type pocoType, IRecordSource source)
        {
            if (_PocoManagers.TryGetValue(pocoType.FullName, out RecordManager pocoMgr))
                throw new InvalidOperationException($"The type {pocoType} is already registered.");
            pocoMgr = new RecordManager(pocoType, _Provider);
            _PocoManagers[pocoType.FullName] = pocoMgr;
            DiscoverChildToParentRelationships(pocoMgr);
            DiscoverParentToChildRelationships(pocoMgr);
            _ManagedPocoSources.Add(source);
            InvalidateCache();
            return pocoMgr;
        }

        internal void InvalidateCache()
        {
            __CachedManagedPocoSet = null;
        }

        /// <summary>
        /// Scan child to parent references and establish relationships.
        /// </summary>
        /// <param name="pocoMgr">The new registered <see cref="RecordManager"/> being discovered.</param>
        private void DiscoverChildToParentRelationships(RecordManager pocoMgr)
        {
            Type pocoType = pocoMgr.RowType;
            foreach (var fkProp in pocoType.GetProperties())
            {
                var refAttr = (ReferencesAttribute)fkProp.GetCustomAttribute(typeof(ReferencesAttribute), true);
                if (refAttr != null)
                {
                    pocoMgr.ReferencesAttributesByProperty.Add((fkProp, refAttr));
                    if (_PocoManagers.TryGetValue(refAttr.ParentType.FullName, out RecordManager parentMgr))
                    {
                        var pkProp = refAttr.PropertyName == null ? null : parentMgr.RowType.GetProperty(refAttr.PropertyName);
                        RegisterRelationship(pocoMgr, parentMgr, fkProp, pkProp, refAttr.GroupName);
                    }
                }
            }
        }

        /// <summary>
        /// Scan parent to child references and establish relationships. See remarks for more info.
        /// </summary>
        /// <param name="pocoMgr">The new registered <see cref="RecordManager"/> being discovered.</param>
        /// <remarks>
        /// In case a parent type is registered after a child, their relationships were not established when the child
        /// was registered. To handle such cases, this method goes through each registered type and see if they could be
        /// children of the new type being registered.
        /// </remarks>
        private void DiscoverParentToChildRelationships(RecordManager pocoMgr)
        {
            foreach (var childMgr in _PocoManagers.Values)
            {
                //NOTE: If childMgr == pocoMgr, then that would be self-reference (same type), and that would have been
                //caught in DiscoverChildToParentRelationships.
                if (childMgr != pocoMgr)
                {
                    foreach (var refAttrsByProp in childMgr.ReferencesAttributesByProperty)
                    {
                        var refAttr = refAttrsByProp.Item2;
                        if (refAttr.ParentType == pocoMgr.RowType)
                        {
                            var fkProp = refAttrsByProp.Item1;
                            var pkProp = refAttr.PropertyName == null ? null : childMgr.RowType.GetProperty(refAttr.PropertyName);
                            RegisterRelationship(childMgr, pocoMgr, fkProp, pkProp, refAttr.GroupName);
                        }
                    }
                }
            }
        }

        private void RegisterRelationship(RecordManager childMgr, RecordManager parentMgr, PropertyInfo fkProp, PropertyInfo pkProp, string groupName)
        {
            //See if the relationship is already known. Here we could look either
            //in the parent or in the child information, and we pick the latter.
            var relInfo = childMgr.RelationshipsWhereIsChild.SingleOrDefault(x => x.ParentManager == parentMgr);
            if (relInfo == null)
            {
                //First time the relationship is established.
                //Note how the same refinfo is used for parent and child.
                Debug.Assert(!parentMgr.RelationshipsWhereIsParent.Any(x => x.ChildManager == childMgr && x.GroupName == groupName));
                relInfo = new Relationship(childMgr, parentMgr, groupName);
                relInfo.AddFKProperty(fkProp, pkProp);
                parentMgr.RelationshipsWhereIsParent.Add(relInfo);
                childMgr.RelationshipsWhereIsChild.Add(relInfo);
            }
            else
            {
                Debug.Assert(relInfo.ChildManager == childMgr);
                Debug.Assert(relInfo == parentMgr.RelationshipsWhereIsParent.Single(x => x.ChildManager == childMgr && x.GroupName == groupName));
                relInfo.AddFKProperty(fkProp, pkProp);
            }
        }

        #endregion

        #region Referential Integrity Management

        /// <summary>
        /// Enumerates through the immediate parent records of the given child record.
        /// </summary>
        public IEnumerable<IRecord> GetParentsOf(IRecord mpChild)
        {
            Debug.Assert(mpChild.HasValue);
            return mpChild.RecordManager.FetchParents(mpChild.Value, ManagedSet);
        }

        /// <summary>
        /// Enumerates through the immediate child records of the given parent record.
        /// </summary>
        public IEnumerable<IRecord> GetChildrenOf(IRecord mpParent)
        {
            Debug.Assert(mpParent.HasValue);
            var parentMgr = mpParent.RecordManager;
            object[] pk = parentMgr.RowManager.GetPrimaryKey(mpParent.Value);
            return parentMgr.FetchChildren(pk, ManagedSet);
        }

        /// <summary>
        /// Cascades down a deletion from a parent record to all referencing child records.
        /// </summary>
        public void CascadeDelete(IRecord mpParent, bool deleteParent = true)
        {
            mpParent.RecordManager.CascadeDelete(mpParent.Value, ManagedSet);
            if (deleteParent)
            {
                mpParent.SetDeleted();
            }
        }

        /// <summary>
        /// Ensures referential integrity between child and parent records within the data model. Relies on
        /// information provided via <see cref="ReferencesAttribute"/> attributes in the managed classes of
        /// the data model.
        /// </summary>
        public void ThrowIfNoReferentialIntegrity()
        {
            foreach (var pocoMgr in _PocoManagers.Values)
                pocoMgr.ThrowIfNoReferentialIntegrity(ManagedSet);
        }

        #endregion

        #region Save

        private void Save(Database db)
        {
            var ds_back = SerializeContent();
            db.BeginTransaction();
            try
            {
                var allrecords = ManagedSet.ToArray();
                foreach (var mp in allrecords)
                    if (mp.Status == RowStatus.Deleted)
                        Save_Delete(db, mp);
                foreach (var mp in allrecords)
                    if (mp.Status == RowStatus.Modified)
                        mp.Save(db);
                foreach (var mp in allrecords)
                    if (mp.Status == RowStatus.New)
                        Save_Insert(db, mp);
                db.CommitTransaction();
            }
            catch
            {
                db.RollbackTransaction();
                Clear();
                DeserializeContent(ds_back);
                throw;
            }
        }

        /// <summary>
        /// When inserting a record in the database, ensures that parent records are inserted first,
        /// and that parents' PK changes are cascaded down to children.
        /// </summary>
        private void Save_Insert(Database db, IRecord mp)
        {
            foreach (var parent in GetParentsOf(mp))
            {
                Debug.Assert(parent.HasValue && parent.IsCNM);
                if (parent.Status == RowStatus.New)
                    Save_Insert(db, parent);
            }
            Debug.Assert(mp.HasValue && mp.Status == RowStatus.New);
            object[] oldPK = mp.RecordManager.RowManager.GetPrimaryKey(mp.Value);
            mp.Save(db);
            object[] newPK = mp.RecordManager.RowManager.GetPrimaryKey(mp.Value);
            if (!newPK.SequenceEqual(oldPK))
                mp.RecordManager.CascadePrimaryKeyUpdate(oldPK, newPK, ManagedSet.ToArray());
        }

        /// <summary>
        /// Wen deleting a record from the database, ensures that children are deleted first.
        /// </summary>
        private void Save_Delete(Database db, IRecord mp)
        {
            foreach (var child in GetChildrenOf(mp))
            {
                Debug.Assert(child.HasValue && child.Status == RowStatus.Deleted);
                Save_Delete(db, child);
            }
            Debug.Assert(mp.HasValue && mp.Status == RowStatus.Deleted);
            mp.Save(db);
        }

        #endregion

        #region Serialization

        public byte[] SerializeContent()
        {
            using (var s = new MemoryStream())
            using (var bw = new BinaryWriterEx(s))
            {
                foreach (var source in _ManagedPocoSources)
                {
                    bw.Write(true); //marks beginning of next record source
                    var serializer = source.RecordManager.RowManager.TableInfo.Serializer;
                    foreach (var record in source)
                    {
                        bw.Write(true); //marks beginning of next record
                        serializer.Invoke(bw, record.Value);
                        bw.Write((int)record.Status);
                    }
                    bw.Write(false); //marks end of record source
                }
                bw.Write(false); //marks end of stream
                return s.ToArray();
            }
        }

        public void DeserializeContent(byte[] content)
        {
            using (var s = new MemoryStream(content))
            using (var br = new BinaryReaderEx(s))
            {
                try
                {
                    int index = 0;
                    while (br.ReadBoolean())
                    {
                        if (index == _ManagedPocoSources.Count)
                            throw new InvalidOperationException("Number of record containers does not match.");
                        var source = _ManagedPocoSources[index++];
                        if (!source.IsClear())
                            throw new InvalidOperationException("Record container already has data.");
                        var deserializer = source.RecordManager.RowManager.TableInfo.Deserializer;
                        while (br.ReadBoolean())
                        {
                            object value = deserializer(br);
                            var status = (RowStatus)br.ReadInt32();
                            source.RestoreContent(value, status);
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new InvalidOperationException("Unable to restore content because of invalid of corrupted data.", ex);
                }
            }
        }

        #endregion
    }
}