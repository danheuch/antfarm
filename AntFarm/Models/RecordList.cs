﻿using AntFarm.Core;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;

namespace AntFarm.Models
{
    /// <summary>
    /// Manages a list of <see cref="Record{TRow}"/>, which are records in a <see cref="ModelBase"/>.
    /// </summary>
    /// <typeparam name="TRow">The type of row (database entity) being managed.</typeparam>
    public class RecordList<TRow> : IRecordSource, IEnumerable<Record<TRow>>
    {
        #region IRecordSource
        ModelBase IRecordSource.Owner => this.Owner;
        RecordManager IRecordSource.RecordManager => this.RecordManager;
        void IRecordSource.Clear() => _List.Clear();
        bool IRecordSource.IsClear() => _List.Count == 0;
        void IRecordSource.RestoreContent(object value, RowStatus status)
        {
            var mp = new Record<TRow>(this, (TRow)value, status);
            _List.Add(mp);
        }
        IEnumerator<IRecord> IRecordSource.GetEnumerator() => GetEnumerator();
        #endregion

        #region IEnumerable
        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
        IEnumerator<Record<TRow>> IEnumerable<Record<TRow>>.GetEnumerator() => GetEnumerator();
        #endregion

        private readonly List<Record<TRow>> _List = new List<Record<TRow>>();

        //internal ManagedPocoList(ModelBase owner)
        //    : this(owner, null)
        //{ }

        internal RecordList(ModelBase owner, IEnumerable<TRow> items)
        {
            Owner = owner;
            RecordManager = owner.RegisterPocoType(typeof(TRow), this);
            if (items != null)
                PopulateWith(items);
        }

        public ModelBase Owner { get; }

        public RecordManager RecordManager { get; }

        public int Count => _List.Count;

        public IEnumerator<Record<TRow>> GetEnumerator() => ((IEnumerable<Record<TRow>>)_List).GetEnumerator();

        /// <summary>
        /// The list of rows that are either Current, New or Modified.
        /// Excludes Deleted rows.
        /// </summary>
        public IEnumerable<TRow> CNM => _List.Where(x => x.IsCNM).Select(x => x.Value);

        public void PopulateWith(TRow item)
        {
            PopulateWith(new[] { item } as IEnumerable<TRow>);
        }

        public void PopulateWith(IEnumerable<TRow> items)
        {
            _List.Clear();
            if (items != null)
                _List.AddRange(items.Select(x => new Record<TRow>(this, x, RowStatus.Current)));
        }

        /// <summary>
        /// Merges a single dto into the managed poco record list.
        /// </summary>
        /// <typeparam name="TDTO">The merged DTO's type.</typeparam>
        /// <param name="dto">The merged DTO.</param>
        /// <param name="options">Options that control how the data is merged.</param>
        public void MergeWith<TDTO>(TDTO dto, MergeOptions options = null)
        {
            MergeWith(new[] { dto } as IEnumerable<TDTO>, options);
        }

        /// <summary>
        /// Merges a single dto into the managed poco record list.
        /// </summary>
        /// <typeparam name="TDTO">The merged DTO's type.</typeparam>
        /// <param name="dto">The merged DTO.</param>
        /// <param name="options">Options that control how the data is merged.</param>
        public void MergeWith<TDTO, TMatchKey>(
            TDTO dto,
            Func<TDTO, TMatchKey> getDTOKeyDelegate,
            Func<TRow, TMatchKey> getPocoKeyDelegate,
            Func<TDTO, TRow> createDelegate,
            Action<TRow, TDTO> updateDelegate,
            Action<TRow> deleteDelegate = null,
            MergeOptions options = null)
        {
            MergeWith(new[] { dto } as IEnumerable<TDTO>, getDTOKeyDelegate, getPocoKeyDelegate, createDelegate, updateDelegate, deleteDelegate, options);
        }

        /// <summary>
        /// Merges a list of DTO's data into the managed poco record list.
        /// </summary>
        /// <typeparam name="TDTO">The merged DTO's type.</typeparam>
        /// <param name="dtos">The merged DTO list.</param>
        /// <param name="options">Options that control how the data is merged.</param>
        public void MergeWith<TDTO>(IEnumerable<TDTO> dtos, MergeOptions options = null)
        {
            options = options ?? new MergeOptions();

            //TODO: These need to be cached.
            DynamicMethod createMethod = null;
            DynamicMethod updateMethod = null;
            DynamicMethod getDTOKeyMethod = null;
            DynamicMethod getPocoKeyMethod = null;

            if (options.AllowInsert)
                createMethod = DynamicMethodGenerator.CreateFactoryAndMapper(typeof(TRow), typeof(TDTO));
            if (options.AllowUpdate)
                updateMethod = DynamicMethodGenerator.CreateMapper(typeof(TRow), typeof(TDTO));
            if (options.AllowInsert || options.AllowUpdate || options.AllowDelete)
            {
                var pkAccessors = RecordManager.RowManager.TableInfo.PrimaryKeyColumns.Select(x => x.Accessor).ToArray();
                getPocoKeyMethod = DynamicMethodGenerator.CreateMultiPropertyGetter(pkAccessors);

                var dtoAccessors = new List<PropertyInfo>(pkAccessors.Length);
                foreach (var pkAccessor in pkAccessors)
                {
                    //NOTE: dtoAccessor can be null. See CreateMultiPropertyGetter for more info.
                    var dtoAccessor = (typeof(TDTO)).GetProperty(pkAccessor.Name);
                    dtoAccessors.Add(dtoAccessor);
                }
                getDTOKeyMethod = DynamicMethodGenerator.CreateMultiPropertyGetter(dtoAccessors.ToArray());
            }

            var createDelegate = createMethod?.CreateDelegate(typeof(Func<TDTO, TRow>)) as Func<TDTO, TRow>;
            var updateDelegate = updateMethod?.CreateDelegate(typeof(Action<TRow, TDTO>)) as Action<TRow, TDTO>;
            var getDTOKeyDelegate = getDTOKeyMethod?.CreateDelegate(typeof(Func<TDTO, object[]>)) as Func<TDTO, object[]>;
            var getPocoKeyDelegate = getPocoKeyMethod?.CreateDelegate(typeof(Func<TRow, object[]>)) as Func<TRow, object[]>;

            MergeWith(dtos, getDTOKeyDelegate, getPocoKeyDelegate, createDelegate, updateDelegate, null, options);
        }

        /// <summary>
        /// Merges a list of DTO's data into the managed poco record list.
        /// </summary>
        /// <typeparam name="TDTO">The merged DTO's type.</typeparam>
        /// <typeparam name="TMatchKey">The type used to matched DTO and poco (typically, the type for the PK or some other unique key identifier).</typeparam>
        /// <param name="dtos">The merged DTO list.</param>
        /// <param name="getDTOKeyDelegate">A function returning the matching key in the DTO.</param>
        /// <param name="getPocoKeyDelegate">A function returning the matching key in the poco.</param>
        /// <param name="createDelegate">A function invoked when creating a new poco. The function needs only returning an instance, but it is passed
        /// the DTO, and can also populate fields such as a parent foreign key or other fixed properties.</param>
        /// <param name="updateDelegate">An action invoked when updating an existing poco. The action is called even in the case of a new record (immediately
        /// after the createFunction delegate is called).</param>
        /// <param name="deleteDelegate">An action invoked when a poco is deleted.</param>
        /// <param name="options">Options that control how the data is merged.</param>
        public void MergeWith<TDTO, TMatchKey>(
            IEnumerable<TDTO> dtos,
            Func<TDTO, TMatchKey> getDTOKeyDelegate,
            Func<TRow, TMatchKey> getPocoKeyDelegate,
            Func<TDTO, TRow> createDelegate,
            Action<TRow, TDTO> updateDelegate,
            Action<TRow> deleteDelegate = null,
            MergeOptions options = null)
        {
            //Get a full outer join between the 2 given lists (DTOs and managed pocos).
            var right = _List.Where(x => x.HasValue && x.IsCNM);
            var fullJoin = FullJoin(dtos ?? throw new ArgumentNullException(nameof(dtos)), right, x => getDTOKeyDelegate(x), y => getPocoKeyDelegate(y.Value)).ToArray();

            //From the combined lists, determine what is created, modified or deleted.
            options = options ?? new MergeOptions();
            foreach (var match in fullJoin)
            {
                var dto = match.Item1;
                var mp = match.Item2;
                try
                {
                    if (dto == null)
                    {
                        //Passing a null DTO... 
                        if (mp == null)
                        {
                            //There is no record and therefore no change.
                        }
                        else
                        {
                            //Mark the exiting record for deletion.
                            Debug.Assert(mp.HasValue && mp.IsCNM);
                            if (!options.AllowDelete)
                                continue;
                            deleteDelegate?.Invoke(mp.Value);
                            Owner.CascadeDelete(mp, true);
                            if (!mp.HasValue)
                            {
                                //Case of new record that is now deleted:
                                Debug.Assert(mp.Status == RowStatus.Current);
                                _List.Remove(mp);
                            }
                        }
                    }
                    else
                    {
                        //Passing a non-null DTO...
                        if (mp == null)
                        {
                            //Install a new record.
                            if (!options.AllowInsert)
                                continue;
                            if (createDelegate == null)
                                throw new ArgumentNullException(nameof(createDelegate));
                            mp = new Record<TRow>(this, default(TRow), RowStatus.Current);
                            mp.MergeWith(dto, createDelegate, updateDelegate, null, options);
                            _List.Add(mp);
                        }
                        else
                        {
                            //Modify the existing record.
                            Debug.Assert(mp.HasValue && mp.IsCNM);
                            if (!options.AllowUpdate)
                                continue;
                            mp.MergeWith(dto, null, updateDelegate, null, options);
                        }
                    }
                }
                catch (DeleteOrModifyOverDeleteException)
                {
                    if (options.StrictMergingRules && Owner.EnforcesStrictMergingRules)
                        throw;
                }
                catch
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Does a full outer join between two lists, returning a list of tuples containing the
        /// left lift items and right lists items matched by a common key, or null when a match
        /// is not found.
        /// </summary>
        /// <typeparam name="T1">The generic type of item in the left list.</typeparam>
        /// <typeparam name="T2">The generic type of item in the right list. </typeparam>
        /// <typeparam name="TKey">The generic type of the key used to join the lists.</typeparam>
        /// <param name="t1">The left list.</param>
        /// <param name="t2">The right list.</param>
        /// <param name="getLeftKey">The delegate to get the key in the left list item.</param>
        /// <param name="getRightKey">The delegate to get the key in the right list item.</param>
        /// <returns>A list of tuples with the left list items matched to the right list items.</returns>
        private static List<(T1, T2)> FullJoin<T1, T2, TKey>(
            IEnumerable<T1> t1,
            IEnumerable<T2> t2,
            Func<T1, TKey> getLeftKey,
            Func<T2, TKey> getRightKey)
        {
            if (typeof(TKey).IsArray)
            {
                //Big problem here because left and right keys are arrays, which are not directly equatable.
                //TODO: What we really need is to treat PKs and FKs as IComparable and/or IEquatable objects.
                var fullJoin = new List<(T1, T2)>();
                var rightJoin = t2.ToList();
                foreach (var item1 in t1)
                {
                    var leftKey = ((IEnumerable<object>)getLeftKey(item1)).ToArray();
                    bool matched = false;
                    foreach (var item2 in t2)
                    {
                        var rightKey = ((IEnumerable<object>)getRightKey(item2)).ToArray();
                        if (leftKey.SequenceEqual(rightKey))
                        {
                            fullJoin.Add((item1, item2));
                            rightJoin.Remove(item2);
                            matched = true;
                            break;
                        }
                    }
                    if (!matched)
                        fullJoin.Add((item1, default(T2)));
                }
                fullJoin.AddRange(rightJoin.Select(x => (default(T1), x)));
                return fullJoin;
            }
            else
            {
                //See https://docs.microsoft.com/en-us/dotnet/csharp/language-reference/keywords/join-clause
                //DTO left join DataSet
                var leftJoin =
                    from a in t1
                    join b in t2 on getLeftKey(a) equals getRightKey(b) into temp
                    from c in temp.DefaultIfEmpty()
                    select (a, c);
                //DTO right join DataSet
                var rightJoin =
                    from a in t2
                    join b in t1 on getRightKey(a) equals getLeftKey(b) into temp
                    from c in temp.DefaultIfEmpty()
                    select (c, a);
                //DTO full join DataSet
                return leftJoin.Union(rightJoin).ToList();
            }
        }
    }
}