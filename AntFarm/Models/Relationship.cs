﻿using AntFarm.Core;
using System;
using System.Diagnostics;
using System.Linq;
using System.Reflection;

namespace AntFarm.Models
{
    public class Relationship
    {
        private readonly PropertyInfo[] _PKProperties;
        private readonly PropertyInfo[] _FKProperties;
        private Func<object, object[]> _FKGetter;
        public Relationship(RecordManager childMgr, RecordManager parentMgr, string groupName)
        {
            parentMgr.RowManager.TableInfo.ThrowIfNoPrimaryKey();
            ChildManager = childMgr;
            ParentManager = parentMgr;
            _PKProperties = ParentManager.RowManager.TableInfo.PrimaryKeyColumns.Select(x => x.Accessor).ToArray();
            _FKProperties = new PropertyInfo[_PKProperties.Length];
            GroupName = groupName;
        }
        public RecordManager ChildManager { get; }
        public RecordManager ParentManager { get; }
        public string GroupName { get; }
        /// <summary>
        /// The properties making up the foreign key in the child type.
        /// The system has ensured that they are sorted in the same order as the properties making up the primary key in the parent type,
        /// and that the types match.
        /// </summary>
        public void AddFKProperty(PropertyInfo fkProp, PropertyInfo pkProp)
        {
            if (_FKGetter != null)
                throw new InvalidOperationException("The foreign key definition can no longer be modified.");
            var fkProps = _FKProperties;
            int index = 0;
            if (pkProp == null)
            {
                //We assume the foreign key properies are declared in the same order as the primary key properties.
                for (index = 0; index < fkProps.Length; index++)
                    if (fkProps[index] == null)
                        break;
            }
            else
            {
                //We can try to match using property names.
                for (index = 0; index < fkProps.Length; index++)
                    if (_PKProperties[index].Name.Equals(pkProp.Name, StringComparison.OrdinalIgnoreCase))
                        break;
            }
            if (index == fkProps.Length)
                throw new InvalidOperationException("Invalid foreign key reference: count or order does not match primary key"); //TODO: better message
            if (fkProps[index] != null)
                throw new InvalidOperationException("Invalid foreign key reference: count or order does not match primary key"); //TODO: better message
            if (_PKProperties[index].PropertyType != fkProp.PropertyType)
                throw new InvalidOperationException("Invalid foreign key reference: types do not match"); //TODO: better message
            fkProps[index] = fkProp;
        }
        public object[] GetPrimaryKey(object parentPoco)
        {
            Debug.Assert(parentPoco.GetType() == ParentManager.RowType);
            return ParentManager.RowManager.GetPrimaryKey(parentPoco);
        }
        public void SetPrimaryKey(object parentPoco, object[] pk)
        {
            Debug.Assert(parentPoco.GetType() == ParentManager.RowType);
            ParentManager.RowManager.SetPrimaryKey(parentPoco, pk);
        }
        public object[] GetForeignKey(object childPoco)
        {
            Debug.Assert(childPoco.GetType() == ChildManager.RowType);
            if (_FKGetter == null)
            {
                var method = DynamicMethodGenerator.CreateMultiPropertyGetter(_FKProperties);
                _FKGetter = method.CreateDelegate(typeof(Func<object, object[]>)) as Func<object, object[]>;
            }
            return _FKGetter.Invoke(childPoco);
        }
        public void SetForeignKey(object childPoco, object[] fk)
        {
            Debug.Assert(childPoco.GetType() == ChildManager.RowType);
            var fkProps = _FKProperties;
            if (fkProps.Length != fk.Length)
                throw new IndexOutOfRangeException();
            for (int i = 0; i < fkProps.Length; i++)
                Utils.SetPropertyValue(fkProps[i], childPoco, fk[i]);
        }
    }
}