﻿using AntFarm.ORM;
using System;
using System.Diagnostics;
using System.Linq;

namespace AntFarm.Models
{
    /// <summary>
    /// Manages a single record in a <see cref="ModelBase"/>. Controls the underlying object (representing a
    /// database row) value and status relative to the backend (current, new, modified or deleted).
    /// </summary>
    /// <typeparam name="TRow">The type of row (database entity) being managed.</typeparam>
    public class Record<TRow> : IRecord
    {
        #region IRecord
        IRecordSource IRecord.Container => this.Container;
        RecordManager IRecord.RecordManager => this.RecordManager;
        bool IRecord.HasValue => this.HasValue;
        object IRecord.Value => this.Value;
        RowStatus IRecord.Status => this.Status;
        bool IRecord.IsCNM => this.IsCNM;
        void IRecord.SetModified() => this.SetModified();
        void IRecord.SetDeleted() => this.SetDeleted();
        void IRecord.Save(Database db) => this.Save(db);
        #endregion

        /// <summary>
        /// Initializes a new <see cref="Record{TRow}"/>.
        /// This constructor is internal.
        /// </summary>
        /// <param name="container">The record's container.</param>
        /// <param name="value">The initial value.</param>
        /// <param name="status">The initial status</param>
        internal Record(IRecordSource container, TRow value, RowStatus status)
        {
            Container = container;
            PopulateWith(value, status);
            container.Owner.InvalidateCache();
        }

        /// <summary>
        /// The <see cref="IRecordSource"/> listing this instance.
        /// </summary>
        public IRecordSource Container { get; }

        /// <summary>
        /// The <see cref="ModelBase"/> to which this instance belong.
        /// </summary>
        public ModelBase Owner { get { return Container.Owner; } }

        /// <summary>
        /// The <see cref="RecordManager"/> controlling essential aspects of how the value is managed.
        /// </summary>
        public RecordManager RecordManager { get { return Container.RecordManager; } }

        /// <summary>
        /// Whether there is a value (true) or not (false).
        /// </summary>
        public bool HasValue { get { return Value != null; } }

        /// <summary>
        /// The actual value of the record.
        /// </summary>
        public TRow Value { get; private set; }

        /// <summary>
        /// The record status with regard to the backend.
        /// </summary>
        public RowStatus Status { get; private set; }

        /// <summary>
        /// Gets whether the record is Current, New or Modified.
        /// False if the record is marked for deletion.
        /// </summary>
        public bool IsCNM
        {
            get
            {
                switch (Status)
                {
                    case RowStatus.Current:
                        return true;
                    case RowStatus.New:
                    case RowStatus.Modified:
                        Debug.Assert(HasValue);
                        return true;
                    default:
                        Debug.Assert(Status == RowStatus.Deleted);
                        return false;
                }
            }
        }

        /// <summary>
        /// Initializes the record value and status.
        /// </summary>
        /// <param name="value">The record value, or null to clear an existing value.</param>
        /// <param name="status">The record status.</param>
        internal void PopulateWith(TRow value, RowStatus status = RowStatus.Current)
        {
            Value = value;
            Status = status;
        }

        /// <summary>
        /// Merges data into the record.
        /// </summary>
        /// <typeparam name="TDTO">The type of merged data.</typeparam>
        /// <param name="dto">The merged data.</param>
        /// <param name="createDelegate">A function invoked when creating a new row value. The function needs only returning an instance, but it is passed
        /// the DTO, and can also populate fields such as a parent foreign key or other fixed properties.</param>
        /// <param name="updateDelegate">An action invoked when updating an existing row value. The action is called even in the case of a new record (immediately
        /// after the createFunction delegate is called).</param>
        /// <param name="deleteDelegate">An action invoked when a row value is deleted.</param>
        /// <param name="options">Options controlling how the data is merged.</param>
        internal void MergeWith<TDTO>(TDTO dto,
            Func<TDTO, TRow> createDelegate,
            Action<TRow, TDTO> updateDelegate,
            Action<TRow> deleteDelegate = null,
            MergeOptions options = null)
        {
            ThrowIfInvalidInternalState();
            options = options ?? new MergeOptions();
            try
            {
                TRow value = Value;
                if (dto == null)
                {
                    //Passing a null DTO... 
                    if (value == null)
                    {
                        //There is no record and therefore no change.
                    }
                    else
                    {
                        //Mark the exiting record for deletion.
                        if (Status == RowStatus.Deleted)
                            throw new DeleteOrModifyOverDeleteException(typeof(TRow));
                        deleteDelegate?.Invoke(value);
                        Owner.CascadeDelete(this, true);
                    }
                }
                else
                {
                    //Passing a non-null DTO...
                    if (value == null)
                    {
                        //Install a new record.
                        Debug.Assert(Status == RowStatus.Current);
                        if (createDelegate == null)
                            throw new ArgumentNullException(nameof(createDelegate));
                        value = createDelegate.Invoke(dto);
                        if (value == null)
                            throw new InvalidOperationException($"No {typeof(TRow)} instantiated.");
                        RecordManager.RowManager.SetDefaultProperties(value);
                        updateDelegate?.Invoke(value, dto);
                        Value = value;
                        Status = RowStatus.New;
                    }
                    else
                    {
                        //Modify the existing record.
                        if (Status == RowStatus.Current)
                        {
                            //Compare before and after values to determine if the object has changed.
                            var oldBytes = RecordManager.RowManager.Row2Bytes(value);
                            updateDelegate?.Invoke(value, dto);
                            var newBytes = RecordManager.RowManager.Row2Bytes(value);
                            if (!oldBytes.SequenceEqual(newBytes))
                                Status = RowStatus.Modified;
                        }
                        else
                        {
                            updateDelegate?.Invoke(value, dto);
                        }
                    }
                }
            }
            catch (DeleteOrModifyOverDeleteException)
            {
                if (options.StrictMergingRules && Owner.EnforcesStrictMergingRules)
                    throw;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Flags the record as modified.
        /// </summary>
        private void SetModified()
        {
            ThrowIfInvalidInternalState();
            switch (Status)
            {
                case RowStatus.Current:
                    if (!HasValue)
                        throw new InvalidOperationException();
                    Status = RowStatus.Modified;
                    break;
                case RowStatus.New:
                case RowStatus.Modified:
                    Debug.Assert(HasValue);
                    break; //keep status as is
                case RowStatus.Deleted:
                    Debug.Assert(HasValue);
                    throw new InvalidOperationException();
                default:
                    throw new InvalidOperationException();
            }
        }

        /// <summary>
        /// Flags the record as deleted.
        /// Note that this operation does not cascade down to child records.
        /// See also <see cref="ModelBase"/> CascadeDelete method.
        /// </summary>
        private void SetDeleted()
        {
            ThrowIfInvalidInternalState();
            switch (Status)
            {
                case RowStatus.Current:
                    if (!HasValue)
                        throw new InvalidOperationException();
                    Status = RowStatus.Deleted;
                    break;
                case RowStatus.New:
                    Debug.Assert(HasValue);
                    Value = default(TRow);
                    Status = RowStatus.Current;
                    break;
                case RowStatus.Modified:
                    Debug.Assert(HasValue);
                    Status = RowStatus.Deleted;
                    break;
                case RowStatus.Deleted:
                    Debug.Assert(HasValue);
                    break; //nothing to do
                default:
                    throw new InvalidOperationException();
            }
        }

        /// <summary>
        /// Saves (inserts, deletes or updates) the record to the database.
        /// </summary>
        private void Save(Database db)
        {
            switch (Status)
            {
                case RowStatus.New:
                    Debug.Assert(HasValue);
                    db.Insert(Value);
                    break;
                case RowStatus.Modified:
                    Debug.Assert(HasValue);
                    db.Update(Value);
                    break;
                case RowStatus.Deleted:
                    Debug.Assert(HasValue);
                    db.Delete(Value);
                    Value = default(TRow);
                    break;
                default:
                    Debug.Assert(Status == RowStatus.Current);
                    break;
            }
            Status = RowStatus.Current;
        }

        /// <summary>
        /// Ensures the status is valid in relation to the record value (or lack thereof).
        /// Throws a <see cref="InvalidInternalStateException"/> if that is found not to be the case.
        /// </summary>
        private void ThrowIfInvalidInternalState()
        {
            switch (Status)
            {
                case RowStatus.New:
                case RowStatus.Modified:
                case RowStatus.Deleted:
                    if (!HasValue)
                        throw new InvalidInternalStateException(this);
                    break;
                default:
                    Debug.Assert(Status == RowStatus.Current);
                    break;
            }
        }
    }
}