﻿using System;

namespace AntFarm.Models
{
    /// <summary>
    /// Applied to a column property to declare a foreign key. The property has to be of type
    /// <see cref="int"/> or <see cref="int?"/>.
    /// </summary>
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false, Inherited = true)]
    public class ReferencesAttribute : Attribute
    {
        public ReferencesAttribute() { }
        public ReferencesAttribute(Type parentType) { ParentType = parentType; }
        public Type ParentType { get; }
        /// <summary>
        /// The name of the matching property in the parent type, which must also be part of the parent primary key.
        /// The value is optional. If null, the system will attempt to match the primary key based on the order of
        /// declaration
        /// </summary>
        public string PropertyName { get; set; }
        /// <summary>
        /// The name of the foreign key group, which can be used in case multiple foreign keys are used to reference the same
        /// parent type.
        /// The value is optional. If null, the system assumes only one foreign key is used to reference the parent type.
        /// </summary>
        public string GroupName { get; set; }
    }
}
