﻿using AntFarm.ORM;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;

namespace AntFarm.Models
{
    public class RecordManager
    {
        public RecordManager(Type rowType, IDatabaseProvider provider)
        {
            RowType = rowType;
            RowManager = RowManager.GetManager(rowType, provider);
            ReferencesAttributesByProperty = new List<(PropertyInfo, ReferencesAttribute)>();
            RelationshipsWhereIsParent = new List<Relationship>();
            RelationshipsWhereIsChild = new List<Relationship>();
        }

        public Type RowType { get; }

        public RowManager RowManager { get; }

        /// <summary>
        /// List of all the reference attributes linking the managed type as child to a parent type, by property.
        /// </summary>
        public List<(PropertyInfo, ReferencesAttribute)> ReferencesAttributesByProperty { get; }

        public List<Relationship> RelationshipsWhereIsParent { get; }

        public List<Relationship> RelationshipsWhereIsChild { get; }

        /// <summary>
        /// Fetches all the managed pocos referencing a given primary key. The returned records have a value
        /// but may be deleted.
        /// </summary>
        internal IEnumerable<IRecord> FetchChildren(object[] pk, IEnumerable<IRecord> mpSet)
        {
            Debug.Assert(pk != null);
            foreach (var relinfo in RelationshipsWhereIsParent)
            {
                var childMgr = relinfo.ChildManager;
                foreach (var mpChild in mpSet.Where(x => x.RecordManager == childMgr))
                    if (mpChild.HasValue && relinfo.GetForeignKey(mpChild.Value).SequenceEqual(pk))
                        yield return mpChild;
            }
        }

        /// <summary>
        /// Fetches all the managed pocos referenced by a given managed poco instance. The returned records have a value
        /// but may be deleted.
        /// </summary>
        internal IEnumerable<IRecord> FetchParents(object childPoco, IEnumerable<IRecord> mpSet)
        {
            Debug.Assert(childPoco != null);
            foreach (var relinfo in RelationshipsWhereIsChild)
            {
                object[] fk = relinfo.GetForeignKey(childPoco);
                var parentMgr = relinfo.ParentManager;
                foreach (var mparent in mpSet.Where(x => x.RecordManager == parentMgr))
                    if (mparent.HasValue && relinfo.GetPrimaryKey(mparent.Value).SequenceEqual(fk))
                        yield return mparent;
            }
        }

        /// <summary>
        /// Ensures referential integrity by cascading down a delete throughout the record set.
        /// </summary>
        internal void CascadeDelete(object parentPoco, IEnumerable<IRecord> mpSet)
        {
            object[] delPK = RowManager.GetPrimaryKey(parentPoco);
            foreach (var relInfo in RelationshipsWhereIsParent)
            {
                foreach (var mpChild in FetchChildren(delPK, mpSet))
                {
                    Debug.Assert(mpChild.HasValue);
                    if (mpChild.IsCNM)
                    {
                        mpChild.SetDeleted();
                        relInfo.ChildManager.CascadeDelete(mpChild.Value, mpSet);
                    }
                }
            }
        }

        /// <summary>
        /// Ensures referential integrity by cascading down a primary key update throughout the record set.
        /// See remarks for more info.
        /// </summary>
        /// <remarks>
        /// This operation is called when a parent primary key is some kind of sequence or auto-number and is
        /// saved. At that point, the system replaces the temporary value it assigned, and propagates it
        /// to foreign keys.
        /// </remarks>
        internal void CascadePrimaryKeyUpdate(object[] oldPK, object[] newPK, IEnumerable<IRecord> mpSet)
        {
            foreach (var relInfo in RelationshipsWhereIsParent)
            {
                foreach (var mpChild in FetchChildren(oldPK, mpSet))
                {
                    Debug.Assert(mpChild.HasValue);
                    if (mpChild.IsCNM)
                    {
                        object[] oldChildPK = mpChild.RecordManager.RowManager.GetPrimaryKey(mpChild.Value);
                        relInfo.SetForeignKey(mpChild.Value, newPK);
                        mpChild.SetModified();
                        object[] newChildPK = mpChild.RecordManager.RowManager.GetPrimaryKey(mpChild.Value);
                        if (!oldChildPK.SequenceEqual(newChildPK))
                            relInfo.ChildManager.CascadePrimaryKeyUpdate(oldChildPK, newChildPK, mpSet);
                    }
                }
            }
        }

        /// <summary>
        /// Checks for referential integrity between all the records and their parents in the list.
        /// </summary>
        internal void ThrowIfNoReferentialIntegrity(IEnumerable<IRecord> mpSet)
        {
            foreach (var relinfo in RelationshipsWhereIsChild)
            {
                var childMgr = relinfo.ChildManager;
                var parentMgr = relinfo.ParentManager;
                foreach (var mpChild in mpSet)
                {
                    if (mpChild.RecordManager == childMgr)
                    {
                        object[] fk = relinfo.GetForeignKey(mpChild.Value);
                        if (fk.Any(x => x != null))
                        {
                            if (!mpSet.Any(x => x.RecordManager == parentMgr && x.RecordManager.RowManager.GetPrimaryKey(x.Value).SequenceEqual(fk)))
                                throw new Exception("Referential integrity error");
                        }
                    }
                }
            }
        }
    }
}