﻿using AntFarm.ORM;
using System.Collections.Generic;

namespace AntFarm.Models
{
    /// <summary>
    /// Contract for a class that represents a record (tuple of row value and status) in a <see cref="ModelBase"/>.
    /// </summary>
    public interface IRecord
    {
        /// <summary>
        /// The record's <see cref="IRecordSource"/>.
        /// </summary>
        IRecordSource Container { get; }
        /// <summary>
        /// The record's <see cref="RecordManager"/>". />
        /// </summary>
        RecordManager RecordManager { get; }
        /// <summary>
        /// Whether the record has a value (true), or is empty (false).
        /// </summary>
        bool HasValue { get; }
        /// <summary>
        /// The actual record value, which can be null (indicating a placeholder), in which case the Status should also
        /// be Current.
        /// </summary>
        object Value { get; }
        /// <summary>
        /// The record's status (current, new, modified or deleted). A value of Current means that the record matches
        /// the database version. Any other value needs synching with the database.
        /// </summary>
        RowStatus Status { get; }
        /// <summary>
        /// Whether the record is Current, New or Modified.
        /// The value is False for records marked for deletion.
        /// </summary>
        bool IsCNM { get; }
        /// <summary>
        /// Marks the record as updatable in the backend.
        /// </summary>
        void SetModified();
        /// <summary>
        /// Marks the record as deletable in the backend.
        /// </summary>
        void SetDeleted();
        /// <summary>
        /// Save pending changes (if any) to the backend.
        /// </summary>
        void Save(Database db);
    }

    /// <summary>
    /// Contract for an class that contains <see cref="IRecord"/> objects in a <see cref="ModelBase"/>.
    /// </summary>
    public interface IRecordSource
    {
        ModelBase Owner { get; }
        RecordManager RecordManager { get; }
        void Clear();
        bool IsClear();
        void RestoreContent(object value, RowStatus status);
        IEnumerator<IRecord> GetEnumerator();
    }
}