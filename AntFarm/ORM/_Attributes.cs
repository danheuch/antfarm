﻿using System;
using System.Data;

namespace AntFarm.ORM
{
    /// <summary>
    /// Defines options for audit columns, which are special columns marking events in the life-cycle of a
    /// record (creator, date of last update, etc.).
    /// </summary>
    public enum AuditColumnOption
    {
        /// <summary>
        /// Not an audit field.
        /// </summary>
        None = 0,
        /// <summary>
        /// Audit field: creator.
        /// </summary>
        CreatedBy,
        /// <summary>
        /// Audit field: date of creation.
        /// </summary>
        CreatedOn,
        /// <summary>
        /// Audit field: last modifier.
        /// </summary>
        UpdatedBy,
        /// <summary>
        /// Audit field: date of last modification.
        /// </summary>
        UpdatedOn
    }

    /// <summary>
    /// Refresh options for after insert and/or update of a record.
    /// </summary>
    [Flags]
    public enum RefreshOptions
    {
        /// <summary>
        /// Not refresh after insert or update.
        /// </summary>
        None = 0,
        /// <summary>
        /// Re-read the row after an insert.
        /// </summary>
        AfterInsert = 1,
        /// <summary>
        /// Re-read the row after an update.
        /// </summary>
        AfterUpdate = 2,
        /// <summary>
        /// Re-read the row after an insert or update.
        /// </summary>
        AfterInsertOrUpdate = AfterInsert | AfterUpdate,
    }

    /// <summary>
    /// Marks a class as defining a SQL table.
    /// </summary>
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false, Inherited = false)]
    public class TableAttribute : Attribute
    {
        public TableAttribute()
        { }
        public TableAttribute(string tableName, string schemaName = null, string databaseName = null)
        {
            TableName = tableName;
            SchemaName = schemaName;
            DatabaseName = databaseName;
        }
        /// <summary>
        /// The table name, devoid of provider-specific delimiters, which should omit the schema name.
        /// If missing, the table name is derived from the class name.
        /// </summary>
        public string TableName { get; }
        /// <summary>
        /// The schema name, devoid of provider-specific delimiters.
        /// If missing, the schema name is the default provided by the database connection.
        /// </summary>
        public string SchemaName { get; }
        /// <summary>
        /// The database name, devoid of provider-specific delimiters, which is optional.
        /// If missing, the database name is the default provided by the database connection.
        /// </summary>
        public string DatabaseName { get; }
        /// <summary>
        /// Whether and how to handle the optional refreshing of records after insert or update.
        /// </summary>
        public RefreshOptions RefreshOptions { get; set; }
    }

    /// <summary>
    /// Marks whether the class properties must explicitely define columns, or if they do so implicitely.
    /// Note that unlike the Table attribute, this attribute is inherited by derived classes, so care
    /// must be taken to be consistent across definitions.
    /// </summary>
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false, Inherited = true)]
    public class ExplicitColumnsAttribute : Attribute
    {
        public ExplicitColumnsAttribute() { Value = true; }
        public ExplicitColumnsAttribute(bool value) { Value = value; }
        /// <summary>
        /// Whether the class properties explicitely define columns (true), of do so implicitely (false).
        /// </summary>
        public bool Value { get; }
    }

    /// <summary>
    /// Marks a property as defining a SQL column.
    /// </summary>
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
    public class ColumnAttribute : Attribute
    {
        /// <summary>
        /// Whether the column is set as read-only from the property attribute.
        /// </summary>
        private bool _IsSetAsReadOnly = false;
        /// <summary>
        /// Initializes the column attribute.
        /// Marks the property as an actual SQL column.
        /// </summary>
        public ColumnAttribute() { IsColumn = true; }
        /// <summary>
        /// Initializes the column attribute.
        /// This constructor is useful to skip a property from the table definition when in implicit mode
        /// (see also <see cref="ExplicitColumnsAttribute"/>).
        /// </summary>
        /// <param name="isColumn">Whether the column is an actual SQL column. Set it to false to ignore it.</param>
        public ColumnAttribute(bool isColumn) { IsColumn = isColumn; }
        /// <summary>
        /// Initializes the column attribute.
        /// This cosntructor is useful when the property name is different than the SQL column name.
        /// </summary>
        /// <param name="columnName">The name of the SQL column.</param>
        public ColumnAttribute(string columnName) { IsColumn = true; ColumnName = columnName; }
        /// <summary>
        /// Whether the property defines a SQL column. If false, the property is ignored in the definition
        /// of the SQL table.
        /// </summary>
        public bool IsColumn { get; }
        /// <summary>
        /// The name of the column in the backend table. A null value (the default)
        /// uses the property name.
        /// </summary>
        public string ColumnName { get; }
        /// <summary>
        /// Whether the column is part of the primary key. The default is false.
        /// </summary>
        public bool IsPrimaryKey { get; set; }
        /// <summary>
        /// Whether the column value is generated automatically when a new record is inserted
        /// into a table.
        /// </summary>
        public bool IsAutoNumber { get; set; }
        /// <summary>
        /// Whether the column is read-only. The default is true for auto-number columns, or false
        /// otherwise.
        /// </summary>
        public bool IsReadOnly
        {
            get => _IsSetAsReadOnly || IsAutoNumber;
            set => _IsSetAsReadOnly = value;
        }
        /// <summary>
        /// The type of audit column. The default is AuditColumnType.None.
        /// </summary>
        public AuditColumnOption AuditOption { get; set; }
        /// <summary>
        /// The name of the Sequence that provides the value upon insert.
        /// </summary>
        public string SequenceName { get; set; }
    }

    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
    public class MapperAttribute : Attribute
    {
        public MapperAttribute(DbType dbType)
        {
            DBType = dbType;
        }
        public DbType DBType { get; }
        public int Size { get; set; }
        public byte Precision { get; set; }
        public byte Scale { get; set; }
        public bool IsNullable { get; set; } = true;
    }

    /// <summary>
    /// Marks a method of a data reader that extracts the field data directly, without conversion.
    /// </summary>
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = false)]
    public class GetAttribute : Attribute  { }
    /// <summary>
    /// Marks a method of a data reader that extracts the field data indirectly, necessitating some type of conversion.
    /// </summary>
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = false)]
    public class ConvertAttribute : Attribute { }
}