﻿namespace System
{
    public static class TypeExtensions
    {
        public static bool IsAssignableFromNull(this Type t) => !(t.IsValueType && Nullable.GetUnderlyingType(t) == null);
    }
    public static class TimeZonInfoExtensions
    {
        /// <summary>
        /// Expresses the current date/time offset in this time zone (or the local time zone if null).
        /// </summary>
        public static DateTimeOffset Now(this TimeZoneInfo tzi)
        {
            return tzi == null || tzi == TimeZoneInfo.Local ? DateTimeOffset.Now : ConvertDateTimeOffset(tzi, DateTimeOffset.Now);
        }

        /// <summary>
        /// Converts a date/time offset from any time zone representation to this time zone (or the local time zone if null).
        /// </summary>
        public static DateTimeOffset ConvertDateTimeOffset(this TimeZoneInfo tzi, DateTimeOffset value)
        {
            if (tzi == null)
                tzi = TimeZoneInfo.Local;
            if (value.Offset == tzi.BaseUtcOffset)
                return value;
            else
            {
                var utcValue = DateTime.SpecifyKind(value.UtcDateTime, DateTimeKind.Unspecified);
                return new DateTimeOffset(utcValue.Add(tzi.BaseUtcOffset), tzi.BaseUtcOffset);
            }
        }
    }
}

namespace System.Data
{
    using Microsoft.Extensions.Logging;

    internal static class IDbCommandExtensions
    {
        public static int LogAndExecuteNonQuery(this IDbCommand cd, ILogger logger)
        {
            if (logger?.IsEnabled(LogLevel.Debug) == true)
            {
                using (logger.BeginScope("Executing non-query statement..."))
                {
                    try
                    {
                        logger.LogDebug(cd.CommandText);
                        return cd.ExecuteNonQuery();
                    }
                    catch (Exception ex)
                    {
                        logger.LogError(ex.Message);
                        throw;
                    }
                }
            }
            else
            {
                return cd.ExecuteNonQuery();
            }
        }

        public static object LogAndExecuteScalar(this IDbCommand cd, ILogger logger)
        {
            if (logger?.IsEnabled(LogLevel.Debug) == true)
            {
                using (logger.BeginScope("Executing scalar statement..."))
                {
                    try
                    {
                        logger.LogDebug(cd.CommandText);
                        return cd.ExecuteScalar();
                    }
                    catch (Exception ex)
                    {
                        logger.LogError(ex.Message);
                        throw;
                    }
                }
            }
            else
            {
                return cd.ExecuteScalar();
            }
        }

        public static IDataReader LogAndExecuteReader(this IDbCommand cd, ILogger logger)
        {
            if (logger?.IsEnabled(LogLevel.Debug) == true)
            {
                using (logger.BeginScope("Executing reader statement..."))
                {
                    try
                    {
                        logger.LogDebug(cd.CommandText);
                        return cd.ExecuteReader();
                    }
                    catch (Exception ex)
                    {
                        logger.LogError(ex.Message);
                        throw;
                    }
                }
            }
            else
            {
                return cd.ExecuteReader();
            }
        }
    }
}

