﻿using System.Threading;

namespace AntFarm.ORM
{
    public class Sequence
    {
        private int _LastValue;
        public Sequence() { _LastValue = 0; }
        public int GetNextValue() => Interlocked.Increment(ref _LastValue);
    }
}