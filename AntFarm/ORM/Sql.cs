﻿using AntFarm.ORM.Providers;
using AntFarm.ORM.SQL;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace AntFarm.ORM
{
    public class Sql
    {
        private readonly SqlParser _Parser = new SqlParser();
        private readonly List<string> _TextFragments = new List<string>();
        private readonly List<object> _ParameterValues = new List<object>();
        private readonly List<(char, int)> _Parts = new List<(char, int)>();
        private char _ParameterPrefix = '?';
        private char _IdentifierBegin = '"';
        private char _IdentifierEnd = '"';
        public static Sql Builder { get { return new Sql(); } }
        public Sql WithParameterPrefix(char prefix) { _ParameterPrefix = prefix; return this; }
        string __MultiLineCommentBuffer;
        public Sql Append(string sqltext, params object[] values)
        {
            if (sqltext != null)
            {
                sqltext = sqltext.TrimEnd();
                if (sqltext.Length > 0)
                {
                    sqltext = __MultiLineCommentBuffer == null ? sqltext : (__MultiLineCommentBuffer + Environment.NewLine + sqltext);
                    var parts = _Parser.Parse(sqltext, _ParameterPrefix);
                    if (parts[parts.Count - 1].Item1 == 'U')
                    {
                        __MultiLineCommentBuffer = sqltext;
                        return this;
                    }
                    __MultiLineCommentBuffer = null;
                    if (_Parts.Count > 0)
                    {
                        //N => New line
                        _Parts.Add(('N', -1));
                    }
                    foreach (var part in parts)
                    {
                        if (part.Item1 == 'T')
                        {
                            //T => Text
                            _Parts.Add(('T', _TextFragments.Count));
                            string text = (string)part.Item2;
                            _TextFragments.Add(text);
                        }
                        else if (part.Item1 == 'I')
                        {
                            //I => Identifier
                            _Parts.Add(('I', _TextFragments.Count));
                            string text = (string)part.Item2;
                            _TextFragments.Add(text);
                        }
                        else if (part.Item1 == 'P')
                        {
                            //P => Parameter
                            _Parts.Add(('P', _ParameterValues.Count));
                            int pindex = (int)part.Item2;
                            if (pindex >= values.Length)
                                throw new InvalidOperationException("The number of parameters is greater than the number of values passed.");
                            object pvalue = values[pindex];
                            _ParameterValues.Add(pvalue);
                        }
                        else if (part.Item1 == 'L')
                        {
                            //L => Line comment
                            _Parts.Add(('L', _TextFragments.Count));
                            string text = (string)part.Item2;
                            _TextFragments.Add(text);
                        }
                        else if (part.Item1 == 'C')
                        {
                            //C => Comment
                            _Parts.Add(('C', _TextFragments.Count));
                            string text = (string)part.Item2;
                            _TextFragments.Add(text);
                        }
                        else
                        {
                            throw new InvalidOperationException();
                        }
                    }
                }
            }
            return this;
        }
        internal string GetCommandText(DatabaseProvider dbProvider)
        {
            (char idBegin, char idEnd) = (dbProvider?.SQLIdentifierDelimiterBegin ?? _IdentifierBegin, dbProvider?.SQLIdentifierDelimiterEnd ?? _IdentifierEnd);
            char pPrefix = dbProvider?.SQLParameterPrefix ?? _ParameterPrefix;
            var sb = new StringBuilder();
            foreach (var part in _Parts)
            {
                if (part.Item1 == 'T')
                {
                    sb.Append(_TextFragments[part.Item2]);
                }
                else if (part.Item1 == 'I')
                {
                    sb.Append($"{idBegin}{_TextFragments[part.Item2]}{idEnd}");
                }
                else if (part.Item1 == 'N')
                {
                    Debug.Assert(part.Item2 == -1);
                    sb.AppendLine();
                }
                else if (part.Item1 == 'L')
                {
                    //At least MySQL wants a space after the --.
                    string text = _TextFragments[part.Item2];
                    sb.Append($"--{(text.StartsWith(" ") ? "" : " ")}{text}");
                }
                else if (part.Item1 == 'C')
                {
                    sb.Append($"/*{_TextFragments[part.Item2]}*/");
                }
                else if (part.Item1 == 'P')
                {
                    sb.Append($"{pPrefix}{part.Item2}");
                }
                else if (part.Item1 == 'U')
                {
                    throw new InvalidOperationException("Comment section not properly closed");
                }
                else
                    throw new InvalidOperationException();
            }
            return sb.ToString();
        }
        internal object[] GetCommandParameters() { return _ParameterValues.ToArray(); }
        public override string ToString() { return GetCommandText(null); }
    }
}