﻿using AntFarm.Core;
using AntFarm.ORM.Providers;
using AntFarm.ORM.SQL.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq.Expressions;

namespace AntFarm.ORM
{
    /// <summary>
    /// Helps a <see cref="Database"/> manage operations on its row types by connecting the types to their
    /// respective <see cref="AntFarm.ORM.Providers.TableProvider"/> and acting as a broker between the two
    /// classes.
    /// NOTE: As it now stands, this class adds little value and may be eliminated in the future.
    /// </summary>
    public class RowManager
    {
        #region Global RowManager Cache

        private static readonly Quiver<RowManager> __RowManagerQuiver = new Quiver<RowManager>();

        public static RowManager GetManager(Type pocoType, DatabaseProvider dbProvider)
        {
            string key = $"{pocoType.FullName}-{dbProvider.GetType().FullName}";
            return __RowManagerQuiver.GetArrow(key, () => new RowManager(dbProvider, pocoType));
        }

        #endregion

        private Sequence Sequence { get; } = new Sequence();

        private string _CommandTextForSelectAll;
        private string _CommandTextForSelectByPK;
        private string _CommandTextForInsert;
        private string _CommandTextForUpdate;
        private string _CommandTextForTruncate;
        private string _CommandTextForDelete;
        private string _CommandTextForDeleteAll;

        public RowManager(DatabaseProvider dbProvider, Type rowType)
        {
            TableProvider = dbProvider.CreateTableProvider(rowType);
        }

        public TableProvider TableProvider { get; }
        public DatabaseProvider DatabaseProvider => TableProvider.DatabaseProvider;
        public Type RowType => TableProvider.RowType;

        public object[] GetPrimaryKey(object row)
        {
            TableProvider.ThrowIfNoPrimaryKey();
            return TableProvider.PrimaryKeyGetter.Invoke(row);
        }

        public void SetPrimaryKey(object row, object[] pk)
        {
            TableProvider.ThrowIfNoPrimaryKey();
            TableProvider.PrimaryKeySetter.Invoke(row, pk);
        }

        ///// <summary>
        ///// Initializes the given row's properties with defaults, if any.
        ///// </summary>
        //public void SetDefaultProperties(object row)
        //{
        //    foreach (var colInfo in TableProvider.Columns)
        //    {
        //        if (colInfo.IsAutoNumber || colInfo.SequenceName != null)
        //        {
        //            if (colInfo.ColumnType != typeof(int))
        //                throw new NotImplementedException($"Auto-number or sequences of type other than {typeof(int)} are not yet implemented.");
        //            Utils.SetPropertyValue(colInfo.Accessor, row, -Sequence.GetNextValue());
        //        }
        //    }
        //}

        public void PrepareForInsert(object row, DateTimeOffset syscreaon, string syscreaby)
        {
            TableProvider.InsertPreparer.Invoke(row, syscreaon, syscreaby);
        }

        public void PrepareForUpdate(object row, DateTimeOffset sysupdton, string sysupdtby)
        {
            TableProvider.UpdatePreparer.Invoke(row, sysupdton, sysupdtby);
        }

        public void InitializeCommandForSelectAll(IDbCommand cd)
        {
            if (_CommandTextForSelectAll == null)
                _CommandTextForSelectAll = TableProvider.GetCommandTextForSelectAll();
            cd.CommandText = _CommandTextForSelectAll;
            cd.CommandType = CommandType.Text;
        }

        public void InitializeCommandForSelectByPK(IDbCommand cd, object[] pk)
        {
            if (_CommandTextForSelectByPK == null)
                _CommandTextForSelectByPK = TableProvider.GetCommandTextForSelectByPK();
            cd.CommandText = _CommandTextForSelectByPK;
            cd.CommandType = CommandType.Text;
            TableProvider.ParameterizeCommandForSelectByPK(cd, pk);
        }

        public void InitializeCommandForSelectByLambda<T>(IDbCommand cd, Expression<Func<T, bool>> predicate)
        {
            var whereClauseBuilder = new SqlExpressionBuilder(this.TableProvider);
            var whereClause = whereClauseBuilder.BuildWhereClause(predicate);
            var whereClauseParameters = new List<(string, object, DbType)>();
            cd.CommandText = TableProvider.GetCommandTextForSelectAllWhere(whereClause, (pvalue, ptype) =>
            {
                int pindex = whereClauseParameters.Count;
                string pname = $"{TableProvider.DatabaseProvider.SQLParameterPrefix}P{pindex}";
                whereClauseParameters.Add((pname, pvalue, ptype));
                return pname;
            });
            cd.CommandType = CommandType.Text;
            TableProvider.DatabaseProvider.ParameterizeCommand(cd, whereClauseParameters);
        }

        public IDbDataParameter[] InitializeCommandForInsert(IDbCommand cd, object row)
        {
            if (_CommandTextForInsert == null)
                _CommandTextForInsert = TableProvider.GetCommandTextForInsert();
            cd.CommandText = _CommandTextForInsert;
            cd.CommandType = CommandType.Text;
            return TableProvider.ParameterizeCommandForInsert(cd, row);
        }

        public void InitializeCommandForUpdate(IDbCommand cd, object row)
        {
            if (_CommandTextForUpdate == null)
                _CommandTextForUpdate = TableProvider.GetCommandTextForUpdate();
            cd.CommandText = _CommandTextForUpdate;
            cd.CommandType = CommandType.Text;
            TableProvider.ParameterizeCommandForUpdate(cd, row);
        }

        public void InitializeCommandForTruncate(IDbCommand cd)
        {
            if (_CommandTextForTruncate == null)
                _CommandTextForTruncate = TableProvider.GetCommandTextForTruncate();
            cd.CommandText = _CommandTextForTruncate;
            cd.CommandType = CommandType.Text;
        }

        public void InitializeCommandForDelete(IDbCommand cd, object[] pk)
        {
            if (_CommandTextForDelete == null)
                _CommandTextForDelete = TableProvider.GetCommandTextForDelete();
            cd.CommandText = _CommandTextForDelete;
            cd.CommandType = CommandType.Text;
            TableProvider.ParameterizeCommandForDelete(cd, pk);
        }

        public void InitializeCommandForDeleteAll(IDbCommand cd)
        {
            if (_CommandTextForDeleteAll == null)
                _CommandTextForDeleteAll = TableProvider.GetCommandTextForDeleteAll();
            cd.CommandText = _CommandTextForDeleteAll;
            cd.CommandType = CommandType.Text;
        }

        /// <summary>
        /// Serializes a row value to a byte array.
        /// </summary>
        internal byte[] Row2Bytes(object row)
        {
            if (row == null)
                return null;
            else
            {
                using (var stream = new MemoryStream())
                using (var bw = new BinaryWriterEx(stream))
                {
                    TableProvider.Serializer.Invoke(bw, row);
                    return stream.ToArray();
                }
            }
        }
    }
}