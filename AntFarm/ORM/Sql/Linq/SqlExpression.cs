﻿using AntFarm.ORM.Providers;

namespace AntFarm.ORM.SQL.Linq
{
    public abstract class SqlExpression
    {
        public virtual bool IsDbNull => false;
    }

    public class SqlNull : SqlExpression
    {
        public override bool IsDbNull => true;
    }

    public class SqlConstant : SqlExpression
    {
        public SqlConstant(object value) => Value = value;
        public object Value { get; }
    }

    public class SqlObject : SqlExpression
    {
        public SqlObject(object value) => Value = value;
        public object Value { get; }
    }

    public class SqlVariable : SqlExpression
    {
        public SqlVariable(ColumnProvider colProvider) => ColumnProvider = colProvider;
        public ColumnProvider ColumnProvider { get; }
    }

    public enum SqlUnaryOperator { Plus, Minus, Not }
    public class SqlUnaryOperation : SqlExpression
    {
        public SqlUnaryOperation(SqlUnaryOperator op, SqlExpression operand)
        {
            this.Operator = op;
            this.Operand = operand;
        }
        public SqlUnaryOperator Operator { get; }
        public SqlExpression Operand { get; }
    }

    public enum SqlBinaryOperator
    {
        Equal, NotEqual,
        Smaller, SmallerOrEqual, Greater, GreaterOrEqual,
        And, Or
    }
    public class SqlBinaryOperation : SqlExpression
    {
        public SqlBinaryOperation(SqlExpression left, SqlBinaryOperator op, SqlExpression right)
        {
            this.LeftOperand = left;
            this.Operator = op;
            this.RightOperand = right;
        }
        public SqlExpression LeftOperand { get; }
        public SqlBinaryOperator Operator { get; }
        public SqlExpression RightOperand { get; }
    }

    //public abstract class SqlFunction : SqlExpression
    //{ }
}