﻿using System.Linq;
using System.Text.RegularExpressions;

namespace AntFarm.ORM.SQL
{
    /// <summary>
    /// Helps support a convenience feature of AntFarm wherein a SELECT statement against a table can omit the command ("select"),
    /// list of column names and table name.
    /// Ensures that the SQL selection statement is prepended with the proper command, column list and table name.
    /// Credit: This feature was inspired by PetaPoco and the code was pulled from Git, with as few changes as possible:
    /// See also: https://github.com/CollaboratingPlatypus/PetaPoco
    /// </summary>
    internal static class AutoSelectHelper
    {
        private static Regex rxSelect = new Regex(@"\A\s*(SELECT|EXECUTE|CALL|WITH|SET|DECLARE)\s",
            RegexOptions.Compiled | RegexOptions.Singleline | RegexOptions.IgnoreCase | RegexOptions.Multiline);

        private static Regex rxFrom = new Regex(@"\A\s*FROM\s",
            RegexOptions.Compiled | RegexOptions.Singleline | RegexOptions.IgnoreCase | RegexOptions.Multiline);

        public static string AddSelectClause(string sql, RowManager rowMgr)
        {
            if (sql.StartsWith(";"))
                return sql.Substring(1);
            if (!rxSelect.IsMatch(sql))
            {
                var tableName = rowMgr.TableProvider.GetTableName();
                var selcols = rowMgr.TableProvider.Columns; //TODO: for now OK, but better to get the list from the provider in case there are special features that make a column not eligible
                string cols = selcols.Count != 0
                    ? string.Join(", ", (from c in selcols select tableName + "." + rowMgr.TableProvider.GetColumnName(c)).ToArray())
                    : "NULL";
                if (!rxFrom.IsMatch(sql))
                    sql = string.Format("SELECT {0} FROM {1} {2}", cols, tableName, sql);
                else
                    sql = string.Format("SELECT {0} {1}", cols, sql);
            }
            return sql;
        }
    }
}