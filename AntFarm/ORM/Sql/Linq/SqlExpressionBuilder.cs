﻿using AntFarm.ORM.Providers;
using System;
using System.Diagnostics;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace AntFarm.ORM.SQL.Linq
{
    internal class SqlExpressionBuilder : ExpressionVisitor
    {
        #region Helpers

        protected static TypeCode ValueToTypeCode(object value)
        {
            if (value == null)
            {
                return TypeCode.Empty;
            }
            else
            {
                var type = value.GetType();
                var typeCode = Type.GetTypeCode(type);
                switch (typeCode)
                {
                    case TypeCode.Boolean:
                    case TypeCode.Byte:
                    case TypeCode.Char:
                    case TypeCode.DateTime:
                    case TypeCode.Decimal:
                    case TypeCode.Double:
                    case TypeCode.Int16:
                    case TypeCode.Int32:
                    case TypeCode.Int64:
                    case TypeCode.SByte:
                    case TypeCode.String:
                    case TypeCode.UInt16:
                    case TypeCode.UInt32:
                    case TypeCode.UInt64:
                        return typeCode;
                    default:
                        return TypeCode.Object;
                }
            }
        }

        protected static object GetMemberValue(object instance, MemberInfo member)
        {
            var type = instance.GetType();
            switch (member.MemberType)
            {
                case MemberTypes.Field:
                    {
                        var finfo = type.GetField(member.Name);
                        return finfo.GetValue(instance);
                    }
                case MemberTypes.Property:
                    {
                        var pinfo = type.GetProperty(member.Name);
                        return pinfo.GetValue(instance);
                    }
                default:
                    throw new NotSupportedException($"Member type {member.MemberType} not supported");
            }
        }

        #endregion

        private SqlExpression _Current = null;

        public SqlExpressionBuilder(TableProvider tbProvider)
        {
            TableProvider = tbProvider;
        }

        public TableProvider TableProvider { get; }

        public SqlExpression BuildWhereClause(Expression expression)
        {
            _Current = null;
            Visit(expression);
            return _Current;
        }

        protected override Expression VisitBinary(BinaryExpression node)
        {
            Visit(node.Left);
            var left = _Current;
            Debug.Assert(left != null);

            SqlBinaryOperator op;
            switch (node.NodeType)
            {
                case ExpressionType.And:
                    op = SqlBinaryOperator.And;
                    break;
                case ExpressionType.Or:
                    op = SqlBinaryOperator.Or;
                    break;
                case ExpressionType.Equal:
                    op = SqlBinaryOperator.Equal;
                    break;
                case ExpressionType.NotEqual:
                    op = SqlBinaryOperator.NotEqual;
                    break;
                case ExpressionType.LessThan:
                    op = SqlBinaryOperator.Smaller;
                    break;
                case ExpressionType.LessThanOrEqual:
                    op = SqlBinaryOperator.SmallerOrEqual;
                    break;
                case ExpressionType.GreaterThan:
                    op = SqlBinaryOperator.Greater;
                    break;
                case ExpressionType.GreaterThanOrEqual:
                    op = SqlBinaryOperator.GreaterOrEqual;
                    break;
                default:
                    throw new NotSupportedException($"The binary operator '{node.NodeType}' is not supported.");
            }

            Visit(node.Right);
            var right = _Current;

            _Current = new SqlBinaryOperation(left, op, right);

            return node;
        }

        protected override Expression VisitConstant(ConstantExpression node)
        {
            var value = node.Value;
            var typeCode = ValueToTypeCode(value);
            switch (typeCode)
            {
                case TypeCode.Empty:
                    _Current = new SqlNull();
                    break;
                case TypeCode.Object:
                    _Current = new SqlObject(value);
                    break;
                default:
                    _Current = new SqlConstant(value);
                    break;
            }
            return node;
        }

        protected override Expression VisitMember(MemberExpression node)
        {
            if (node.Expression?.NodeType == ExpressionType.Parameter)
            {
                var colInfo = TableProvider.Columns
                    .SingleOrDefault(x => x.Accessor.Name.Equals(node.Member.Name, StringComparison.Ordinal));
                if (colInfo == null)
                    throw new InvalidOperationException($"Identifier {node.Member.Name} unmatched to database column");
                _Current = new SqlVariable(colInfo);
            }
            else if (node.Expression?.NodeType == ExpressionType.Constant)
            {
                var constExpression = node.Expression as ConstantExpression;
                object value = GetMemberValue(constExpression.Value, node.Member);
                var typeCode = ValueToTypeCode(value);
                switch (typeCode)
                {
                    case TypeCode.Empty:
                        _Current = new SqlNull();
                        break;
                    case TypeCode.Object:
                        _Current = new SqlObject(value);
                        break;
                    default:
                        _Current = new SqlConstant(value);
                        break;
                }
            }
            else if (node.Expression?.NodeType == ExpressionType.MemberAccess)
            {
                var memberExpression = node.Expression as MemberExpression;
                var memberExpressionToObject = Expression.Convert(memberExpression, typeof(object));
                var getterLambda = Expression.Lambda<Func<object>>(memberExpressionToObject);
                var getterLambdaCompiled = getterLambda.Compile();
                object value;
                if (Nullable.GetUnderlyingType(node.Expression.Type) != null)
                {
                    //The getter returns the nullable expression Value.
                    switch (node.Member.Name)
                    {
                        case "Value":
                            value = getterLambdaCompiled.Invoke();
                            break;
                        case "HasValue":
                            value = getterLambdaCompiled.Invoke() != null;
                            break;
                        default:
                            throw new NotImplementedException($"The member '{node.Member.Name}' is not supported on nullable types.");
                    }
                }
                else
                {
                    //The getter returns the object, and we just need to invoke the member property or field.
                    var memberValue = getterLambdaCompiled.Invoke();
                    value = GetMemberValue(memberValue, node.Member);
                }
                var typeCode = ValueToTypeCode(value);
                switch (typeCode)
                {
                    case TypeCode.Empty:
                        _Current = new SqlNull();
                        break;
                    case TypeCode.Object:
                        _Current = new SqlObject(value); //Currently not supported when emiting SQL
                        break;
                    default:
                        _Current = new SqlConstant(value);
                        break;
                }
            }
            else
            {
                throw new NotSupportedException($"The member '{node.Member.Name}' is not supported.");
            }

            return node;
        }

        protected override Expression VisitMethodCall(MethodCallExpression node)
        {
            throw new NotSupportedException($"The method '{node.Method.Name}' is not supported.");
        }

        protected override Expression VisitUnary(UnaryExpression node)
        {
            SqlUnaryOperator op;
            switch (node.NodeType)
            {
                case ExpressionType.Not:
                    op = SqlUnaryOperator.Not;
                    break;
                case ExpressionType.Or:
                    op = SqlUnaryOperator.Plus;
                    break;
                case ExpressionType.Equal:
                    op = SqlUnaryOperator.Minus;
                    break;
                default:
                    throw new NotSupportedException($"The unary operator '{node.NodeType}' is not supported.");
            }

            Visit(node.Operand);
            var operand = _Current;

            _Current = new SqlUnaryOperation(op, operand);

            return node;
        }


#if DEBUG
        #region Useful for debugging breakpoints

        public override Expression Visit(Expression node)
        {
            //Good stopping place to but a break if needed.
            var nodeType = node.NodeType;
            return base.Visit(node);
        }

        protected override Expression VisitLambda<T>(Expression<T> node)
        {
            //Normally called once for the entire lambda expression. No need to
            //do anything.
            return base.VisitLambda(node);
        }

        protected override Expression VisitParameter(ParameterExpression node)
        {
            //In an expression like: { x => x.ColumnYYY == "ZZZ" }, the node will reference the
            //lambda expression's "x" parameter. Here, it represents the actual record, so no
            //need to do anything.
            return base.VisitParameter(node);
        }

        #endregion
#endif


#if DEBUG
        #region Unsupported "Visit" methods

        protected override Expression VisitBlock(BlockExpression node) => throw new NotSupportedException();
        protected override CatchBlock VisitCatchBlock(CatchBlock node) => throw new NotSupportedException();
        protected override Expression VisitConditional(ConditionalExpression node) => throw new NotSupportedException();
        protected override Expression VisitDebugInfo(DebugInfoExpression node) => throw new NotSupportedException();
        protected override Expression VisitDefault(DefaultExpression node) => throw new NotSupportedException();
        protected override Expression VisitDynamic(DynamicExpression node) => throw new NotSupportedException();
        protected override ElementInit VisitElementInit(ElementInit node) => throw new NotSupportedException();
        protected override Expression VisitExtension(Expression node) => throw new NotSupportedException();
        protected override Expression VisitGoto(GotoExpression node) => throw new NotSupportedException();
        protected override Expression VisitIndex(IndexExpression node) => throw new NotSupportedException();
        protected override Expression VisitInvocation(InvocationExpression node) => throw new NotSupportedException();
        protected override Expression VisitLabel(LabelExpression node) => throw new NotSupportedException();
        protected override LabelTarget VisitLabelTarget(LabelTarget node) => throw new NotSupportedException();
        protected override Expression VisitListInit(ListInitExpression node) => throw new NotSupportedException();
        protected override Expression VisitLoop(LoopExpression node) => throw new NotSupportedException();
        protected override MemberAssignment VisitMemberAssignment(MemberAssignment node) => throw new NotSupportedException();
        protected override MemberBinding VisitMemberBinding(MemberBinding node) => throw new NotSupportedException();
        protected override Expression VisitMemberInit(MemberInitExpression node) => throw new NotSupportedException();
        protected override MemberListBinding VisitMemberListBinding(MemberListBinding node) => throw new NotSupportedException();
        protected override MemberMemberBinding VisitMemberMemberBinding(MemberMemberBinding node) => throw new NotSupportedException();
        protected override Expression VisitNew(NewExpression node) => throw new NotSupportedException();
        protected override Expression VisitNewArray(NewArrayExpression node) => throw new NotSupportedException();
        protected override Expression VisitRuntimeVariables(RuntimeVariablesExpression node) => throw new NotSupportedException();
        protected override Expression VisitSwitch(SwitchExpression node) => throw new NotSupportedException();
        protected override SwitchCase VisitSwitchCase(SwitchCase node) => throw new NotSupportedException();
        protected override Expression VisitTry(TryExpression node) => throw new NotSupportedException();
        protected override Expression VisitTypeBinary(TypeBinaryExpression node) => throw new NotSupportedException();

        #endregion
#endif
    }
}