using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace AntFarm.ORM.SQL
{
    internal class SqlParser
    {
        public List<(char, object)> Parse(string expr, char parameterPrefix)
        {
            if (expr != null)
            {
                try
                {
                    m_buffer = expr.ToCharArray();
                    m_bufferLength = m_buffer.Length;
                    m_bufferIndex = 0;
                    m_symbolBuffer = new char[m_bufferLength];
                    NextChar();
                    NextSymbol(parameterPrefix);
                    return BuildExpression(parameterPrefix);
                }
                finally
                {
                    m_buffer = null;
                    m_symbolBuffer = null;
                }
            }
            return null;
        }

        #region Parsing

        private enum Symbol
        {
            Text, QuotedText, Identifier, Parameter, LineComment, Comment, OpenComment, End
        }

        private char[] m_buffer;
        private int m_bufferLength;
        private int m_bufferIndex;
        private char m_char;
        private Symbol m_symbol;
        private char[] m_symbolBuffer;
        private int m_symbolLength;

        private List<(char, object)> BuildExpression(char parameterPrefix)
        {
            var list = new List<(char, object)>();
            while (m_symbol != Symbol.End)
                switch (m_symbol)
                {
                    case Symbol.Text:
                    case Symbol.QuotedText:
                        {
                            string symbolString = new string(m_symbolBuffer, 0, m_symbolLength);
                            NextSymbol(parameterPrefix);
                            list.Add(('T', symbolString));
                        }
                        break;
                    case Symbol.Identifier:
                        {
                            //string symbolString = new string(m_symbolBuffer, 1, m_symbolLength - 2);
                            string symbolString = new string(m_symbolBuffer, 0, m_symbolLength);
                            NextSymbol(parameterPrefix);
                            list.Add(('I', symbolString));
                        }
                        break;
                    case Symbol.Parameter:
                        {
                            string symbolString = new string(m_symbolBuffer, 0, m_symbolLength);
                            int paramIndex = int.Parse(symbolString);
                            NextSymbol(parameterPrefix);
                            list.Add(('P', paramIndex));
                        }
                        break;
                    case Symbol.LineComment:
                        {
                            //string symbolString = new string(m_symbolBuffer, 0, m_symbolLength).TrimEnd()/*trim to remove white space including \r or \n at end*/;
                            string symbolString = new string(m_symbolBuffer, 0, m_symbolLength);
                            NextSymbol(parameterPrefix);
                            list.Add(('L', symbolString));
                        }
                        break;
                    case Symbol.Comment:
                        {
                            //string symbolString = new string(m_symbolBuffer, 0, m_symbolLength).Trim(' ')/*only trim whitespace (not \r\n)*/;
                            string symbolString = new string(m_symbolBuffer, 0, m_symbolLength);
                            NextSymbol(parameterPrefix);
                            list.Add(('C', symbolString));
                        }
                        break;
                    case Symbol.OpenComment:
                        {
                            list.Add(('U', null));
                            m_symbol = Symbol.End;
                        }
                        break;
                    default:
                        throw new InvalidOperationException();
                }
            return list;
        }

        private void NextSymbol(char parameterPrefix)
        {
            m_symbolLength = 0;
            switch (m_char)
            {
                case '\0':
                    m_symbol = Symbol.End;
                    break;

                case '\'': //String literal
                    m_symbol = Symbol.QuotedText;
                    m_symbolBuffer[m_symbolLength++] = m_char;
                    NextChar();
                    while (m_char != '\'')
                    {
                        if (m_char == '\\')
                            NextChar();
                        m_symbolBuffer[m_symbolLength++] = m_char;
                        NextChar();
                    }
                    m_symbolBuffer[m_symbolLength++] = m_char;
                    NextChar();
                    break;

                case '"': //ANSI style identifier
                    m_symbol = Symbol.Identifier;
                    //m_symbolBuffer[m_symbolLength++] = m_char;
                    NextChar();
                    while (m_char != '"')
                    {
                        if (m_char == '\\')
                            NextChar();
                        m_symbolBuffer[m_symbolLength++] = m_char;
                        NextChar();
                    }
                    //m_symbolBuffer[m_symbolLength++] = m_char;
                    NextChar();
                    break;

                case '[': //SQL Server style identifier
                    m_symbol = Symbol.Identifier;
                    //m_symbolBuffer[m_symbolLength++] = m_char;
                    NextChar();
                    while (m_char != ']')
                    {
                        m_symbolBuffer[m_symbolLength++] = m_char;
                        NextChar();
                    }
                    //m_symbolBuffer[m_symbolLength++] = m_char;
                    NextChar();
                    break;

                case '`': //MySQL style identifier
                    m_symbol = Symbol.Identifier;
                    //m_symbolBuffer[m_symbolLength++] = m_char;
                    NextChar();
                    while (m_char != '`')
                    {
                        m_symbolBuffer[m_symbolLength++] = m_char;
                        NextChar();
                    }
                    //m_symbolBuffer[m_symbolLength++] = m_char;
                    NextChar();
                    break;

                case '-': //Line comment (possibly)
                    if (PeekChar() == '-')
                    {
                        m_symbol = Symbol.LineComment;
                        NextChar();
                        Debug.Assert(m_char == '-');
                        NextChar();
                        while (m_char != '\0')
                        {
                            bool eol = m_char == '\n';
                            m_symbolBuffer[m_symbolLength++] = m_char;
                            NextChar();
                            if (eol)
                                break;
                        }
                        break;
                    }
                    else
                        goto default;

                case '/': //Comment (possibly)
                    if (PeekChar() == '*')
                    {
                        m_symbol = Symbol.OpenComment;
                        NextChar();
                        Debug.Assert(m_char == '*');
                        NextChar();
                        while (m_char != '\0')
                        {
                            bool eoc = m_char == '*' && PeekChar() == '/';
                            if (eoc)
                            {
                                NextChar();
                                Debug.Assert(m_char == '/');
                                NextChar();
                                m_symbol = Symbol.Comment;
                                break;
                            }
                            m_symbolBuffer[m_symbolLength++] = m_char;
                            NextChar();
                        }
                        break;
                    }
                    else
                        goto default;

                default:
                    if (m_char == parameterPrefix)
                    {
                        m_symbol = Symbol.Parameter;
                        NextChar();
                        while (char.IsNumber(m_char))
                        {
                            m_symbolBuffer[m_symbolLength++] = m_char;
                            NextChar();
                        }
                    }
                    else
                    {
                        m_symbol = Symbol.Text;
                        m_symbolBuffer[m_symbolLength++] = m_char;
                        NextChar();
                        while (m_char != '\0' && m_char != '"' && m_char != '\'' && m_char != '[' && m_char != '`' && m_char != '-' && m_char != '/' && m_char != parameterPrefix)
                        {
                            m_symbolBuffer[m_symbolLength++] = m_char;
                            NextChar();
                        }
                    }
                    break;
            }
        }

        private void NextChar()
        {
            if (m_bufferIndex < m_bufferLength)
            {
                m_char = m_buffer[m_bufferIndex];
                m_bufferIndex++;
            }
            else if (m_bufferIndex == m_bufferLength)
            {
                m_char = '\0';
                m_bufferIndex++;
            }
            else
                throw new Exception("Reading past end of string");
        }

        private char PeekChar()
        {
            if (m_bufferIndex < m_bufferLength)
            {
                return m_buffer[m_bufferIndex];
            }
            else if (m_bufferIndex == m_bufferLength)
            {
                return '\0';
            }
            else
                throw new Exception("Reading past end of string");
        }

        #endregion
    }
}