﻿using AntFarm.Core;
using AntFarm.ORM.Providers;
using AntFarm.ORM.SQL;
using Microsoft.Extensions.Logging;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Dynamic;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Reflection.Emit;

namespace AntFarm.ORM
{
    public class Database : IDisposable
    {
        #region IDisposable

        /// <summary>
        /// The connection specifically created by this instance, and which must be disposed of during finalization.
        /// Currently the same as _Connection, but that could change in the future.
        /// </summary>
        private IDbConnection _DisposableConnection;

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (_DisposableConnection != null)
                {
                    Debug.Assert(_DisposableConnection == _Connection);
                    if (_OpenConnectionCount > 0)
                        throw new Exception("Unclosed connection being disposed");
                    _DisposableConnection.Dispose();
                    _DisposableConnection = null;
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            // TODO: uncomment the following line if implementing a finalizer.
            // GC.SuppressFinalize(this);
        }

        #endregion

        /// <summary>
        /// Implements the ability to iterate over a set of rows returned by a data reader.
        /// </summary>
        /// <typeparam name="T">The type of row being iterated over.</typeparam>
        /// <remarks>
        /// The challenge being addressed here is keeping the connection open until the iteration is over.
        /// Indeed, all the data selection methods (SelectXXX or EnumerateXXX) have the same pattern wherein
        /// the body of the method, which in all cases creates a data reader, executes while the connection is
        /// briefly opened for the duration of the method and the data is collected. This works fine when the
        /// data reader is consumed locally (SelectXXX methods), but does not work if the data reader is exposed
        /// outside (EnumerateXXX), because in that case the connection gets closed upon exiting the method,
        /// and the reader becomes useless. For that we need an iterator that will ensure the connection remains
        /// open, and that is what this class does.
        /// </remarks>
        public class RowEnumerable<T> : IEnumerable<T>, IEnumerator<T>
        {
            object IEnumerator.Current => Current;
            IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
            private Database _Database;
            private IDbCommand _Command;
            private IDataReader _DataReader;
            private Func<DataReaderCo, T> _ILReader;
            private DataReaderCo _CoReader;
            internal RowEnumerable(Database db, IDbCommand cd, IDataReader dr, Func<DataReaderCo, T> ilReader, DataReaderCo coReader)
            {
                //An open connection to the backend (at least 1) is expected, but the code that instantites us will soon close it. If the
                //connection count is only 1, this will effectively close the connection and prevent iteration over the data. To prevent this,
                //we call OpenConnection, which will increase the connection counter. We'll do the opposite upon disposing.
                _Database = db;
                Debug.Assert(_Database.OpenConnectionCount > 0);
                _Database.OpenConnection();
                //cd and dr are other resources whose disposal we are now responsible for. Note that not all providers expect the command to remain
                //alive, but some (like MySQL) do. It depends on the implementation.
                _Command = cd;
                _DataReader = dr;
                //These are the resources we really need to iterate through the data:
                _ILReader = ilReader;
                _CoReader = coReader;
                Debug.Assert(coReader.DataReader == dr);
            }
            public void Dispose()
            {
                //We can dispose of the data resources. See more info in the cosntructor.
                _DataReader?.Dispose();
                _Command?.Dispose();
                _Database?.CloseConnection();
            }
            public T Current => _ILReader.Invoke(_CoReader);
            public bool MoveNext() => _CoReader.DataReader.Read();
            public void Reset() => throw new NotSupportedException();
            public IEnumerator<T> GetEnumerator() => this;
        }

        private readonly ILogger _Logger;
        private readonly ILogger _SqlLogger;
        private readonly IDbConnection _Connection;

        /// <summary>
        /// Initializes a new <see cref="Database"/>.
        /// </summary>
        public Database(string csKey)
            : this(AntFarmer.DefaultConfig ?? throw new InvalidOperationException("Missing AntFarmManager's default configuration."), csKey)
        { }

        /// <summary>
        /// Initializes a new <see cref="Database"/>.
        /// </summary>
        public Database(AntConfig config, string csKey)
        {
            Config = config ?? throw new ArgumentNullException(nameof(config));
            _Logger = config.LoggerFactory?.CreateLogger<Database>();
            _SqlLogger = Config.SqlLoggingEnabled ? _Logger : null;
            var cs = Config.ConnectionStrings[csKey ?? throw new ArgumentNullException(nameof(csKey))];
            DatabaseProvider = cs.Provider;
            _DisposableConnection = cs.Provider.CreateConnection();
            _Connection = _DisposableConnection;
            _Connection.ConnectionString = cs.ConnectionString;
            UserName = config.DefaultUserName;
            DataCulture = config.DefaultDataCulture;
            DataTimeZone = config.DefaultDataTimeZone;
        }

        public AntConfig Config { get; }
        public DatabaseProvider DatabaseProvider { get; }

        /// <summary>
        /// The user identifier used to tag records when creating or modifying them.
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// The <see cref="CultureInfo"/> used to format or interpret data during conversions.
        /// </summary>
        public CultureInfo DataCulture { get; set; }

        /// <summary>
        /// The <see cref="TimeZoneInfo"/> of reference for date/time information.
        /// </summary>
        public TimeZoneInfo DataTimeZone { get; set; }

        ///// <summary>
        ///// Returns the current date time as a <see cref="DateTimeOffset"/> in the data time zone.
        ///// See property DataTimeZone.
        ///// </summary>
        ///// <returns></returns>
        //public DateTimeOffset NowInDataTimeZone() => DataTimeZone.Now();

        #region Shared Connection Management (protected)

        private int _OpenConnectionCount;

        public int OpenConnectionCount => _OpenConnectionCount;

        protected void OpenConnection()
        {
            if (_OpenConnectionCount == 0)
            {
                _SqlLogger?.LogDebug("Acquiring connection...");
                _Connection.Open();
            }
            _OpenConnectionCount++;
        }

        protected void CloseConnection()
        {
            _OpenConnectionCount--;
            if (_OpenConnectionCount == 0)
            {
                _SqlLogger?.LogDebug("Dismissing connection...");
                _Connection.Close();
            }
        }

        protected void ExecuteInOpenConnection(Action<IDbTransaction> action)
        {
            OpenConnection();
            try { action.Invoke(CurrentTransaction); }
            finally { CloseConnection(); }
        }

        protected T ExecuteInOpenConnection<T>(Func<IDbTransaction, T> func)
        {
            OpenConnection();
            try { return func.Invoke(CurrentTransaction); }
            finally { CloseConnection(); }
        }

        #endregion

        #region Selection Methods

        /* SelectPK */

        /// <summary>
        /// Selects a single row in a table by its non-compound primary key.
        /// </summary>
        /// <typeparam name="T">The type representing the table affected by the operation.</typeparam>
        /// <param name="pk">The row's simple primary key.</param>
        /// <returns>0 or 1 row matching the primary key.</returns>
        public T SelectPK<T>(object pk)
        {
            return SelectPK<T>(new[] { pk });
        }

        /// <summary>
        /// Selects a single row in a table by its compound primary key.
        /// </summary>
        /// <typeparam name="T">The type representing the table affected by the operation.</typeparam>
        /// <param name="pk">The row's compound primary key, in the same order as the defining properties.</param>
        /// <returns>0 or 1 row matching the primary key.</returns>
        public T SelectPK<T>(object[] pk)
        {
            Type rowType = typeof(T);
            var rowmgr = RowManager.GetManager(rowType, DatabaseProvider);
            rowmgr.TableProvider.ThrowIfNoPrimaryKey(); //TODO: not the best message!
            return ExecuteInOpenConnection((tn) =>
            {
                using (var cd = _Connection.CreateCommand())
                {
                    cd.Transaction = tn;
                    rowmgr.InitializeCommandForSelectByPK(cd, pk);
                    return ReadOne<T>(cd, rowmgr.TableProvider);
                }
            });
        }

        /* SelectOne */

        /// <summary>
        /// Selects a single row in a table.
        /// </summary>
        /// <typeparam name="T">The type representing the table affected by the operation.</typeparam>
        /// <param name="query">A parameterized SQL query used to filter the rows in the table.</param>
        /// <param name="values">The parameterized query values.</param>
        /// <returns>0 or 1 row matching the filter.</returns>
        public T SelectOne<T>(string query, params object[] values)
        {
            return SelectOne<T>(Sql.Builder.Append(query, values));
        }

        /// <summary>
        /// Selects a single row in a table.
        /// </summary>
        /// <typeparam name="T">The type representing the table affected by the operation.</typeparam>
        /// <param name="sql">The SQL query used to filter the row in the table.</param>
        /// <returns>0 or 1 row matching the filter.</returns>
        public T SelectOne<T>(Sql sql)
        {
            var rowmgr = RowManager.GetManager(typeof(T), DatabaseProvider);
            return ExecuteInOpenConnection((tn) =>
            {
                using (var cd = _Connection.CreateCommand())
                {
                    cd.Transaction = tn;
                    string query = sql.GetCommandText(DatabaseProvider);
                    cd.CommandText = AutoSelectHelper.AddSelectClause(query, rowmgr);
                    cd.CommandType = CommandType.Text;
                    object[] values = sql.GetCommandParameters();
                    if (values?.Length > 0)
                        DatabaseProvider.ParameterizeCommand(cd, values);
                    return ReadOne<T>(cd, rowmgr.TableProvider);
                }
            });
        }

        /// <summary>
        /// Selects a single row in a table.
        /// </summary>
        /// <typeparam name="T">The type representing the table affected by the operation.</typeparam>
        /// <param name="sql">The SQL query used to filter the row in the table.</param>
        /// <param name="cbReader">A callback delegate that takes in the row's data and returns the row object.</param>
        /// <returns>0 or 1 row matching the filter.</returns>
        public T SelectOne<T>(Sql sql, Func<DataReaderCo, T> cbReader)
        {
            var rowmgr = RowManager.GetManager(typeof(T), DatabaseProvider);
            return ExecuteInOpenConnection((tn) =>
            {
                using (var cd = _Connection.CreateCommand())
                {
                    cd.Transaction = tn;
                    string query = sql.GetCommandText(DatabaseProvider);
                    cd.CommandText = AutoSelectHelper.AddSelectClause(query, rowmgr);
                    cd.CommandType = CommandType.Text;
                    object[] values = sql.GetCommandParameters();
                    if (values?.Length > 0)
                        DatabaseProvider.ParameterizeCommand(cd, values);
                    return ReadOne(cd, cbReader);
                }
            });
        }

        /// <summary>
        /// Selects a single row in a table.
        /// </summary>
        /// <typeparam name="T">The type representing the table affected by the operation.</typeparam>
        /// <param name="predicate">A lambda expression used to filter the rows in the table.</param>
        /// <returns>0 or 1 row matching the filter.</returns>
        public T SelectOne<T>(Expression<Func<T, bool>> predicate)
        {
            var rowmgr = RowManager.GetManager(typeof(T), DatabaseProvider);
            return ExecuteInOpenConnection<T>((tn) =>
            {
                using (var cd = _Connection.CreateCommand())
                {
                    cd.Transaction = tn;
                    rowmgr.InitializeCommandForSelectByLambda(cd, predicate);
                    return ReadOne<T>(cd, rowmgr.TableProvider);
                }
            });
        }

        /* Select/EnumerateMany */

        /// <summary>
        /// Selects multiple rows in a table.
        /// </summary>
        /// <typeparam name="T">The type representing the table affected by the operation.</typeparam>
        /// <param name="query">A parameterized SQL query used to filter the rows in the table.</param>
        /// <param name="values">The parameterized query values.</param>
        /// <returns>A list of the selected rows matching the filter.</returns>
        public List<T> SelectMany<T>(string query, params object[] values)
        {
            return SelectMany<T>(Sql.Builder.Append(query, values));
        }

        /// <summary>
        /// Selects multiple rows in a table.
        /// </summary>
        /// <typeparam name="T">The type representing the table affected by the operation.</typeparam>
        /// <param name="query">A parameterized SQL query used to filter the rows in the table.</param>
        /// <param name="values">The parameterized query values.</param>
        /// <returns>An enumerator over of all the selected rows.</returns>
        public IEnumerable<T> EnumerateMany<T>(string query, params object[] values)
        {
            return EnumerateMany<T>(Sql.Builder.Append(query, values));
        }

        /// <summary>
        /// Selects multiple rows in a table.
        /// </summary>
        /// <typeparam name="T">The type representing the table affected by the operation.</typeparam>
        /// <param name="sql">The SQL query used to filter the rows in the table.</param>
        /// <returns>A list of the selected rows matching the filter.</returns>
        public List<T> SelectMany<T>(Sql sql)
        {
            var rowmgr = RowManager.GetManager(typeof(T), DatabaseProvider);
            return ExecuteInOpenConnection((tn) =>
            {
                using (var cd = _Connection.CreateCommand())
                {
                    cd.Transaction = tn;
                    string query = sql.GetCommandText(DatabaseProvider);
                    cd.CommandText = AutoSelectHelper.AddSelectClause(query, rowmgr);
                    cd.CommandType = CommandType.Text;
                    object[] values = sql.GetCommandParameters();
                    if (values?.Length > 0)
                        DatabaseProvider.ParameterizeCommand(cd, values);
                    return ReadMany_AsList<T>(cd, rowmgr.TableProvider);
                }
            });
        }

        /// <summary>
        /// Selects multiple rows in a table.
        /// </summary>
        /// <typeparam name="T">The type representing the table affected by the operation.</typeparam>
        /// <param name="sql">The SQL query used to filter the rows in the table.</param>
        /// <returns>A list of the selected rows matching the filter.</returns>
        public List<T> SelectMany<T>(Sql sql, Func<DataReaderCo, T> cbReader)
        {
            var rowmgr = RowManager.GetManager(typeof(T), DatabaseProvider);
            return ExecuteInOpenConnection((tn) =>
            {
                using (var cd = _Connection.CreateCommand())
                {
                    cd.Transaction = tn;
                    string query = sql.GetCommandText(DatabaseProvider);
                    cd.CommandText = AutoSelectHelper.AddSelectClause(query, rowmgr);
                    cd.CommandType = CommandType.Text;
                    object[] values = sql.GetCommandParameters();
                    if (values?.Length > 0)
                        DatabaseProvider.ParameterizeCommand(cd, values);
                    return ReadMany_AsList(cd, cbReader);
                }
            });
        }

        /// <summary>
        /// Selects multiple rows in a table.
        /// </summary>
        /// <typeparam name="T">The type representing the table affected by the operation.</typeparam>
        /// <param name="sql">The SQL query used to filter the rows in the table.</param>
        /// <returns>An enumerator over of all the selected rows.</returns>
        public IEnumerable<T> EnumerateMany<T>(Sql sql)
        {
            var rowmgr = RowManager.GetManager(typeof(T), DatabaseProvider);
            return ExecuteInOpenConnection((tn) =>
            {
                var cd = _Connection.CreateCommand();
                try
                {
                    cd.Transaction = tn;
                    string query = sql.GetCommandText(DatabaseProvider);
                    cd.CommandText = AutoSelectHelper.AddSelectClause(query, rowmgr);
                    cd.CommandType = CommandType.Text;
                    object[] values = sql.GetCommandParameters();
                    if (values?.Length > 0)
                        DatabaseProvider.ParameterizeCommand(cd, values);
                    return ReadMany_AsEnumerable<T>(cd, rowmgr.TableProvider);
                }
                catch
                {
                    cd.Dispose();
                    throw;
                }
            });
        }

        /// <summary>
        /// Selects multiple rows in a table.
        /// </summary>
        /// <typeparam name="T">The type representing the table affected by the operation.</typeparam>
        /// <param name="sql">The SQL query used to filter the rows in the table.</param>
        /// <returns>An enumerator over of all the selected rows.</returns>
        public IEnumerable<T> EnumerateMany<T>(Sql sql, Func<DataReaderCo, T> cbReader)
        {
            var rowmgr = RowManager.GetManager(typeof(T), DatabaseProvider);
            return ExecuteInOpenConnection((tn) =>
            {
                var cd = _Connection.CreateCommand();
                try
                {
                    cd.Transaction = tn;
                    string query = sql.GetCommandText(DatabaseProvider);
                    cd.CommandText = AutoSelectHelper.AddSelectClause(query, rowmgr);
                    cd.CommandType = CommandType.Text;
                    object[] values = sql.GetCommandParameters();
                    if (values?.Length > 0)
                        DatabaseProvider.ParameterizeCommand(cd, values);
                    return ReadMany_AsEnumerable(cd, cbReader);
                }
                catch
                {
                    cd.Dispose();
                    throw;
                }
            });
        }

        /// <summary>
        /// Selects multiple rows in a table.
        /// </summary>
        /// <typeparam name="T">The type representing the table affected by the operation.</typeparam>
        /// <param name="predicate">A lambda expression used to filter the rows in the table.</param>
        /// <returns>A list of the selected rows matching the filter.</returns>
        public List<T> SelectMany<T>(Expression<Func<T, bool>> predicate)
        {
            var rowmgr = RowManager.GetManager(typeof(T), DatabaseProvider);
            return ExecuteInOpenConnection((tn) =>
            {
                using (var cd = _Connection.CreateCommand())
                {
                    cd.Transaction = tn;
                    rowmgr.InitializeCommandForSelectByLambda(cd, predicate);
                    return ReadMany_AsList<T>(cd, rowmgr.TableProvider);
                }
            });
        }

        /// <summary>
        /// Selects multiple rows in a table.
        /// </summary>
        /// <typeparam name="T">The type representing the table affected by the operation.</typeparam>
        /// <param name="predicate">A lambda expression used to filter the rows in the table.</param>
        /// <returns>An enumerator over of all the selected rows.</returns>
        public IEnumerable<T> EnumerateMany<T>(Expression<Func<T, bool>> predicate)
        {
            var rowmgr = RowManager.GetManager(typeof(T), DatabaseProvider);
            return ExecuteInOpenConnection((tn) =>
            {
                var cd = _Connection.CreateCommand();
                try
                {
                    cd.Transaction = tn;
                    rowmgr.InitializeCommandForSelectByLambda(cd, predicate);
                    return ReadMany_AsEnumerable<T>(cd, rowmgr.TableProvider);
                }
                catch
                {
                    cd.Dispose();
                    throw;
                }
            });
        }

        /* Select/EnumerateAll */

        /// <summary>
        /// Selects all the rows in a table.
        /// </summary>
        /// <typeparam name="T">The type representing the table affected by the operation.</typeparam>
        /// <returns>A list of all the selected rows.</returns>
        public List<T> SelectAll<T>()
        {
            Type rowType = typeof(T);
            var rowmgr = RowManager.GetManager(rowType, DatabaseProvider);
            rowmgr.TableProvider.ThrowIfNoTableName(); //TODO: not the best message!
            return ExecuteInOpenConnection((tn) =>
            {
                using (var cd = _Connection.CreateCommand())
                {
                    cd.Transaction = tn;
                    rowmgr.InitializeCommandForSelectAll(cd);
                    return ReadMany_AsList<T>(cd, rowmgr.TableProvider);
                }
            });
        }

        /// <summary>
        /// Selects all the rows in a table.
        /// </summary>
        /// <typeparam name="T">The type representing the table affected by the operation.</typeparam>
        /// <returns>A list of all the selected rows.</returns>
        public List<T> SelectAll<T>(Func<DataReaderCo, T> cbReader)
        {
            Type rowType = typeof(T);
            var rowmgr = RowManager.GetManager(rowType, DatabaseProvider);
            rowmgr.TableProvider.ThrowIfNoTableName(); //TODO: not the best message!
            return ExecuteInOpenConnection((tn) =>
            {
                using (var cd = _Connection.CreateCommand())
                {
                    cd.Transaction = tn;
                    rowmgr.InitializeCommandForSelectAll(cd);
                    return ReadMany_AsList(cd, cbReader);
                }
            });
        }

        /// <summary>
        /// Selects all the rows in a table.
        /// </summary>
        /// <typeparam name="T">The type representing the table affected by the operation.</typeparam>
        /// <returns>A list of all the selected rows.</returns>
        public IEnumerable<T> EnumerateAll<T>()
        {
            Type rowType = typeof(T);
            var rowmgr = RowManager.GetManager(rowType, DatabaseProvider);
            rowmgr.TableProvider.ThrowIfNoTableName(); //TODO: not the best message!
            return ExecuteInOpenConnection((tn) =>
            {
                var cd = _Connection.CreateCommand();
                try
                {
                    cd.Transaction = tn;
                    rowmgr.InitializeCommandForSelectAll(cd);
                    return ReadMany_AsEnumerable<T>(cd, rowmgr.TableProvider);
                }
                catch
                {
                    cd.Dispose();
                    throw;
                }
            });
        }

        /// <summary>
        /// Selects all the rows in a table.
        /// </summary>
        /// <typeparam name="T">The type representing the table affected by the operation.</typeparam>
        /// <returns>An enumerator over of all the selected rows.</returns>
        public IEnumerable<T> EnumerateAll<T>(Func<DataReaderCo, T> cbReader)
        {
            Type rowType = typeof(T);
            var rowmgr = RowManager.GetManager(rowType, DatabaseProvider);
            rowmgr.TableProvider.ThrowIfNoTableName(); //TODO: not the best message!
            return ExecuteInOpenConnection((tn) =>
            {
                var cd = _Connection.CreateCommand();
                try
                {
                    cd.Transaction = tn;
                    rowmgr.InitializeCommandForSelectAll(cd);
                    return ReadMany_AsEnumerable(cd, cbReader);
                }
                catch
                {
                    cd.Dispose();
                    throw;
                }
            });
        }

        /* Private Read methods, including cache management */

        private T ReadOne<T>(IDbCommand cd, TableProvider tbProvider)
        {
            using (var dr = cd.LogAndExecuteReader(_SqlLogger))
            {
                var coReader = DatabaseProvider.CreateDataReaderCo(dr, DataCulture);
                var ilReader = GetFastFactoryAndReader<T>(coReader, tbProvider);
                if (dr.Read())
                {
                    var t = ilReader.Invoke(coReader);
                    coReader.ThrowIfContinueReading();
                    return t;
                }
                else
                    return default(T);
            }
        }

        private T ReadOne<T>(IDbCommand cd, Func<DataReaderCo, T> cbReader)
        {
            using (var dr = cd.LogAndExecuteReader(_SqlLogger))
            {
                var coReader = DatabaseProvider.CreateDataReaderCo(dr, DataCulture);
                if (dr.Read())
                {
                    var t = cbReader(coReader);
                    coReader.ThrowIfContinueReading();
                    return t;
                }
                else
                    return default(T);
            }
        }

        private List<T> ReadMany_AsList<T>(IDbCommand cd, TableProvider tbProvider)
        {
            using (var dr = cd.LogAndExecuteReader(_SqlLogger))
            {
                var coReader = DatabaseProvider.CreateDataReaderCo(dr, DataCulture);
                var ilReader = GetFastFactoryAndReader<T>(coReader, tbProvider);
                var list = new List<T>();
                while (dr.Read())
                    list.Add(ilReader.Invoke(coReader));
                return list;
            }
        }

        private List<T> ReadMany_AsList<T>(IDbCommand cd, Func<DataReaderCo, T> cbReader)
        {
            using (var dr = cd.LogAndExecuteReader(_SqlLogger))
            {
                var coReader = DatabaseProvider.CreateDataReaderCo(dr, DataCulture);
                var list = new List<T>();
                while (dr.Read())
                    list.Add(cbReader(coReader));
                return list;
            }
        }

        private IEnumerable<T> ReadMany_AsEnumerable<T>(IDbCommand cd, TableProvider tbProvider)
        {
            var dr = cd.LogAndExecuteReader(_SqlLogger);
            try
            {
                var coReader = DatabaseProvider.CreateDataReaderCo(dr, DataCulture);
                var ilReader = GetFastFactoryAndReader<T>(coReader, tbProvider);
                return new RowEnumerable<T>(this, cd, dr, ilReader, coReader);
            }
            catch
            {
                dr.Dispose();
                throw;
            }
        }

        private IEnumerable<T> ReadMany_AsEnumerable<T>(IDbCommand cd, Func<DataReaderCo, T> cbReader)
        {
            var dr = cd.LogAndExecuteReader(_SqlLogger);
            try
            {
                var coReader = DatabaseProvider.CreateDataReaderCo(dr, DataCulture);
                return new RowEnumerable<T>(this, cd, dr, cbReader, coReader);
            }
            catch
            {
                dr.Dispose();
                throw;
            }
        }

        /// <summary>
        /// The cache of fast dynamic data reader used to populate rows.
        /// These readers expect the row to be passed as input.
        /// </summary>
        private static readonly Quiver<DynamicMethod> __DataReaderQuiver1 = new Quiver<DynamicMethod>();
        /// <summary>
        /// The cache of fast dynamic data reader used to populate rows.
        /// These readers instantiate the rows.
        /// </summary>
        private static readonly Quiver<DynamicMethod> __DataReaderQuiver2 = new Quiver<DynamicMethod>();

        /// <summary>
        /// Returns a delegate to a method allowing to rapidly populate an existing row from a data source.
        /// </summary>
        /// <typeparam name="T">The affected row type.</typeparam>
        /// <param name="coReader">The data source.</param>
        /// <param name="tbProvider">The mapping between destination row and data source.</param>
        /// <returns>A delegate to the IL-generated method.</returns>
        private Action<T, DataReaderCo> GetFastReader<T>(DataReaderCo coReader, TableProvider tbProvider)
        {
            string key = coReader.GetSignature(tbProvider);
            var ilReaderMethod = __DataReaderQuiver1.GetArrow(key, () =>
            {
                _Logger.LogDebug($"Generating reader for {typeof(T)}... Signature: {key})");
                return MSILGenerator.CreateRowReader(tbProvider, coReader);
            });
            return ilReaderMethod.CreateDelegate(typeof(Action<T, DataReaderCo>)) as Action<T, DataReaderCo>;
        }

        /// <summary>
        /// Returns a delegate to a method allowing to rapidly create and populate a new row from a data source.
        /// </summary>
        /// <typeparam name="T">The affected row type.</typeparam>
        /// <param name="coReader">The data source.</param>
        /// <param name="tbProvider">The mapping between destination row and data source.</param>
        /// <returns>A delegate to the IL-generated method.</returns>
        private Func<DataReaderCo, T> GetFastFactoryAndReader<T>(DataReaderCo coReader, TableProvider tbProvider)
        {
            MethodInfo ilMethod;
            var rowType = typeof(T);
            if (RowTypeHelper.IsPlainObject(rowType))
            {
                //Don't rely on a dynamic method. Instead, use the hard-coded ReadRowAsExpandoObject static method.
                ilMethod = this.GetType().GetMethod("ReadRowAsExpandoObject", BindingFlags.Static | BindingFlags.NonPublic);
            }
            else if (RowTypeHelper.IsObjectArray(rowType))
            {
                //Don't rely on a dynamic method. Instead, use the hard-coded ReadRowAsObjectArray static method.
                ilMethod = this.GetType().GetMethod("ReadRowAsObjectArray", BindingFlags.Static | BindingFlags.NonPublic);
            }
            else if (RowTypeHelper.IsGenericValueTuple(rowType))
            {
                //The dynamic method is cached (thread-safe).
                string key = coReader.GetSignature(tbProvider);
                ilMethod = __DataReaderQuiver2.GetArrow(key, () =>
                {
                    _Logger.LogDebug($"Generating reader for {rowType}... Signature: {key})");
                    return MSILGenerator.CreateRowFactoryAndReaderForTuple(tbProvider, coReader);
                });
            }
            else
            {
                //The dynamic method is cached (thread-safe).
                string key = coReader.GetSignature(tbProvider);
                ilMethod = __DataReaderQuiver2.GetArrow(key, () =>
                {
                    _Logger.LogDebug($"Generating reader for {rowType}... Signature: {key})");
                    return MSILGenerator.CreateRowFactoryAndReaderForObject(tbProvider, coReader);
                });
            }
            return ilMethod.CreateDelegate(typeof(Func<DataReaderCo, T>)) as Func<DataReaderCo, T>;
        }

        /// <summary>
        /// Hard-coded method (not IL-generated) to read row data into a new <see cref="ExpandoObject"/>. This method is used when
        /// selecting data via dynamic or pure object type.
        /// </summary>
        /// <remarks>
        /// Even though this method is not referenced directly, it's accessed htrough Reflection via GetFactoryAndReader.
        /// DON'T DELETE IT!
        /// </remarks>
        private static object ReadRowAsExpandoObject(DataReaderCo adr)
        {
            var dyn = new ExpandoObject() as IDictionary<string, object>;
            for (int i = 0; i < adr.DataReader.FieldCount; i++)
            {
                //TODO: A fool-proof way to turn a column name into a valid .NET identifier.
                //Also, the issue of transforming the name may make it worth to create a dynamic method.
                string key = adr.DataReader.GetName(i)
                    .Replace(' ', '_')
                    .Replace('-', '_');
                dyn.Add(key, adr.GetObject(i));
            }
            return dyn;
        }

        /// <summary>
        /// Hard-coded method (not IL-generated) to read row data into a new object array. This method is used when
        /// selecting data via an object array.
        /// </summary>
        /// <remarks>
        /// Even though this method is not referenced directly, it's accessed htrough Reflection via GetFactoryAndReader.
        /// DON'T DELETE IT!
        /// </remarks>
        private static object[] ReadRowAsObjectArray(DataReaderCo adr)
        {
            var arr = new object[adr.DataReader.FieldCount];
            for (int i = 0; i < adr.DataReader.FieldCount; i++)
                arr[i] = adr.GetObject(i);
            return arr;
        }

        #endregion

        #region Execution Methods

        /// <summary>
        /// Executes a SQL statement.
        /// </summary>
        /// <param name="query">A parameterized SQL query to execute.</param>
        /// <param name="values">The parameterized query values.</param>
        public void Execute(string query, params object[] values)
        {
            Execute(Sql.Builder.Append(query, values));
        }

        /// <summary>
        /// Executes a SQL statement.
        /// </summary>
        /// <param name="sql">The SQL query to execute.</param>
        public void Execute(Sql sql)
        {
            ExecuteInOpenConnection((tn) =>
            {
                using (var cd = _Connection.CreateCommand())
                {
                    cd.Transaction = tn;
                    cd.CommandText = sql.GetCommandText(DatabaseProvider);
                    cd.CommandType = CommandType.Text;
                    object[] values = sql.GetCommandParameters();
                    if (values?.Length > 0)
                        DatabaseProvider.ParameterizeCommand(cd, values);
                    cd.LogAndExecuteNonQuery(_SqlLogger);
                }
            });
        }

        /// <summary>
        /// Executes a SQL statement and returns the first value of the first record in the first set.
        /// </summary>
        /// <param name="query">A parameterized SQL query to execute.</param>
        /// <param name="values">The parameterized query values.</param>
        /// <returns>The first value of the first record in the first returned set.</returns>
        public T ExecuteScalar<T>(string query, params object[] values)
        {
            return ExecuteScalar<T>(Sql.Builder.Append(query, values));
        }

        /// <summary>
        /// Executes a SQL statement and returns the first value of the first record in the first set.
        /// </summary>
        /// <param name="sql">The SQL query to execute.</param>
        /// <returns>The first value of the first record in the first returned set.</returns>
        public T ExecuteScalar<T>(Sql sql)
        {
            return ExecuteInOpenConnection<T>((tn) =>
            {
                using (var cd = _Connection.CreateCommand())
                {
                    cd.Transaction = tn;
                    cd.CommandText = sql.GetCommandText(DatabaseProvider);
                    cd.CommandType = CommandType.Text;
                    object[] values = sql.GetCommandParameters();
                    if (values?.Length > 0)
                        DatabaseProvider.ParameterizeCommand(cd, values);
                    object v = cd.LogAndExecuteScalar(_SqlLogger);
                    Type t = typeof(T);
                    if (v == null || Convert.IsDBNull(v))
                    {
                        //Make sure the requested type is assignable from null...
                        if (t.IsValueType && Nullable.GetUnderlyingType(t) == null)
                            throw new InvalidCastException();
                        return default(T);
                    }
                    else
                    {
                        return (T)Convert.ChangeType(v, Nullable.GetUnderlyingType(t) ?? t);
                    }
                }
            });
        }

        #endregion

        #region Transactions

        private Stack<IDbTransaction> _TransactionStack = new Stack<IDbTransaction>();

        protected IDbTransaction CurrentTransaction
        {
            get { return _TransactionStack.Count > 0 ? _TransactionStack.Peek() : null; }
        }

        public int BeginTransaction()
        {
            OpenConnection();
            var tn = _Connection.BeginTransaction();
            _TransactionStack.Push(tn);
            return _TransactionStack.Count;
        }

        public void CommitTransaction(int? tncount = null)
        {
            if (tncount.HasValue && tncount.Value != _TransactionStack.Count)
                throw new InvalidOperationException("Bad transaction count");
            var tn = _TransactionStack.Pop();
            tn.Commit();
            CloseConnection();
        }

        public void RollbackTransaction(int? tncount = null)
        {
            if (tncount.HasValue && tncount.Value != _TransactionStack.Count)
                throw new InvalidOperationException("Bad transaction count");
            var tn = _TransactionStack.Pop();
            tn.Rollback();
            CloseConnection();
        }

        #endregion

        #region CRUD Operations

        public int Insert<T>(T row)
        {
            var rowmgr = RowManager.GetManager(typeof(T), DatabaseProvider);
            void refreshDelegate(IDbTransaction tn, object[] pk)
            {
                using (var cd = _Connection.CreateCommand())
                {
                    cd.Transaction = tn;
                    rowmgr.InitializeCommandForSelectByPK(cd, pk);
                    using (var dr = cd.LogAndExecuteReader(_SqlLogger))
                    {
                        var coReader = DatabaseProvider.CreateDataReaderCo(dr, DataCulture);
                        var ilReader = GetFastReader<T>(coReader, rowmgr.TableProvider);
                        if (dr.Read())
                        {
                            ilReader.Invoke(row, coReader);
                            Debug.Assert(!dr.Read());
                        }
                    }
                }
            }
            rowmgr.PrepareForInsert(row, DataTimeZone.Now(), UserName);
            return ExecuteInOpenConnection<int>((tn) =>
            {
                using (var cd = _Connection.CreateCommand())
                {
                    cd.Transaction = tn;
                    var ppks = rowmgr.InitializeCommandForInsert(cd, row);
                    int insertCount = cd.LogAndExecuteNonQuery(_SqlLogger);
                    Debug.Assert(insertCount < 0 || insertCount == 1);
                    object[] pk = ppks?.Length > 0 ? ppks.Select(x => x.Value).ToArray() : null;
                    if ((rowmgr.TableProvider.RefreshOptions & RefreshOptions.AfterInsert) != 0)
                    {
                        Debug.Assert(ppks?.Length > 0 && pk != null); //Already enforced. See TableInfo constructor.
                        refreshDelegate(tn, pk);
                    }
                    else if (pk != null)
                        rowmgr.SetPrimaryKey(row, pk);
                    return insertCount;
                }
            });
        }

        public int Update<T>(T row)
        {
            var rowmgr = RowManager.GetManager(typeof(T), DatabaseProvider);
            void refreshDelegate(IDbTransaction tn)
            {
                using (var cd = _Connection.CreateCommand())
                {
                    cd.Transaction = tn;
                    object[] pk = rowmgr.GetPrimaryKey(row);
                    rowmgr.InitializeCommandForSelectByPK(cd, pk);
#if DEBUG && false
                    string cmd = cd.CommandText;
                    var ps = cd.Parameters;
#endif
                    using (var dr = cd.LogAndExecuteReader(_SqlLogger))
                    {
                        var coReader = DatabaseProvider.CreateDataReaderCo(dr, DataCulture);
                        var ilReader = GetFastReader<T>(coReader, rowmgr.TableProvider);
                        if (dr.Read())
                        {
                            ilReader.Invoke(row, coReader);
                            Debug.Assert(!dr.Read());
                        }
                    }
                }
            }
            rowmgr.PrepareForUpdate(row, DataTimeZone.Now(), UserName);
            return ExecuteInOpenConnection<int>((tn) =>
            {
                using (var cd = _Connection.CreateCommand())
                {
                    cd.Transaction = tn;
                    rowmgr.InitializeCommandForUpdate(cd, row);
                    int updateCount = cd.LogAndExecuteNonQuery(_SqlLogger);
                    if ((rowmgr.TableProvider.RefreshOptions & RefreshOptions.AfterUpdate) != 0)
                        refreshDelegate(tn);
                    return updateCount;
                }
            });
        }

        public int Delete<T>(object[] pk)
        {
            var rowmgr = RowManager.GetManager(typeof(T), DatabaseProvider);
            return ExecuteInOpenConnection<int>((tn) =>
            {
                using (var cd = _Connection.CreateCommand())
                {
                    cd.Transaction = tn;
                    rowmgr.InitializeCommandForDelete(cd, pk);
#if DEBUG && false
                    string cmd = cd.CommandText;
                    var ps = cd.Parameters;
#endif
                    int deleteCount = cd.LogAndExecuteNonQuery(_SqlLogger);
                    Debug.Assert(deleteCount == 0 || deleteCount == 1);
                    return deleteCount;
                }
            });
        }

        public int Delete<T>(T row)
        {
            var rowmgr = RowManager.GetManager(typeof(T), DatabaseProvider);
            return ExecuteInOpenConnection<int>((tn) =>
            {
                using (var cd = _Connection.CreateCommand())
                {
                    cd.Transaction = tn;
                    object[] pk = rowmgr.GetPrimaryKey(row);
                    rowmgr.InitializeCommandForDelete(cd, pk);
#if DEBUG && false
                    string cmd = cd.CommandText;
                    var ps = cd.Parameters;
#endif
                    int deleteCount = cd.LogAndExecuteNonQuery(_SqlLogger);
                    Debug.Assert(deleteCount == 0 || deleteCount == 1);
                    return deleteCount;
                }
            });
        }

        /// <summary>
        /// Deletes all the rows in a table.
        /// </summary>
        /// <typeparam name="T">The type representing the table affected by the operation.</typeparam>
        /// <returns>The number of rows affected.</returns>
        public int DeleteAll<T>()
        {
            var rowmgr = RowManager.GetManager(typeof(T), DatabaseProvider);
            return ExecuteInOpenConnection((tn) =>
            {
                using (var cd = _Connection.CreateCommand())
                {
                    cd.Transaction = tn;
                    rowmgr.InitializeCommandForDeleteAll(cd);
                    return cd.LogAndExecuteNonQuery(_SqlLogger);
                }
            });
        }

        /// <summary>
        /// Truncates a table.
        /// </summary>
        /// <typeparam name="T">The type representing the table affected by the operation.</typeparam>
        public void Truncate<T>()
        {
            var rowmgr = RowManager.GetManager(typeof(T), DatabaseProvider);
            ExecuteInOpenConnection((tn) =>
            {
                using (var cd = _Connection.CreateCommand())
                {
                    cd.Transaction = tn;
                    rowmgr.InitializeCommandForTruncate(cd);
                    cd.LogAndExecuteNonQuery(_SqlLogger);
                }
            });
        }

        #endregion
    }
}