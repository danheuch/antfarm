﻿using System;
using System.Data;
using System.Diagnostics;
using System.Reflection;

namespace AntFarm.ORM.Providers
{
    public class ColumnProvider
    {
        public ColumnProvider(TableProvider tbProvider, PropertyInfo pinfo, ColumnAttribute colAttribute, MapperAttribute mapAttribute)
        {
            TableProvider = tbProvider;

            Accessor = pinfo;
            ColumnName = (colAttribute?.ColumnName) ?? pinfo.Name;
            ColumnType = pinfo.PropertyType.UnderlyingSystemType;
            NullableColumnType = Nullable.GetUnderlyingType(ColumnType);
            if (colAttribute != null)
            {
                Debug.Assert(colAttribute.IsColumn);
                AuditOption = colAttribute.AuditOption;
                IsAutoNumber = colAttribute.IsAutoNumber;
                IsPrimaryKey = colAttribute.IsPrimaryKey;
                IsReadOnly = colAttribute.IsReadOnly;
                SequenceName = colAttribute.SequenceName;
            }
            DBType = mapAttribute?.DBType ?? tbProvider.DatabaseProvider.GetDbType(ColumnType);
            IsNullable = mapAttribute?.IsNullable == true || ColumnType.IsAssignableFromNull();
            if (mapAttribute != null)
            {
                Size = mapAttribute.Size;
                Precision = mapAttribute.Precision;
                Scale = mapAttribute.Scale;
            }
        }

        public TableProvider TableProvider { get; }
        public DatabaseProvider DatabaseProvider => TableProvider.DatabaseProvider;

        /// <summary>
        /// The column accessor (get and set methods of the property).
        /// </summary>
        public PropertyInfo Accessor { get; }
        /// <summary>
        /// The column name.
        /// </summary>
        public string ColumnName { get; }
        /// <summary>
        /// The .NET column type, as declared (e.g., string, int, or int?).
        /// </summary>
        public Type ColumnType { get; }
        /// <summary>
        /// If ColumnType is nullable (e.g., int?), then the underlying nullable type (int).
        /// Otherwise, null.
        /// </summary>
        public Type NullableColumnType { get; }

        public AuditColumnOption AuditOption { get; protected set; }
        public bool IsAutoNumber { get; protected set; }
        public bool IsPrimaryKey { get; protected set; }
        public bool IsReadOnly { get; protected set; }
        public string SequenceName { get; protected set; }

        public DbType DBType { get; protected set; }
        public bool IsNullable { get; protected set; }
        public int? Size { get; protected set; }
        public byte? Precision { get; protected set; }
        public byte? Scale { get; protected set; }

        public override string ToString() => ColumnName;

        public virtual IDbDataParameter CreateCommandParameter(IDbCommand cd, string pname, object pvalue)
        {
            //IDbDataParameter
            //if (IsReadOnly)
            //    throw new InvalidOperationException($"Unable to create an input command parameter for column {pname} because it is read-only.");
            var p = cd.CreateParameter();
            p.Direction = ParameterDirection.Input;
            p.ParameterName = pname;
            p.DbType = this.DBType;
            if (this.Precision.HasValue)
                p.Precision = this.Precision.Value;
            if (this.Scale.HasValue)
                p.Scale = this.Scale.Value;
            if (this.Size.HasValue)
                p.Size = this.Size.Value;
            if (pvalue != null)
                p.Value = pvalue;
            else if (!this.IsNullable)
                throw new InvalidOperationException($"Column {pname} is marked as non-nullable, but the input parameter value is null.");
            else
                p.Value = DBNull.Value;
            return p;
        }
    }
}