﻿using AntFarm.Core;
using AntFarm.ORM.SQL.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;

namespace AntFarm.ORM.Providers
{
    public class TableProvider
    {
        public virtual ColumnProvider CreateColumnProvider(PropertyInfo pinfo, ColumnAttribute colAttribute, MapperAttribute mapAttribute)
            => new ColumnProvider(this, pinfo, colAttribute, mapAttribute);

        public TableProvider(DatabaseProvider dbProvider, Type rowType)
        {
            DatabaseProvider = dbProvider;
            RowType = rowType;

            if (RowTypeHelper.PossiblyDescribesATable(rowType))
            {
                var tableAttribute = rowType.GetCustomAttribute<TableAttribute>();
                if (tableAttribute != null)
                {
                    TableName = tableAttribute.TableName ?? rowType.Name;
                    SchemaName = tableAttribute.SchemaName;
                    DatabaseName = tableAttribute.DatabaseName;
                    RefreshOptions = tableAttribute.RefreshOptions;
                }
                else
                {
                    TableName = rowType.Name;
                }

                var explicitColumnsAttribute = rowType.GetCustomAttribute<ExplicitColumnsAttribute>();
                bool explicitColumns = (explicitColumnsAttribute?.Value) ?? false;
                foreach (var pinfo in rowType.GetProperties())
                {
                    var colAttribute = pinfo.GetCustomAttribute<ColumnAttribute>();
                    bool isColumn = (colAttribute == null && !explicitColumns) || (colAttribute?.IsColumn == true);
                    if (isColumn)
                    {
                        if (!pinfo.CanRead || !pinfo.CanWrite)
                        {
                            if (explicitColumns)
                            {
                                Debug.Assert(colAttribute != null);
                                throw new Exception("Missing getter or setter on column property");
                            }
                            continue; //explicitColumns is false, so we just skip this property.
                        }
                        var mapAttribute = pinfo.GetCustomAttribute<MapperAttribute>();
                        var colInfo = this.CreateColumnProvider(pinfo, colAttribute, mapAttribute);
                        if (colInfo.IsPrimaryKey)
                        {
                            if (!pinfo.PropertyType.IsValueType && pinfo.PropertyType != typeof(string))
                                throw new Exception("Primary key property not of value or string type");
                            HasPrimaryKey = true;
                        }
                        Columns.Add(colInfo);
                    }
                }

                if (HasPrimaryKey)
                {
                    var accessors = PrimaryKeyColumns.Select(x => x.Accessor).ToArray();
                    var getter = MSILGenerator.CreateMultiPropertyGetter(accessors);
                    PrimaryKeyGetter = getter.CreateDelegate(typeof(Func<object, object[]>)) as Func<object, object[]>;
                    var setter = MSILGenerator.CreateMultiPropertySetter(accessors);
                    PrimaryKeySetter = setter.CreateDelegate(typeof(Action<object, object[]>)) as Action<object, object[]>;
                }
                else if (RefreshOptions != RefreshOptions.None)
                {
                    throw new InvalidOperationException("Primary key missing on table with after-insert or after-update refresh set");
                }
            }
        }

        public Guid GUID { get; } = Guid.NewGuid();

        public DatabaseProvider DatabaseProvider { get; }
        public AntConfig Config => DatabaseProvider.Config;
        public Type RowType { get; }

        public string TableName { get; }
        public string SchemaName { get; }
        public string DatabaseName { get; }

        public RefreshOptions RefreshOptions { get; }

        public List<ColumnProvider> Columns { get; } = new List<ColumnProvider>();

        public bool HasPrimaryKey { get; }

        public ColumnProvider[] PrimaryKeyColumns => Columns.Where(x => x.IsPrimaryKey).ToArray();

        public void ThrowIfNoPrimaryKey()
        {
            if (!HasPrimaryKey)
                throw new InvalidOperationException($"Invalid operation on row type {RowType}: the primary key is not defined.");
        }

        public void ThrowIfNoTableName()
        {
            if (TableName == null)
                throw new InvalidOperationException($"Invalid operation on row type {RowType}: the table name is not defined.");
        }

        public override string ToString() => TableName;

        #region Delegates

        Action<BinaryWriterEx, object> __Serializer;
        Func<BinaryReaderEx, object> __Deserializer;
        Action<object, DateTimeOffset, string> __InsertPreparer;
        Action<object, DateTimeOffset, string> __UpdatePreparer;

        public Func<object, object[]> PrimaryKeyGetter { get; }

        public Action<object, object[]> PrimaryKeySetter { get; }

        public Action<BinaryWriterEx, object> Serializer
        {
            get
            {
                if (__Serializer == null)
                {
                    var ilm = MSILGenerator.CreateRowSerializer(this.RowType);
                    __Serializer = ilm.CreateDelegate(typeof(Action<BinaryWriterEx, object>)) as Action<BinaryWriterEx, object>;
                }
                return __Serializer;
            }
        }

        public Func<BinaryReaderEx, object> Deserializer
        {
            get
            {
                if (__Deserializer == null)
                {
                    var ilm = MSILGenerator.CreateRowDeserializer(this.RowType);
                    __Deserializer = ilm.CreateDelegate(typeof(Func<BinaryReaderEx, object>)) as Func<BinaryReaderEx, object>;
                }
                return __Deserializer;
            }
        }

        public Action<object, DateTimeOffset, string> InsertPreparer
        {
            get
            {
                if (__InsertPreparer == null)
                {
                    var ilm = MSILGenerator.CreateRowInsertPreparer(this);
                    __InsertPreparer = ilm.CreateDelegate(typeof(Action<object, DateTimeOffset, string>)) as Action<object, DateTimeOffset, string>;
                }
                return __InsertPreparer;
            }
        }


        public Action<object, DateTimeOffset, string> UpdatePreparer
        {
            get
            {
                if (__UpdatePreparer == null)
                {
                    var ilm = MSILGenerator.CreateRowUpdatePreparer(this);
                    __UpdatePreparer = ilm.CreateDelegate(typeof(Action<object, DateTimeOffset, string>)) as Action<object, DateTimeOffset, string>;
                }
                return __UpdatePreparer;
            }
        }

        #endregion

        /// <summary>
        /// Ensures that the given identifier is properly delimited, if needed (e.g., contains spaces).
        /// </summary>
        /// <param name="identifier">The non-delimited identifier.</param>
        /// <param name="forceDelimiters">Whether to always delimit the identifier (true), or use internal rules to decide (false).</param>
        /// <returns>The given identifier, properly delimited if needed.</returns>
        protected virtual string EnsureDelimitedIdentifier(string identifier, bool forceDelimiters = false)
        {
            if (string.IsNullOrEmpty(identifier))
                return null;
#if true
            identifier = $"{DatabaseProvider.SQLIdentifierDelimiterBegin}{identifier}{DatabaseProvider.SQLIdentifierDelimiterEnd}";
#else
            if (DatabaseProvider.ReservedWords.Contains(identifier))
                forceDelimiters = true;
            if (forceDelimiters || !DatabaseProvider.UnquotedIdentifierRegex.IsMatch(identifier))
                identifier = $"{DatabaseProvider.SQLIdentifierDelimiterBegin}{identifier}{DatabaseProvider.SQLIdentifierDelimiterEnd}";
#endif
            return identifier;
        }

        /// <summary>
        /// Prepends the database and schema (if any) to the table name, to return the
        /// full table identifier. Ensures that all name parts are properly quoted, if needed.
        /// </summary>
        public virtual string GetTableName()
        {
            var sb = new StringBuilder();
            string dname = EnsureDelimitedIdentifier(this.DatabaseName);
            if (dname != null)
                sb.Append(dname);
            string sname = EnsureDelimitedIdentifier(this.SchemaName);
            if (sname != null)
                sb.Append(sb.Length > 0 ? "." : "").Append(sname);
            string tname = EnsureDelimitedIdentifier(this.TableName);
            Debug.Assert(tname != null);
            sb.Append(sb.Length > 0 ? "." : "").Append(tname);
            return sb.ToString();
        }

        /// <summary>
        /// Returns the column name after ensuring that it is properly quoted,
        /// if needed.
        /// </summary>
        public virtual string GetColumnName(ColumnProvider colInfo)
            => EnsureDelimitedIdentifier(colInfo.ColumnName ?? throw new InvalidOperationException("Oops... The column name is missing."));

        /// <summary>
        /// Returns the column's sequence name after ensuring that it is properly quoted,
        /// if needed. Throws an exception if no sequence name is defined.
        /// </summary>
        public virtual string GetSequenceName(ColumnProvider colInfo)
            => EnsureDelimitedIdentifier(colInfo.SequenceName ?? throw new InvalidOperationException("Oops... The sequence name is missing."));

        public virtual string GetCommandTextForSelectAll()
        {
            var sb = new StringBuilder();
            sb.Append("SELECT ");
            var selcols = this.Columns;
            for (int i = 0; i < selcols.Count; i++)
                sb.Append(i > 0 ? "," : "").Append(GetColumnName(selcols[i]));
            sb
                .Append(" FROM ")
                .Append(GetTableName());
            return sb.ToString();
        }
        public virtual string GetCommandTextForSelectByPK()
        {
            this.ThrowIfNoPrimaryKey();
            var sb = new StringBuilder();
            sb.Append("SELECT ");
            var selcols = this.Columns;
            for (int i = 0; i < selcols.Count; i++)
                sb.Append(i > 0 ? "," : "").Append(GetColumnName(selcols[i]));
            sb
                .Append(" FROM ")
                .Append(GetTableName())
                .Append(" WHERE ");
            var pkcols = this.PrimaryKeyColumns;
            Debug.Assert(pkcols.Length > 0);
            for (int i = 0; i < pkcols.Length; i++)
            {
                sb
                    .Append(i > 0 ? " AND " : "")
                    .Append(GetColumnName(pkcols[i]))
                    .Append("=").Append(DatabaseProvider.SQLParameterPrefix).Append("PK").Append(i);
            }
            return sb.ToString();
        }
        public virtual string GetCommandTextForInsert() => throw new NotImplementedException();
        public virtual string GetCommandTextForUpdate()
        {
            this.ThrowIfNoPrimaryKey();
            var sb = new StringBuilder();
            var updcols = this.Columns.Where(x => !x.IsReadOnly && !x.IsPrimaryKey).ToList();
            sb
                .Append("UPDATE ")
                .Append(GetTableName())
                .Append(" SET ");
            for (int i = 0; i < updcols.Count; i++)
            {
                sb
                    .Append(i > 0 ? "," : "")
                    .Append(GetColumnName(updcols[i]))
                    .Append("=")
                    .Append(DatabaseProvider.SQLParameterPrefix).Append("V").Append(i);
            }
            sb.Append(" WHERE ");
            var pkcols = this.PrimaryKeyColumns;
            Debug.Assert(pkcols.Length > 0);
            for (int i = 0; i < pkcols.Length; i++)
            {
                sb
                    .Append(i > 0 ? " AND " : "")
                    .Append(GetColumnName(pkcols[i]))
                    .Append("=")
                    .Append(DatabaseProvider.SQLParameterPrefix).Append("PK").Append(i);
            }
            return sb.ToString();
        }
        public virtual string GetCommandTextForTruncate()
        {
            var sb = new StringBuilder();
            sb
               .Append("TRUNCATE TABLE ")
               .Append(GetTableName());
            return sb.ToString();
        }
        public virtual string GetCommandTextForDelete()
        {
            this.ThrowIfNoPrimaryKey();
            var sb = new StringBuilder();
            sb
               .Append("DELETE FROM ")
               .Append(GetTableName())
               .Append(" WHERE ");
            var pkcols = this.PrimaryKeyColumns;
            Debug.Assert(pkcols.Length > 0);
            for (int i = 0; i < pkcols.Length; i++)
            {
                sb
                    .Append(i > 0 ? " AND " : "")
                    .Append(GetColumnName(pkcols[i]))
                    .Append("=")
                    .Append(DatabaseProvider.SQLParameterPrefix).Append("PK").Append(i);
            }
            return sb.ToString();
        }
        public virtual string GetCommandTextForDeleteAll()
        {
            var sb = new StringBuilder();
            sb
               .Append("DELETE FROM ")
               .Append(GetTableName());
            return sb.ToString();
        }
        public virtual string GetCommandTextForSelectAllWhere(SqlExpression whereClause, Func<object, DbType, string> emitParameter = null)
        {
            var sb = new StringBuilder();
            sb.Append("SELECT ");
            var selcols = this.Columns;
            for (int i = 0; i < selcols.Count; i++)
                sb.Append(i > 0 ? "," : "").Append(GetColumnName(selcols[i]));
            sb
                .Append(" FROM ")
                .Append(GetTableName())
                .Append(" WHERE ")
                .Append(EmitSqlFromExpression(whereClause, emitParameter));
            return sb.ToString();
        }

        public virtual void ParameterizeCommandForSelectByPK(IDbCommand cd, object[] pk)
        {
            var pkcols = this.PrimaryKeyColumns;
            Debug.Assert(pkcols.Length == pk.Length);
            for (int i = 0; i < pkcols.Length; i++)
            {
                var p = pkcols[i].CreateCommandParameter(cd, $"{DatabaseProvider.SQLParameterPrefix}PK{i}", pk[i]);
                cd.Parameters.Add(p);
            }
        }
        public virtual IDbDataParameter[] ParameterizeCommandForInsert(IDbCommand cd, object row) => throw new NotImplementedException();
        public virtual void ParameterizeCommandForUpdate(IDbCommand cd, object row)
        {
            var updateColumns = this.Columns.Where(x => !x.IsReadOnly && !x.IsPrimaryKey).ToList();
            for (int i = 0; i < updateColumns.Count; i++)
            {
                var colInfo = updateColumns[i];
                object value = colInfo.Accessor.GetValue(row);
                var p = colInfo.CreateCommandParameter(cd, $"{DatabaseProvider.SQLParameterPrefix}V{i}", value);
                cd.Parameters.Add(p);
            }
            var pkcols = this.PrimaryKeyColumns;
            for (int i = 0; i < pkcols.Length; i++)
            {
                var p = pkcols[i].CreateCommandParameter(cd, $"{DatabaseProvider.SQLParameterPrefix}PK{i}", pkcols[i].Accessor.GetValue(row));
                cd.Parameters.Add(p);
            }
        }
        public virtual void ParameterizeCommandForDelete(IDbCommand cd, object[] pk)
        {
            var pkcols = this.PrimaryKeyColumns;
            Debug.Assert(pkcols.Length == pk.Length);
            for (int i = 0; i < pkcols.Length; i++)
            {
                var p = pkcols[i].CreateCommandParameter(cd, $"{DatabaseProvider.SQLParameterPrefix}PK{i}", pk[i]);
                cd.Parameters.Add(p);
            }
        }

        protected virtual string EmitSqlFromExpression(SqlExpression expression, Func<object, DbType, string> emitParameter)
        {
            var sb = new StringBuilder();
            switch (expression)
            {
                case SqlNull n:
                    {
                        sb.Append("NULL");
                    }
                    break;
                case SqlObject o:
                    throw new NotSupportedException();
                case SqlConstant c:
                    {
                        var dbType = this.DatabaseProvider.GetDbType(c.Value.GetType());
                        if (emitParameter != null)
                        {
                            string pname = emitParameter.Invoke(c.Value, dbType);
                            sb.Append(pname);
                        }
                        else
                        {
                            #region Non-Parameterized output of the value - This is for debugging
                            var type = c.Value.GetType();
                            var typeCode = Type.GetTypeCode(type);
                            switch (typeCode)
                            {
                                case TypeCode.Boolean:
                                    {
                                        string strValue = (bool)c.Value ? "1" : "0";
                                        sb.Append(strValue);
                                    }
                                    break;
                                case TypeCode.Char:
                                    {
                                        char charValue = (char)c.Value;
                                        if (charValue == '\'')
                                            sb.Append("CHAR(39)");
                                        else
                                            sb.Append("'").Append(charValue).Append("'");
                                    }
                                    break;
                                case TypeCode.String:
                                    {
                                        string strValue = (string)c.Value;
                                        sb.Append("'").Append(strValue.Replace("'", "''")).Append("'");
                                    }
                                    break;
                                case TypeCode.DateTime:
                                    {
                                        var dtValue = (DateTime)c.Value;
                                        string format = "dd-MMM-yyyy HH:mm:ss";
                                        string strValue = dtValue.ToString(format, CultureInfo.InvariantCulture);
                                        sb.Append("\"").Append(strValue).Append("\"");
                                    }
                                    break;
                                case TypeCode.Byte:
                                case TypeCode.Decimal:
                                case TypeCode.Double:
                                case TypeCode.Int16:
                                case TypeCode.Int32:
                                case TypeCode.Int64:
                                case TypeCode.SByte:
                                case TypeCode.UInt16:
                                case TypeCode.UInt32:
                                case TypeCode.UInt64:
                                    {
                                        string strValue = c.Value.ToString();
                                        sb.Append(strValue);
                                    }
                                    break;
                                default:
                                    throw new NotSupportedException();
                            }
                            #endregion
                        }
                    }
                    break;
                case SqlVariable v:
                    {
                        sb.Append(GetColumnName(v.ColumnProvider));
                    }
                    break;
                case SqlUnaryOperation u:
                    {
                        switch (u.Operator)
                        {
                            case SqlUnaryOperator.Plus:
                                sb.Append("(+").Append(EmitSqlFromExpression(u.Operand, emitParameter)).Append(")");
                                break;
                            case SqlUnaryOperator.Minus:
                                sb.Append("(-").Append(EmitSqlFromExpression(u.Operand, emitParameter)).Append(")");
                                break;
                            case SqlUnaryOperator.Not:
                                sb.Append("(NOT ").Append(EmitSqlFromExpression(u.Operand, emitParameter)).Append(")");
                                break;
                            default:
                                throw new NotSupportedException();
                        }
                    }
                    break;
                case SqlBinaryOperation b:
                    {
                        switch (b.Operator)
                        {
                            case SqlBinaryOperator.Equal:
                                sb
                                    .Append("(")
                                    .Append(EmitSqlFromExpression(b.LeftOperand, emitParameter))
                                    .Append(b.RightOperand.IsDbNull ? " IS " : " = ")
                                    .Append(EmitSqlFromExpression(b.RightOperand, emitParameter))
                                    .Append(")");
                                break;
                            case SqlBinaryOperator.NotEqual:
                                sb
                                    .Append("(")
                                    .Append(EmitSqlFromExpression(b.LeftOperand, emitParameter))
                                    .Append(b.RightOperand.IsDbNull ? " IS NOT " : " <> ")
                                    .Append(EmitSqlFromExpression(b.RightOperand, emitParameter))
                                    .Append(")");
                                break;
                            case SqlBinaryOperator.Smaller:
                                sb
                                    .Append("(")
                                    .Append(EmitSqlFromExpression(b.LeftOperand, emitParameter))
                                    .Append(" < ")
                                    .Append(EmitSqlFromExpression(b.RightOperand, emitParameter))
                                    .Append(")");
                                break;
                            case SqlBinaryOperator.SmallerOrEqual:
                                sb
                                    .Append("(")
                                    .Append(EmitSqlFromExpression(b.LeftOperand, emitParameter))
                                    .Append(" <= ")
                                    .Append(EmitSqlFromExpression(b.RightOperand, emitParameter))
                                    .Append(")");
                                break;
                            case SqlBinaryOperator.Greater:
                                sb
                                    .Append("(")
                                    .Append(EmitSqlFromExpression(b.LeftOperand, emitParameter))
                                    .Append(" > ")
                                    .Append(EmitSqlFromExpression(b.RightOperand, emitParameter))
                                    .Append(")");
                                break;
                            case SqlBinaryOperator.GreaterOrEqual:
                                sb
                                    .Append("(")
                                    .Append(EmitSqlFromExpression(b.LeftOperand, emitParameter))
                                    .Append(" >= ")
                                    .Append(EmitSqlFromExpression(b.RightOperand, emitParameter))
                                    .Append(")");
                                break;
                            case SqlBinaryOperator.And:
                                sb
                                    .Append("(")
                                    .Append(EmitSqlFromExpression(b.LeftOperand, emitParameter))
                                    .Append(" AND ")
                                    .Append(EmitSqlFromExpression(b.RightOperand, emitParameter))
                                    .Append(")");
                                break;
                            case SqlBinaryOperator.Or:
                                sb
                                    .Append("(")
                                    .Append(EmitSqlFromExpression(b.LeftOperand, emitParameter))
                                    .Append(" OR ")
                                    .Append(EmitSqlFromExpression(b.RightOperand, emitParameter))
                                    .Append(")");
                                break;
                            default:
                                throw new NotSupportedException();
                        }
                    }
                    break;
            }
            return sb.ToString();
        }
    }
}
