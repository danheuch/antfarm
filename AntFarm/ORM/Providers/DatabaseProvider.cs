﻿using AntFarm.Core;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Globalization;
using System.Text.RegularExpressions;

namespace AntFarm.ORM.Providers
{
#if DEBUG //The base class is useful for some basic unit tests that don't involve access to the data or other provider-specific features.
    public class DatabaseProvider
#else
    public abstract class DatabaseProvider
#endif
    {
        public virtual char SQLIdentifierDelimiterBegin => '"';
        public virtual char SQLIdentifierDelimiterEnd => '"';
        public virtual char SQLStatementSeparator => ';';
        public virtual char SQLParameterPrefix => '?';
        /// <summary>
        /// The pattern for an unquoted identifier. Must start with a letter, followed by only
        /// letters and numbers. Any space or special character will require quoting.
        /// </summary>
        public virtual string UnquotedIdentifierPattern => "^[A-Za-z_][A-Za-z0-9_]*$";

        static DatabaseProvider()
        {
            ////TODO: List all the SQL reserved words that can cause issues with identifiers.
            ////That, or devise a strategy around the issue of quoting the identifiers. Always
            ////bracketing with quotes (or whatever provider-specific character) does not seem 
            ////to be the right approach, because of at least Oracle that makes the matching
            ////case-sensitive.
            ////Here is a start, but the list will obviously be much longer...
            //ReservedWords.Add("name");
            //ReservedWords.Add("numeric");
        }

        public DatabaseProvider(AntConfig config)
        {
            Config = config;
            UnquotedIdentifierRegex = new Regex(UnquotedIdentifierPattern);
        }

        /// <summary>
        /// Configuration information controlling various aspects of the provider.
        /// </summary>
        public AntConfig Config { get; }

        /// <summary>
        /// The pattern matcher for an unquoted identifier. Must start with a letter, followed by only
        /// letters and numbers. Any space or special character will require quoting.
        /// </summary>
        public Regex UnquotedIdentifierRegex { get; }

#if DEBUG
        public virtual DbConnection CreateConnection() => throw new NotImplementedException();
#else
        public abstract DbConnection CreateConnection();
#endif

        //public static HashSet<string> ReservedWords { get; } = new HashSet<string>(StringComparer.OrdinalIgnoreCase);

        public virtual TableProvider CreateTableProvider(Type rowType) => new TableProvider(this, rowType);

        public virtual void ParameterizeCommand(IDbCommand cd, params object[] values)
        {
            for (int i = 0; i < values.Length; i++)
            {
                var p = CreateCommandParameter(cd, $"{SQLParameterPrefix}{i}", values[i], values[i] == null ? typeof(object) : values[i].GetType());
                cd.Parameters.Add(p);
            }
        }

        public virtual void ParameterizeCommand(IDbCommand cd, IEnumerable<(string pname, object pvalue, DbType ptype)> plist)
        {
            foreach (var (pname, pvalue, ptype) in plist)
            {
                var p = CreateCommandParameter(cd, pname, pvalue, ptype);
                cd.Parameters.Add(p);
            }
        }

        public virtual IDbDataParameter CreateCommandParameter(IDbCommand cd, string pname, object pvalue, Type dataType)
        {
            var dbType = GetDbType(dataType);
            return CreateCommandParameter(cd, pname, pvalue, dbType);
        }

        protected virtual IDbDataParameter CreateCommandParameter(IDbCommand cd, string pname, object pvalue, DbType dbType)
        {
            //IDbDataParameter
            var p = cd.CreateParameter();
            p.DbType = dbType;
            p.Direction = ParameterDirection.Input;
            p.ParameterName = pname;
            //Precision, Scale and Size apparently not needed for input parameters.
            //TODO: make sure that is the case and that precision is not lost.
            //SourceColumn and SourceVersion are not applicable (they are used by DataSets).
            p.Value = pvalue ?? DBNull.Value;
            return p;
        }

        protected static Dictionary<Type, DbType> DBTypeMap { get; } = new Dictionary<Type, DbType>();

        /// <summary>
        /// Evaluates the <see cref="DbType"/> that best represents a given .NET type.
        /// </summary>
        /// <param name="type">The given .NET type. If nullable (e.g., int?), the underlying non-nullable type (e.g., int) is what is evaluated.</param>
        /// <returns>A matching <see cref="DbType"/>, or <see cref="DbType.Object"/> if no better one can be found.</returns>
        public virtual DbType GetDbType(Type type)
        {
            if (!DBTypeMap.TryGetValue(type, out DbType dbType))
                lock (DBTypeMap)
                    if (!DBTypeMap.TryGetValue(type, out dbType))
                    {
                        dbType = MapNet2DbType(type);
                        DBTypeMap[type] = dbType;
                    }
            return dbType;
        }

        protected virtual DbType MapNet2DbType(Type type)
        {
            Type underlyingType = Nullable.GetUnderlyingType(type) ?? type;
            switch (underlyingType)
            {
                #region Resources
                //See https://docs.microsoft.com/en-us/sql/t-sql/data-types/data-types-transact-sql
                //See https://docs.microsoft.com/en-us/dotnet/framework/data/adonet/sql-server-data-type-mappings
                //Dapper map - See https://stackoverflow.com/questions/7952142/how-to-resolve-system-type-to-system-data-dbtype
                //map[typeof(bool)] = DbType.Boolean;
                //map[typeof(byte)] = DbType.Byte;
                //map[typeof(sbyte)] = DbType.SByte;
                //map[typeof(short)] = DbType.Int16;
                //map[typeof(ushort)] = DbType.UInt16;
                //map[typeof(int)] = DbType.Int32;
                //map[typeof(uint)] = DbType.UInt32;
                //map[typeof(long)] = DbType.Int64;
                //map[typeof(ulong)] = DbType.UInt64;
                //map[typeof(float)] = DbType.Single;
                //map[typeof(double)] = DbType.Double;
                //map[typeof(decimal)] = DbType.Decimal;
                //map[typeof(char)] = DbType.StringFixedLength;
                //map[typeof(string)] = DbType.String;
                //map[typeof(DateTime)] = DbType.DateTime;
                //map[typeof(DateTimeOffset)] = DbType.DateTimeOffset;
                //map[typeof(Guid)] = DbType.Guid;
                //map[typeof(byte[])] = DbType.Binary;
                //map[typeof(System.Data.Linq.Binary)] = DbType.Binary;
                #endregion

                case Type vtype when vtype == typeof(bool): return DbType.Boolean;

                case Type vtype when vtype == typeof(byte): return DbType.Byte;
                case Type vtype when vtype == typeof(sbyte): return DbType.SByte;

                case Type vtype when vtype == typeof(short): return DbType.Int16;
                case Type vtype when vtype == typeof(ushort): return DbType.UInt16; //Not supported by Sql Server!
                case Type vtype when vtype == typeof(int): return DbType.Int32;
                case Type vtype when vtype == typeof(uint): return DbType.UInt32; //Not supported by Sql Server!
                case Type vtype when vtype == typeof(long): return DbType.Int64;
                case Type vtype when vtype == typeof(ulong): return DbType.UInt64; //Not supported by Sql Server!

                case Type vtype when vtype == typeof(float): return DbType.Single;
                case Type vtype when vtype == typeof(double): return DbType.Double;
                case Type vtype when vtype == typeof(decimal): return DbType.Decimal;
                //There is not currency type in C#. Not sure under what circumstance we could use DbType.Currency.
                //case Type vtype when vtype == typeof(decimal): return DbType.Currency;

                case Type vtype when vtype == typeof(string): return DbType.String;
                case Type vtype when vtype == typeof(char): return DbType.StringFixedLength;
                //Other string types that can be used if one knows there is no Unicode character...
                //TODO: Find a way to determine that.
                //case Type vtype when vtype == typeof(string): return DbType.AnsiString;
                //case Type vtype when vtype == typeof(char): return DbType.AnsiStringFixedLength;
                //case Type vtype when vtype == typeof(string): return DbType.Xml;

                case Type vtype when vtype == typeof(DateTime): return DbType.DateTime;
                case Type vtype when vtype == typeof(DateTimeOffset): return DbType.DateTimeOffset;
                //SQL Server returns a DbType.Time as a TimeSpan when selecting rows, but accepts it as
                //a DateTime when inserting or updating. This causes conversion issues, because there is
                //no real way (without making assumptions as to what the TimeSpan represents) to convert
                //from one another. Here we assume that the DbType.Time represents the time portion of a date/time,
                //and in that case, the System.DateTime is the right type to use in the row.
                case Type vtype when vtype == typeof(TimeSpan): return DbType.DateTime; //Not fully supported by Sql Server!
                //Other date types that can be used, though I'm not sure what's the difference...
                //case Type vtype when vtype == typeof(DateTime): return DbType.Date;
                //case Type vtype when vtype == typeof(DateTime): return DbType.DateTime2;

                case Type vtype when vtype == typeof(Guid): return DbType.Guid;

                case Type vtype when vtype == typeof(byte[]): return DbType.Binary;

                default:
                    if (underlyingType.IsEnum)
                        return DbType.Int32;
                    else
                        return DbType.Object; //Most likely will cause an error down the stream.
            }
        }

        public virtual DataReaderCo CreateDataReaderCo(IDataReader dr, CultureInfo culture) => new DataReaderCo(dr, culture);
    }
}
