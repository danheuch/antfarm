﻿using AntFarm.ORM;
using System;
using System.Data;

namespace AntFarm.Providers.SQLServer
{
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
    public class SqlMapperAttribute : MapperAttribute
    {
        public SqlMapperAttribute(SqlDbType sqlDBType)
            : base(DbType.Object)
        {
            SqlDBType = sqlDBType;
        }
        public SqlDbType SqlDBType { get; }
    }
}