﻿using AntFarm.Core;
using AntFarm.ORM;
using AntFarm.ORM.Providers;
using System;
using System.Data;
using System.Data.Common;
using System.Globalization;

namespace AntFarm.Providers.SQLServer
{
    public class SqlDatabaseProvider : DatabaseProvider
    {
        public override char SQLIdentifierDelimiterBegin => '[';
        public override char SQLIdentifierDelimiterEnd => ']';
        public override char SQLStatementSeparator => ';';
        public override char SQLParameterPrefix => '@';

        public SqlDatabaseProvider(AntConfig config)
            : base(config)
        { }

        public override DbConnection CreateConnection() => System.Data.SqlClient.SqlClientFactory.Instance.CreateConnection();

        public override TableProvider CreateTableProvider(Type rowType) => new SqlTableProvider(this, rowType);

        protected override DbType MapNet2DbType(Type type)
        {
            var dbType = base.MapNet2DbType(type);
            switch (dbType)
            {
                ////These DB types are not implemented in Sql Server:
                case DbType.SByte: return DbType.Int16; //No mapping exists from DbType SByte to a known SqlDbType
                case DbType.UInt16: return DbType.Int32; //No mapping exists from DbType UInt16 to a known SqlDbType
                case DbType.UInt32: return DbType.Int64; //No mapping exists from DbType UInt32 to a known SqlDbType
                case DbType.UInt64: return DbType.Decimal; //No mapping exists from DbType UInt64 to a known SqlDbType
                //Others seem OK:
                default: return dbType;
            }
        }

        public override DataReaderCo CreateDataReaderCo(IDataReader dr, CultureInfo culture) => new SqlDataReaderCo(dr, culture);
    }
}