﻿using AntFarm.ORM;
using AntFarm.ORM.Providers;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Reflection;

namespace AntFarm.Providers.SQLServer
{
    public class SqlColumnProvider : ColumnProvider
    {
        public SqlColumnProvider(TableProvider tbProvider, PropertyInfo pinfo, ColumnAttribute colAttribute, MapperAttribute mapAttribute)
            : base(tbProvider, pinfo, colAttribute, mapAttribute)
        {
            if (mapAttribute is SqlMapperAttribute mapAttribute_cast)
            {
                SqlDBType = mapAttribute_cast.SqlDBType;
                switch (SqlDBType)
                {
                    //NOTE: Sql Server "timespan" data type is read-only.
                    case SqlDbType.Timestamp:
                        IsReadOnly = true;
                        break;
                }
            }
            else if ((this.NullableColumnType ?? this.ColumnType) == typeof(TimeSpan) && this.DBType == DbType.DateTime)
            {
                //NOTE: Unfortunately the scope of the Sql Server "time" data type is limited to a single day. Any greater value will cause
                //an overflow. Still, as long as the user is aware of this limitation, this allows the system to read and write Sql Server
                //"time" values via a TimeSpan.
                SqlDBType = SqlDbType.Time;
            }
        }

        public SqlDbType? SqlDBType { get; }

        public override IDbDataParameter CreateCommandParameter(IDbCommand cd, string pname, object pvalue)
        {
            //IDbDataParameter
            var p = base.CreateCommandParameter(cd, pname, pvalue) as SqlParameter ?? throw new InvalidOperationException();
            if (SqlDBType.HasValue)
            {
                //Override the parameter's default set up.
                p.SqlDbType = SqlDBType.Value;
            }
            return p;
        }
    }
}