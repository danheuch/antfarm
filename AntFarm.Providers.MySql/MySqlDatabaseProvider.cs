﻿using AntFarm.ORM.Providers;
using System;
using System.Data.Common;

namespace AntFarm.Providers.MySQL
{
    public class MySqlDatabaseProvider : DatabaseProvider
    {
        public override char SQLIdentifierDelimiterBegin => '`';
        public override char SQLIdentifierDelimiterEnd => '`';
        public override char SQLStatementSeparator => ';';
        public override char SQLParameterPrefix => '?';
        public MySqlDatabaseProvider(AntConfig config)
            : base(config)
        { }
        public override DbConnection CreateConnection() => global::MySql.Data.MySqlClient.MySqlClientFactory.Instance.CreateConnection();

        public override TableProvider CreateTableProvider(Type rowType) => new MySqlTableProvider(this, rowType);
    }
}