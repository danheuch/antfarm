﻿using AntFarm.ORM;
using AntFarm.ORM.Providers;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Data;
using System.Diagnostics;
using System.Reflection;

namespace AntFarm.Providers.Oracle
{
    public class OracleColumnProvider : ColumnProvider
    {
        public OracleColumnProvider(TableProvider tbProvider, PropertyInfo pinfo, ColumnAttribute colAttribute, MapperAttribute mapAttribute)
            : base(tbProvider, pinfo, colAttribute, mapAttribute)
        {
            if (mapAttribute is OracleMapperAttribute mapAttribute_cast)
            {
                OracleDBType = mapAttribute_cast.OraDBType;
                if (OracleDBType.Value == OracleDbType.BFile)
                    //Not sure I understand those well enough to do them justices.
                    //TODO: Support this type of data.
                    throw new NotSupportedException("Oracle BFILE column type is currently not supported.");
            }
        }

        public OracleDbType? OracleDBType { get; }

        public override IDbDataParameter CreateCommandParameter(IDbCommand cd, string pname, object pvalue)
        {
            //IDbDataParameter
            var p = base.CreateCommandParameter(cd, pname, pvalue) as OracleParameter ?? throw new InvalidOperationException();
            if (OracleDBType.HasValue)
            {
                //Override the parameter's default set up.
                p.OracleDbType = OracleDBType.Value;
            }
            return p;
        }
    }
}