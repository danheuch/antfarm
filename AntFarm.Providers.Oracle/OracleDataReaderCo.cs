﻿using AntFarm.Core;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Data;

namespace AntFarm.Providers.Oracle
{
    /// <summary>
    /// Extends the base class to provide support for <see cref="TimeSpan"/> type.
    /// </summary>
    public class OracleDataReaderCo : DataReaderCo
    {
        public OracleDataReaderCo(IDataReader dr, IFormatProvider fp)
            : base(dr, fp)
        {
            OracleDataReader = 
                dr as OracleDataReader
                ?? throw new InvalidOperationException($"Invalid type of data reader. Expected {typeof(OracleDataReader)}, got {dr.GetType()}.");
        }

        public OracleDataReader OracleDataReader { get; }

        public override TimeSpan GetTimeSpan(int i) => OracleDataReader.GetTimeSpan(i);
    }
}
