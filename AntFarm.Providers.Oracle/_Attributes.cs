﻿using AntFarm.ORM;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Data;

namespace AntFarm.Providers.Oracle
{
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
    public class OracleMapperAttribute : MapperAttribute
    {
        public OracleMapperAttribute(OracleDbType oraDbType)
            : base(DbType.Object)
        {
            OraDBType = oraDbType;
        }
        public OracleDbType OraDBType { get; }
    }
}