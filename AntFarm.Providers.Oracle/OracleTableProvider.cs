﻿using AntFarm.ORM;
using AntFarm.ORM.Providers;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Reflection;
using System.Text;

namespace AntFarm.Providers.Oracle
{
    public class OracleTableProvider : TableProvider
    {
        public OracleTableProvider(OracleDatabaseProvider dbProvider, Type rowType)
            : base(dbProvider, rowType)
        { }

        public override ColumnProvider CreateColumnProvider(PropertyInfo pinfo, ColumnAttribute colAttribute, MapperAttribute mapAttribute)
            => new OracleColumnProvider(this, pinfo, colAttribute, mapAttribute);

        public override string GetCommandTextForInsert()
        {
            //Insert via PL/SQL procedure. This is the pattern of the procedure:
            //DECLARE
            //  <declare of insert statement's return type and variable>
            //BEGIN
            //  <insert statement, with optional return of auto-generated column values>
            //  <output of primary keys columns>
            //END;
            //For example, for the following table, where SEQ and ID1 form a compound PK:
            /*CREATE TABLE "TABLE1" (
                "SEQ" INT NOT NULL, -- auto-number generated from sequence named "sequence1"
                "ID1" INT GENERATED ALWAYS AS IDENTITY NOT NULL, --auto-generated (i.e., identity) column
	            "TXT" VARCHAR2(20 BYTE) NULL
            ); 
            */
            //Here would be the generated PL/SQL procedure:
            /*DECLARE 
                TYPE retType IS RECORD (seq0 NUMBER, id1 NUMBER);
                retValue retType;
            BEGIN
                INSERT INTO TABLE1(SEQ,TXT)VALUES(sequence1.NEXTVAL,'Hello')RETURNING SEQ,ID1 INTO retValue;
                :PK0:=retValue.seq0;
                :PK1:=retValue.id1;
            END;
            */

            var sb = new StringBuilder();

            //Declare 
            var sbReturningRecordType = new StringBuilder(); //column list of retType record
            var sbReturningColumns = new StringBuilder(); //column list of returning statement
            string prefix = "";
            for (int i = 0; i < this.Columns.Count; i++)
            {
                var colInfo = this.Columns[i];
                if (colInfo.IsAutoNumber || colInfo.SequenceName != null)
                {
                    string sqlTypeName;
                    //switch (this.GetDbType(colInfo.ColumnType))
                    switch (colInfo.DBType)
                    {
                        case DbType.SByte:
                        case DbType.Byte:
                        case DbType.Int16:
                        case DbType.UInt16:
                        case DbType.Int32:
                        case DbType.UInt32:
                        case DbType.Int64:
                            sqlTypeName = "NUMBER";
                            break;
                        default:
                            throw new NotSupportedException("Unable to determine the sequence type based on column information");
                    }
                    sbReturningRecordType
                        .Append(prefix)
                        .Append(colInfo.IsAutoNumber ? "id" : "seq").Append(i)
                        .Append(" ")
                        .Append(sqlTypeName);
                    sbReturningColumns
                        .Append(prefix)
                        .Append(GetColumnName(colInfo));
                    prefix = ",";
                }
            }
            if (sbReturningRecordType.Length > 0)
                sb
                    .AppendLine("DECLARE")
                    .Append("TYPE retType IS RECORD (")
                    .Append(sbReturningRecordType.ToString())
                    .AppendLine(");")
                    .AppendLine("retValue retType;");

            //Begin
            sb.AppendLine("BEGIN");

            //Insert
            sb
                .Append("INSERT INTO ")
                .Append(GetTableName())
                .Append("(");
            prefix = "";
            for (int i = 0; i < this.Columns.Count; i++)
            {
                var colInfo = this.Columns[i];
                if (!colInfo.IsReadOnly)
                {
                    sb
                        .Append(prefix)
                        .Append(GetColumnName(colInfo));
                    prefix = ",";
                }
            }
            sb.Append(")VALUES(");
            prefix = "";
            for (int i = 0; i < this.Columns.Count; i++)
            {
                var colInfo = this.Columns[i];
                if (!colInfo.IsReadOnly)
                {
                    sb
                        .Append(prefix)
                        .Append(colInfo.SequenceName != null ? $"{GetSequenceName(colInfo)}.NEXTVAL" : $":V{i}");
                    prefix = ",";
                }
            }
            sb.Append(")");
            if (sbReturningColumns.Length > 0)
                sb
                    .Append("RETURNING ")
                    .Append(sbReturningColumns.ToString())
                    .Append(" INTO retValue");
            sb.AppendLine(";");

            //PK output:
            for (int i = 0; i < this.Columns.Count; i++)
            {
                var colInfo = this.Columns[i];
                if (colInfo.IsPrimaryKey)
                {
                    sb.Append(":PK").Append(i).Append(":=");
                    if (colInfo.IsAutoNumber)
                        sb.Append("retValue.id").Append(i);
                    else if (colInfo.SequenceName != null)
                        sb.Append("retValue.seq").Append(i);
                    else
                        sb.Append(":V").Append(i);
                    sb.AppendLine(";");
                }
            }

            //End
            sb.AppendLine("END;");

            return sb.ToString();
        }

        public override IDbDataParameter[] ParameterizeCommandForInsert(IDbCommand cd, object row)
        {
            List<OracleParameter> outps = null;
            for (int i = 0; i < this.Columns.Count; i++)
            {
                var colInfo = this.Columns[i];
                if (!colInfo.IsReadOnly && colInfo.SequenceName == null)
                {
                    object value = colInfo.Accessor.GetValue(row);
                    var p = colInfo.CreateCommandParameter(cd, $":V{i}", value);
                    cd.Parameters.Add(p);
                }
            }
            for (int i = 0; i < this.Columns.Count; i++)
            {
                var colInfo = this.Columns[i];
                if (colInfo.IsPrimaryKey)
                {
                    if (outps == null)
                        outps = new List<OracleParameter>();
                    //IDbDataParameter
                    var p = new OracleParameter
                    {
                        ParameterName = $":PK{i}",
                        Direction = ParameterDirection.Output,
                        DbType = colInfo.DBType,
                        Size = 4000 //TODO: Better way to determine the max size. Via the Column attribute?
                    };
                    outps.Add(p);
                    cd.Parameters.Add(p);
                }
            }
            return outps?.ToArray();
        }

        protected override string EnsureDelimitedIdentifier(string identifier, bool forceDelimiters = false)
        {
            //TODO: Focing the uppercase should be controllable. Suggestion:
            //- If the name comes from the attribute => preserve the casing.
            //- Otherwise, offer an attribute at the table level.
            //- If none of these options are used, then force the uppercase.
            return base.EnsureDelimitedIdentifier(identifier?.ToUpper(), forceDelimiters);
        }
    }
}