﻿using AntFarm.Core;
using AntFarm.ORM.Providers;
using System;
using System.Data;
using System.Data.Common;
using System.Globalization;

namespace AntFarm.Providers.Oracle
{
    public class OracleDatabaseProvider : DatabaseProvider
    {
        public override char SQLIdentifierDelimiterBegin => '"';
        public override char SQLIdentifierDelimiterEnd => '"';
        public override char SQLStatementSeparator => ';';
        public override char SQLParameterPrefix => ':';

        public OracleDatabaseProvider(AntConfig config)
            : base(config)
        { }

        public override DbConnection CreateConnection() => global::Oracle.ManagedDataAccess.Client.OracleClientFactory.Instance.CreateConnection();

        public override TableProvider CreateTableProvider(Type rowType) => new OracleTableProvider(this, rowType);

        //protected override DbType MapNet2DbType(Type type)
        //{
        //    var dbType = base.MapNet2DbType(type);
        //    switch (dbType)
        //    {
        //        case DbType.Time: return DbType.DateTime;
        //        //Others seem OK:
        //        default: return dbType;
        //    }
        //}

        public override DataReaderCo CreateDataReaderCo(IDataReader dr, CultureInfo culture) => new OracleDataReaderCo(dr, culture);
    }
}