﻿DROP TABLE IF EXISTS Country;

CREATE TABLE Country
(
    "Alpha-2" char(2) not null primary key,
    "Alpha-3" char(3) not null,
    "Numeric" char(3) not null,
    "Name" varchar(75) not null
);
