﻿using AntFarm.ORM;
using System;

/// <summary>
/// Sample HR system database.
/// Credit: http://www.sqltutorial.org/sql-sample-database/
/// </summary>
namespace AntFarm.Data.Samples.HR
{
    [Table("regions")]
    public class Region
    {
        [Column(IsPrimaryKey = true, IsAutoNumber = true)]
        public int region_id { get; set; }
        public string region_name { get; set; }
    }

    [Table("countries")]
    public class Country
    {
        [Column(IsPrimaryKey = true)]
        public string country_id { get; set; }
        public string country_name { get; set; }
        public int region_id { get; set; }
    }

    [Table("locations")]
    public class Location
    {
        [Column(IsPrimaryKey = true, IsAutoNumber = true)]
        public int location_id { get; set; }
        public string street_address { get; set; }
        public string postal_code { get; set; }
        public string city { get; set; }
        public string state_province { get; set; }
        public string country_id { get; set; }
    }

    [Table("jobs")]
    public class Job
    {
        [Column(IsPrimaryKey = true, IsAutoNumber = true)]
        public int job_id { get; set; }
        public string job_title { get; set; }
        public decimal? min_salary { get; set; }
        public decimal? max_salary { get; set; }
    }

    [Table("departments")]
    public class Department
    {
        [Column(IsPrimaryKey = true, IsAutoNumber = true)]
        public int department_id { get; set; }
        public string department_name { get; set; }
        public int? location_id { get; set; }
    }

    [Table("employees")]
    public class Employee
    {
        [Column(IsPrimaryKey = true, IsAutoNumber = true)]
        public int employee_id { get; set; }
        public string first_name { get; set; }
        public string last_name { get; set; }
        public string email { get; set; }
        public string phone_number { get; set; }
        public DateTime hire_date { get; set; }
        public int job_id { get; set; }
        public decimal salary { get; set; }
        public int? manager_id { get; set; }
        public int? department_id { get; set; }
    }

    [Table("dependents")]
    public class Dependent
    {
        [Column(IsPrimaryKey = true, IsAutoNumber = true)]
        public int dependent_id { get; set; }
        public string first_name { get; set; }
        public string last_name { get; set; }
        public string relationship { get; set; }
        public int employee_id { get; set; }
    }
}