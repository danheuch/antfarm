﻿Demo:
	- Folder contains all the classes needed to run the Demo project, as well as the database creation scripts for all the supported providers.
Test:
	- Folder contains all the classes needed to run the unit tests, as well as the database creation scripts for all the supported providers.
Samples:
	- Sample databases found across the web...
	- HR: http://www.sqltutorial.org/sql-sample-database/