﻿using AntFarm.ORM;
using System;

namespace AntFarm.Data.Test
{
    [Table]
    public class T01_SimplePK
    {
        [Column(IsPrimaryKey = true)]
        public string Code { get; set; }
        public string Name { get; set; }
    }

    [Table]
    public class T02_AutoNumberPK
    {
        [Column(IsPrimaryKey = true, IsAutoNumber = true)]
        public int ID { get; set; }
        public string Name { get; set; }
    }
}