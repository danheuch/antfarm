﻿using AntFarm.ORM;
using System;

namespace AntFarm.Data.Test
{
    /// <summary>
    /// Table representing the basic .NET types supported by AntFarm.
    /// Each column should be able to hold the complete range of possible values of the represented type,
    /// with the following additional constraints/limits:
    /// - STRING: Column should be able to contain at least 50 Unicode characters.
    /// - BYTES and CHARS: Columns should be able to hold arrays of up to 100,000 elements (byte or Unicode char).
    /// </summary>
    [Table]
    public class NetTypes
    {
        public int ID { get; set; }
        //-----------------------------
        public bool? BOOLEAN { get; set; }
        public byte? BYTE { get; set; }
        public sbyte? SIGNEDBYTE { get; set; }
        public short? INT16 { get; set; }
        public ushort? UINT16 { get; set; }
        public int? INT32 { get; set; }
        public uint? UINT32 { get; set; }
        public long? INT64 { get; set; }
        public ulong? UINT64 { get; set; }
        public float? SINGLE { get; set; }
        public double? DOUBLE { get; set; }
        public decimal? DECIMAL { get; set; }
        public DateTime? DATETIME { get; set; }
        public DateTimeOffset? DATETIMEOFFSET { get; set; }
        public TimeSpan? TIMESPAN { get; set; }
        public char? CHAR { get; set; }
        public string STRING { get; set; }
        public Guid? GUID { get; set; }
        public byte[] BYTES { get; set; }
        public char[] CHARS { get; set; }

        public static NetTypes Create_Null(int id)
        {
            return new NetTypes { ID = id };
        }

        public static NetTypes Create_Empty(int id)
        {
            return new NetTypes
            {
                ID = id,
                BOOLEAN = false,
                BYTE = 0,
                SIGNEDBYTE = 0,
                INT16 = 0,
                UINT16 = 0,
                INT32 = 0,
                UINT32 = 0,
                INT64 = 0,
                UINT64 = 0,
                SINGLE = 0,
                DOUBLE = 0,
                DECIMAL = 0,
                DATETIME = new DateTime(),
                DATETIMEOFFSET = new DateTimeOffset(),
                TIMESPAN = new TimeSpan(),
                CHAR = '\0',
                STRING = string.Empty,
                GUID = Guid.Empty,
                BYTES = new byte[0],
                CHARS = new char[0],
            };
        }

        public static NetTypes Create_Min(int id)
        {
            return new NetTypes
            {
                ID = id,
                BOOLEAN = false,
                BYTE = byte.MinValue,
                SIGNEDBYTE = sbyte.MinValue,
                INT16 = short.MinValue,
                UINT16 = ushort.MinValue,
                INT32 = int.MinValue,
                UINT32 = uint.MinValue,
                INT64 = long.MinValue,
                UINT64 = uint.MinValue,
                SINGLE = float.MinValue,
                DOUBLE = double.MinValue,
                DECIMAL = decimal.MinValue,
                DATETIME = DateTime.MinValue,
                DATETIMEOFFSET = DateTimeOffset.MinValue,
                TIMESPAN = TimeSpan.MinValue,
                CHAR = char.MinValue,
                STRING = "",
                GUID = new Guid(),
                BYTES = new byte[0],
                CHARS = new char[0],
            };
        }

        public static NetTypes Create_Max(int id)
        {
            const int max_string_length = 50;
            const int max_blob_length = 100000;
            const int max_clob_length = 100000;
            return new NetTypes
            {
                ID = id,
                BOOLEAN = true,
                BYTE = byte.MaxValue,
                SIGNEDBYTE = sbyte.MaxValue,
                INT16 = short.MaxValue,
                UINT16 = ushort.MaxValue,
                INT32 = int.MaxValue,
                UINT32 = uint.MaxValue,
                INT64 = long.MaxValue,
                UINT64 = uint.MaxValue,
                SINGLE = float.MaxValue,
                DOUBLE = double.MaxValue,
                DECIMAL = decimal.MaxValue,
                DATETIME = DateTime.MaxValue,
                DATETIMEOFFSET = DateTimeOffset.MaxValue,
                TIMESPAN = TimeSpan.MaxValue,
                CHAR = char.MaxValue,
                STRING = new string(char.MaxValue, max_string_length),
                GUID = Guid.NewGuid(), //n/a - any value is OK
                BYTES = RandomData.RandomBytes(max_blob_length), //n/a - use max_blob_length of random bytes
                CHARS = RandomData.RandomChars(max_clob_length), //n/a - use max_clob_length of random characters
            };
        }
    }
}