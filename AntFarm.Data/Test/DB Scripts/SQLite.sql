﻿DROP TABLE IF EXISTS "NetTypes";
DROP TABLE IF EXISTS "T01_SimplePK";
DROP TABLE IF EXISTS "T02_AutoNumberPK";
DROP TABLE IF EXISTS "Country";

CREATE TABLE "NetTypes" (
	ID INTEGER NOT NULL,
	-------------------
	"BOOLEAN" INTEGER,
	"BYTE" INTEGER,
	"SIGNEDBYTE" INTEGER,
	"INT16" INTEGER,
	"UINT16" INTEGER,
	"INT32" INTEGER,
	"UNINT32" INTEGER,
	"INT64" INTEGER,
	"UNINT64" REAL,
	"SINGLE" REAL,
	"DOUBLE" REAL,
	"DECIMAL" REAL,
	-- "DATETIME" TEXT ,
	-- "DATETIMEOFFSET" TEXT,
	-- "TIMESPAN" TEXT,
	"CHAR" TEXT,
	"STRING" TEXT,
	"GUID" TEXT,
	"BYTES" BLOB,
	"CHARS" BLOB
);

CREATE TABLE "T01_SimplePK" (
	"CODE" TEXT NOT NULL PRIMARY KEY,
	"NAME" TEXT NOT NULL
)

CREATE TABLE "T02_AutoNumberPK" (
	"ID" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
	"NAME" TEXT NOT NULL
)

CREATE TABLE Country (
    "Code2" TEXT NOT NULL PRIMARY KEY,
    "Code3" TEXT NOT NULL,
    "Numeric" TEXT NOT NULL,
    "Name" TEXT NOT NULL
);
