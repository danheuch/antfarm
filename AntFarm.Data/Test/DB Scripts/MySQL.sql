﻿DROP TABLE IF EXISTS `NetTypes`;
DROP TABLE IF EXISTS `T01_SimplePK`;
DROP TABLE IF EXISTS `T02_AutoNumberPK`;
DROP TABLE IF EXISTS `Country`;

CREATE TABLE `NetTypes` (
	`ID` INT NOT NULL,
	`BOOLEAN` BIT,
	`BYTE` TINYINT UNSIGNED,
	`SIGNEDBYTE` TINYINT,
	`INT16` smallint,
	`UINT16` smallint unsigned,
	`INT32` INT,
	`UNINT32` int unsigned,
	`INT64` bigint,
	`UNINT64` bigint unsigned
	-- "SINGLE" BINARY_FLOAT,
	-- "DOUBLE" BINARY_DOUBLE,
	-- "DECIMAL" number(18,3),
	-- "DATETIME" DATE,
	-- "DATETIMEOFFSET" TIMESTAMP WITH time ZONE,
	-- "TIMESPAN" INTERVAL DAY TO SECOND,
	-- "CHAR" NCHAR(1),
	-- "STRING" NVARCHAR2(50),
	-- "GUID" RAW(16),
	-- "BYTES" BLOB,
	-- "CHARS" CLOB
);

CREATE TABLE `T01_SimplePK` (
	`Code` CHAR(2) NOT NULL PRIMARY KEY,
	`Name` VARCHAR(75) CHARACTER SET UTF16 NOT NULL
);

CREATE TABLE `T02_AutoNumberPK` (
	`ID` int NOT NULL AUTO_INCREMENT PRIMARY KEY,
	`Name` VARCHAR(75) CHARACTER SET UTF16 NOT NULL
)

CREATE TABLE `Country` (
	`Code2` CHAR(2) NOT NULL PRIMARY KEY,
	`Code3` CHAR(3) NOT NULL,
	`Numeric` CHAR(3) NOT NULL,
	`Name` VARCHAR(75) CHARACTER SET UTF16 NOT NULL
)
