﻿DROP TABLE IF EXISTS "nettypes";
DROP TABLE IF EXISTS "t01_simplepk";
DROP TABLE IF EXISTS "t02_autonumberpk";
DROP TABLE IF EXISTS "country";

CREATE TABLE "nettypes" (
	"id" int NOT NULL
	-------------------
	-- "BOOLEAN" NUMBER(1,0),
	-- "BYTE" number(3,0),
	-- "SIGNEDBYTE" number(3,0),
	-- "INT16" number(5,0),
	-- "UINT16" number(5,0),
	-- "INT32" number(10,0),
	-- "UNINT32" number(10,0),
	-- "INT64" number(19,0),
	-- "UNINT64" number(20,0),
	-- "SINGLE" BINARY_FLOAT,
	-- "DOUBLE" BINARY_DOUBLE,
	-- "DECIMAL" number(18,3),
	-- "DATETIME" DATE,
	-- "DATETIMEOFFSET" TIMESTAMP WITH time ZONE,
	-- "TIMESPAN" INTERVAL DAY TO SECOND,
	-- "CHAR" NCHAR(1),
	-- "STRING" NVARCHAR2(50),
	-- "GUID" RAW(16),
	-- "BYTES" BLOB,
	-- "CHARS" CLOB
);

CREATE TABLE "t01_simplepk" (
	"code" CHAR(2) NOT NULL PRIMARY KEY,
	"name" VARCHAR(75) NOT NULL
);

CREATE TABLE "t02_autonumberpk" (
	"id" SERIAL PRIMARY KEY,
	"name" VARCHAR(75) NOT NULL
);

CREATE TABLE "country" (
	"code2" CHARACTER(2) NOT NULL PRIMARY KEY,
	"code3" CHARACTER(3) NOT NULL,
	"numeric" CHARACTER(3) NOT NULL,
	"name" CHARACTER(75) NOT NULL
)
