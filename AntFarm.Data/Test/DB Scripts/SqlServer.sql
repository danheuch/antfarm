﻿if object_id('[NetTypes]') is not null DROP TABLE [NetTypes];
if object_id('[T01_SimplePK]') is not null DROP TABLE [T01_SimplePK];
if object_id('[T02_AutoNumberPK]') is not null DROP TABLE [T02_AutoNumberPK];
if object_id('[Country]') is not null DROP TABLE [Country];

CREATE TABLE [NetTypes] (
	[ID] int NOT NULL,
	-------------------
	[BOOLEAN] bit,
	[BYTE] tinyint,
	[SIGNEDBYTE] smallint,
	[INT16] smallint,
	[UINT16] int,
	[INT32] int,
	[UNINT32] bigint,
	[INT64] bigint,
	[UNINT64] decimal(20,0),
	[SINGLE] real,
	[DOUBLE] float,
	[DECIMAL] decimal(18,3),
	[DATETIME] DATE,
	[DATETIMEOFFSET] datetimeoffset,
	[TIMESPAN] time,
	[CHAR] NCHAR(1),
	[STRING] NVARCHAR(50),
	[GUID] uniqueidentifier,
	[BYTES] varbinary(max),
	[CHARS] varchar(max)
);

CREATE TABLE [T01_SimplePK] (
	[Code] CHAR(2) NOT NULL PRIMARY KEY CLUSTERED,
	[Name] NVARCHAR(75) NOT NULL,
)

CREATE TABLE [T02_AutoNumberPK] (
	[ID] int identity(1,1) NOT NULL PRIMARY KEY CLUSTERED,
	[Name] NVARCHAR(75) NOT NULL,
)

CREATE TABLE [Country] (
	[Code2] CHAR(2) NOT NULL PRIMARY KEY CLUSTERED,
	[Code3] CHAR(3) NOT NULL,
	[Numeric] CHAR(3) NOT NULL,
	[Name] NVARCHAR(75) NOT NULL,
)

/*
-----------------------------------------------------------------------------------------------------------
-- Drop all the constraints preventing dropping the tables
-- Credit: https://stackoverflow.com/questions/15107238/how-can-i-drop-a-table-if-there-is-a-foreign-key-constraint-in-sql-server/15107343
DECLARE @cmd varchar(4000)
DECLARE MY_CURSOR CURSOR LOCAL STATIC READ_ONLY FORWARD_ONLY
FOR 
select 'ALTER TABLE ['+s.name+'].['+t.name+'] DROP CONSTRAINT [' + RTRIM(f.name) +'];'
FROM sys.Tables t 
	INNER JOIN sys.foreign_keys f ON f.parent_object_id = t.object_id 
	INNER JOIN sys.schemas s ON s.schema_id = f.schema_id
OPEN MY_CURSOR
FETCH NEXT FROM MY_CURSOR INTO @cmd
WHILE @@FETCH_STATUS = 0
BEGIN
    PRINT @cmd
    EXEC (@cmd)
    FETCH NEXT FROM MY_CURSOR INTO @cmd
END
CLOSE MY_CURSOR
DEALLOCATE MY_CURSOR
-----------------------------------------------------------------------------------------------------------
*/