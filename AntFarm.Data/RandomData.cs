﻿using System;
using System.Linq;

namespace AntFarm.Data
{
    public static class RandomData
    {
        private const string chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

        private static readonly Random _rnd = new Random();

        public static string RandomString(int size)
        {
            return new string(Enumerable.Repeat(chars, _rnd.Next(1, size)).Select(s => s[_rnd.Next(1, s.Length)]).ToArray());
        }

        public static string RandomXml()
        {
            return $"<root><string>{RandomString(50)}</string><number>{RandomInt32()}</number><date>{RandomDateTime(true)}</date></root>";
        }

        public static DateTime RandomDateTime(bool includeTime)
        {
            var x = DateTime.Now
                .AddYears(_rnd.Next(50))
                .AddMonths(_rnd.Next(12))
                .AddDays(_rnd.Next(30))
                .AddHours(_rnd.Next(24))
                .AddMinutes(_rnd.Next(60))
                .AddSeconds(_rnd.Next(60))
                .AddMilliseconds(_rnd.Next(1000));
            return includeTime ? x : x.Subtract(x.TimeOfDay);
        }

        public static DateTimeOffset RandomDateTimeOffset(bool includeTime)
        {
            var x = DateTimeOffset.Now
                .AddYears(_rnd.Next(50))
                .AddMonths(_rnd.Next(12))
                .AddDays(_rnd.Next(30))
                .AddHours(_rnd.Next(24))
                .AddMinutes(_rnd.Next(60))
                .AddSeconds(_rnd.Next(60))
                .AddMilliseconds(_rnd.Next(1000));
            return includeTime ? x : x.Subtract(x.TimeOfDay);
        }

        public static TimeSpan RandomTimeSpan(int days)
        {
            return TimeSpan.FromDays(days * _rnd.NextDouble());
        }

        public static short RandomInt16()
        {
            return Convert.ToInt16(_rnd.NextDouble() * 1000);
        }

        public static int RandomInt32()
        {
            return Convert.ToInt32(_rnd.NextDouble() * 1000);
        }

        public static long RandomInt64(long min, long max)
        {
            return min + Convert.ToInt64(_rnd.NextDouble() * (max - min));
        }

        public static decimal RandomDecimal()
        {
            return Convert.ToDecimal(_rnd.NextDouble() * 1000);
        }

        public static double RandomDouble()
        {
            return _rnd.NextDouble() * 1000;
        }

        public static float RandomSingle()
        {
            return Convert.ToSingle(_rnd.NextDouble() * 1000);
        }

        public static byte[] RandomBytes(int size)
        {
            var buffer = new byte[_rnd.Next(0, size)];
            _rnd.NextBytes(buffer);
            return buffer;
        }
        public static char[] RandomChars(int size)
        {
            var buffer = new byte[_rnd.Next(0, size)];
            _rnd.NextBytes(buffer);
            return System.Text.Encoding.UTF8.GetString(buffer).ToCharArray();
        }

        public static bool RandomBoolean()
        {
            return _rnd.NextDouble() >= .5;
        }

        public static byte RandomByte()
        {
            return Convert.ToByte(_rnd.Next(byte.MinValue, byte.MaxValue));
        }

        public static char RandomChar()
        {
            return chars[_rnd.Next(0, chars.Length - 1)];
        }

        public static object RandomObject()
        {
            return new object[] { RandomInt16(), RandomDateTime(true), RandomString(50) }[_rnd.Next(0, 3)];
        }
    }
}